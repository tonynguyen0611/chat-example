import React from 'react'
import { withRouter } from 'react-router-dom'
import PropTypes from 'prop-types'
import axios from 'axios'
import DetailsCollectionModal from '../preview_album'
import ConfirmModal from '../../modals/confirm'
import './collections_list.scss'

const ObjectImage = ({
  collection,
  getCollection,
  index,
  isEditable,
  handleDeleteCollection,
  handleClickEditCollection,
  currentUser
}) => {
  return (
    <div>
      {
        collection.preview_media_item && collection.preview_media_item.images &&
        <div className='collection-item' onClick={() => getCollection(collection, collection.preview_media_item)}>
          <img className='img-responsive' src={collection.preview_media_item.images[0].source} />
          <div className='collection-item-title'>
            <div className='collection-item-title-text'>
              {collection.title}
            </div>
            {isEditable && collection.user.id === currentUser.id &&
              <div className='edit-tool'>
                {!collection.isDeleting &&
                  <div>
                    <i
                      className='fa fa-trash'
                      aria-hidden='true'
                      onClick={(e) => handleDeleteCollection(e, index)} />
                    <i
                      className='fa fa-pencil-square-o'
                      aria-hidden='true'
                      onClick={(e) => handleClickEditCollection(e, collection.id)} />
                  </div>
                }
                {collection.isDeleting &&
                  <img className='img-responsive' src='https://cdn.zenquiz.net/static/Assets/loading-animation.gif' />
                }
              </div>
            }
          </div>
        </div>
      }
    </div>
  )
}

class CollectionsList extends React.Component {

  constructor (props) {
    super(props)
    this.state = {
      isLoadingCollectionList: false,
      isError: false,
      isSuccess: false,
      isEditable: props.canChange === 'YES',
      errorMessage: '',
      successMessage: '',
      numOfCol: props.numOfCol || 4,
      collectionActive: {},
      mediaItems:[],
      indexColection: 0,
      mediaItem: {},
      isShowModal: false,
      isShowConfirmModal: false,
      deletionIndex: '',
      currentUser: JSON.parse(localStorage.getItem('userinfo'))
    }
  }

  getCollection = (collection, mediaItem) => {
    this.setState({
      collectionActive: collection,
      mediaItem: mediaItem,
      isShowModal: true
    })
  }

  handleClickEditCollection = (e, collectionId) => {
    e.stopPropagation()
    this.props.history.push('/collections/edit/' + collectionId)
  }

  handleDeleteCollection = (e, index) => {
    e.stopPropagation()
    this.setState({
      isShowConfirmModal: true,
      deletionIndex: index
    })
  }

  deleteCollection = () => {
    let collectionsList = this.props.collectionsList
    let index = this.state.deletionIndex
    collectionsList[index].isDeleting = true
    this.setState({
      isShowConfirmModal: false
    })

    let url = ENVIRONMENT.API_DOMAIN + '/albums/' + collectionsList[index].id
    collectionsList.splice(index, 1)
    axios.delete(encodeURI(url), { withCredentials: true })
    .then(res => console.log('this', res))
    .catch(err => console.log('err', err.response.data.error))
  }

  setModalState = () => {
    let currentIsShowModal = this.state.isShowModal
    this.setState({
      isShowModal: !currentIsShowModal
    })
  }

  cancelConfirmModal = () => {
    this.setState({
      isShowConfirmModal: false
    })
  }

  componentWillMount () {
  }

  render () {
    let _this = this
    let numOfCol = this.state.numOfCol
    const modalDetailCollection = DetailsCollectionModal({
      collection: _this.state.collectionActive,
      mediaItem: _this.state.mediaItem,
      isOpen: _this.state.isShowModal
    }, _this.setModalState)
    const confirmModal = ConfirmModal({
      isShowingCancel: true,
      isShowingConfirm: true,
      onCancel: _this.cancelConfirmModal,
      onConfirm: _this.deleteCollection,
      text: 'Do you want to delete it?',
      isOpen: _this.state.isShowConfirmModal
    })
    let collectionGroups = []

    for (let i = 0; i < numOfCol; i++) {
      collectionGroups.push([])
    }

    if (_this.props.collectionsList && _this.props.collectionsList.length > 0) {
      _this.props.collectionsList.map((collection, i) => {
        collectionGroups[i % numOfCol].push(
          <ObjectImage
            key={i} index={i}
            collection={collection}
            getCollection={_this.getCollection}
            handleDeleteCollection={_this.handleDeleteCollection}
            isEditable={_this.state.isEditable}
            handleClickEditCollection={_this.handleClickEditCollection}
            currentUser={_this.state.currentUser} />
        )
      })
    }
    return (
      <div className='collections-container'>
        {modalDetailCollection}
        {confirmModal}
        <div className='collections-list'>
          <div className='col-lg-12 col-md-12 col-sm-12 col-xs-12 collections-grid'>
            {
              collectionGroups.map((collectionGroup, index) => {
                return (
                  <div className={'custom-col col-md-' + (12 / numOfCol)}>
                    {collectionGroup}
                  </div>
                )
              })
            }
          </div>
        </div>
      </div>
    )
  }
}

CollectionsList.propTypes = {
  match: PropTypes.object.isRequired,
  getCollectionResultList: PropTypes.func,
  collectionsList: PropTypes.array.isRequired,
  numOfCol: PropTypes.number.isRequired,
  canChange: PropTypes.string,
  history: PropTypes.object
}

ObjectImage.propTypes = {
  collection: PropTypes.object.isRequired,
  getCollection: PropTypes.func.isRequired,
  index: PropTypes.number.isRequired,
  isEditable: PropTypes.bool.isRequired,
  handleDeleteCollection: PropTypes.func.isRequired,
  handleClickEditCollection: PropTypes.func.isRequired,
  currentUser: PropTypes.object.isRequired
}

export default withRouter(CollectionsList)
