import axios from 'axios'
import { setCurrentUSer } from '../../user_view/modules/header'

// ------------------------------------
// Constants
// ------------------------------------
const INPUT_VALUE = 'INPUT_VALUE'
const UPDATE_SUCCESS = 'UPDATE_SUCCESS'
const UPDATE_ERROR = 'UPDATE_ERROR'
const UPDATE_LOADING = 'UPDATE_LOADING'
const RESTORE_ATTRIBUTES = 'RESTORE_ATTRIBUTES'
const allowFields = ['first_name', 'last_name', 'bio', 'description',
  'instagram', 'linkedin', 'twitter', 'skills', 'tags', 'city',
  'personal_website', 'region', 'country', 'phone', 'birthday', 'categories',
  'occupation', 'gender'
]
var userInfo = JSON.parse(localStorage.getItem('userinfo'))
// ------------------------------------
// Actions
// ------------------------------------
export function handleInputValueChange (e, divName) {
  return (dispatch, getState) => {
    let input = {}
    if (divName === 'birthday') {
      input[divName] = e
    } else {
      input[divName] = e.target.value
    }
    dispatch(setInput(input))
  }
}

export function saveChanges () {
  return (dispatch, getState) => {
    let moduleState = getState().profileInfo
    let location = ['city', 'region', 'country']
    let externalProfiles = ['instagram', 'linkedin', 'twitter']
    let arrayFields = ['occupation', 'skills', 'tags', 'categories']
    let dataObj = {}
    dataObj.location = {}
    dataObj.external_profiles = {}
    allowFields.forEach(field => {
      // remove spaces before and after text
      let inputValue = moduleState[field] ? moduleState[field].trim() : ''

      if (inputValue) {
        if (location.indexOf(field) >= 0) {
          dataObj.location[field] = inputValue
        } else if (externalProfiles.indexOf(field) >= 0) {
          dataObj.external_profiles[field] = inputValue
        } else if (arrayFields.indexOf(field) >= 0) {
          let tempArr = []
          tempArr.push(inputValue)
          dataObj[field] = tempArr
        } else {
          dataObj[field] = inputValue
        }
      }
    })
    dispatch(sendUpdateRequest(dataObj))
  }
}

export function restoreAllAttributes () {
  return {
    type: RESTORE_ATTRIBUTES
  }
}

function sendUpdateRequest (dataObj) {
  return (dispatch, getState) => {
    dispatch(setLoading())
    axios.put(ENVIRONMENT.API_DOMAIN + '/users/' + userInfo.id, dataObj, { withCredentials: true })
    .then(res => {
      dispatch(setCurrentUSer(res.data))
      dispatch(updateSuccess())
    })
    .catch(err => {
      let finalError = err.response && err.response.data ? err.response.data.error : err
      dispatch(setErrorMessages(finalError))
    })
  }
}

function setLoading () {
  return {
    type: UPDATE_LOADING
  }
}
function setInput (input) {
  return {
    type: INPUT_VALUE,
    input: input
  }
}

function updateSuccess () {
  return {
    type: UPDATE_SUCCESS
  }
}

function setErrorMessages (msg) {
  return {
    type: UPDATE_ERROR,
    errorMessage: msg
  }
}
// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [INPUT_VALUE]: (state, action) => {
    return {
      ...state,
      ...action.input
    }
  },
  [UPDATE_SUCCESS]: (state, action) => {
    return {
      ...state,
      isLoading: false,
      isUpdateSuccess: true,
      successMessage: 'Save successfully'
    }
  },
  [UPDATE_ERROR]: (state, action) => {
    return {
      ...state,
      isLoading: false,
      isUpdateError: true,
      errorMessage: action.errorMessage
    }
  },
  [UPDATE_LOADING]: (state, action) => {
    return {
      ...state,
      isLoading: true,
      isUpdateError: false,
      isUpdateSuccess: false,
      errorMessage: '',
      successMessage: ''
    }
  },
  [RESTORE_ATTRIBUTES]: (state, action) => {
    return {
      ...state,
      ...initialState
    }
  }
}

// ------------------------------------
// Reducer
// ------------------------------------

const initialState = {
  isLoading: false,
  isUpdateSuccess: false,
  isUpdateError: false,
  errorMessage: '',
  successMessage: '',
  first_name: userInfo.first_name || '',
  last_name: userInfo.last_name || '',
  email: userInfo.email || '',
  personal_website: userInfo.personal_website || '',
  birthday: userInfo.birthday || '',
  phone: userInfo.phone || '',
  country: userInfo.location.country || '',
  region: userInfo.location.region || '',
  city: userInfo.location.city || '',
  bio: userInfo.bio || '',
  occupation: userInfo.occupation[0] || '',
  skills: userInfo.skills[0] || '',
  categories: userInfo.categories[0] || '',
  tags: userInfo.tags[0] || '',
  description: userInfo.description || '',
  gender: userInfo.gender || '',
  instagram: userInfo.external_profiles.instagram || '',
  linkedin: userInfo.external_profiles.linkedin || '',
  twitter: userInfo.external_profiles.twitter || ''
}

export default function profileReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
