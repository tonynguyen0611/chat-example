import { connectWithLifecycle } from 'react-lifecycle-component'
import { CreateGroupChanelOneByOne,
  handleInputReplyChange,
  handleSendMessage,
  connectSendBird,
  handleInitSendBirdSuccess,
  getPreviousMessages,
  createNewChannel,
  receiveMessage,
  handleAddNewChannel,
  getUsers,
  handleSelectUserToChat,
  handleDeleteChannel,
  handleSelectChannel
} from '../modules/messages'
import {
   handleUpdateWhenRecieveMessage,
   handleUpdateWhenChannelChange } from '../../../store/notifications'
import Messages from '../components/messages'

const mapDispatchToProps = {
  CreateGroupChanelOneByOne,
  handleInputReplyChange,
  handleSendMessage,
  connectSendBird,
  handleInitSendBirdSuccess,
  getPreviousMessages,
  createNewChannel,
  receiveMessage,
  handleAddNewChannel,
  getUsers,
  handleSelectUserToChat,
  handleSelectChannel,
  handleDeleteChannel,
  handleUpdateWhenRecieveMessage,
  handleUpdateWhenChannelChange
}

const mapStateToProps = (state) => {
  return {
    ...state.messages,
    notifications: state.notifications,
    user: state.user
  }
}

export default connectWithLifecycle(
  mapStateToProps, mapDispatchToProps
)(Messages)
