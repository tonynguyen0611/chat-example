import React from 'react'
import PropTypes from 'prop-types'
import CollectionsList from '../../../components/collections_list'
import './collection_result.scss'

const loadingImage = <img src='https://cdn.zenquiz.net/static/Assets/loading-animation.gif' alt='Loading...' />

class CollectionResult extends React.Component {
  componentWillMount () {
    let searchKey = this.props.match.params.searchKey
    this.props.getCollectionResultList(searchKey)
  }

  render () {
    let _this = this
    let searchKey = this.props.match.params.searchKey
    return (
      <div className='search-container collection-result'>
        <div className='search-part collection-part'>
          <div className='search-collection-title'>
            Collections
          </div>
          <div className='search-content'>
            {
              _this.props.isLoadingCollectionList ? loadingImage
              : <CollectionsList collectionsList={_this.props.collectionList} numOfCol={4} />
            }
          </div>
          <div className='load-more' onClick={() => _this.props.getCollectionResultList(searchKey)}>
            { _this.props.isLoadingMore ? loadingImage : 'Load more' }
          </div>
        </div>
      </div>
    )
  }
}

CollectionResult.propTypes = {
  match: PropTypes.object.isRequired,
  getCollectionResultList: PropTypes.func
}

export default CollectionResult
