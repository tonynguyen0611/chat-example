import axios from 'axios'

export function upLoadImage (file) {
  return new Promise(function (resolve, reject) {
    let urlImg = URL.createObjectURL(file)
    let img = new Image()
    img.onload = () => {
      let rootImage = {
        x1: 0,
        y1: 0,
        w: img.width,
        h: img.height
      }
      let base64RootImage = doCrop(img, rootImage, file.type, 1, 1)

      let bufRootImage = new Buffer(base64RootImage.replace(/^data:image\/\w+;base64,/, ''), 'base64')
      processUpload(file, bufRootImage)
        .then(values => {
          resolve(values)
        })
        .catch(err => {
          reject(err)
        })
    }
    img.src = urlImg
  })
}
export function upLoadImageThumbnail (file) {
  return new Promise(function (resolve, reject) {
    let urlImg = URL.createObjectURL(file)
    let img = new Image()
    img.onload = () => {
      let smallThumbnail = {
        x1: 0,
        y1: 0,
        w: 100,
        h: 100 * img.height / img.width
      }
      let Thumbnail = {
        x1: 0,
        y1: 0,
        w: img.width,
        h: img.height
      }
      let rootImage = {
        x1: 0,
        y1: 0,
        w: img.width,
        h: img.height
      }
      let base64SmallThumbnail = doCrop(img, smallThumbnail, file.type, 1, 1)
      let base64Thumbnail = doCrop(img, Thumbnail, file.type, 0.8, 1)
      let base64RootImage = doCrop(img, rootImage, file.type, 1, 1)

      let bufSmallThumbnail = new Buffer(base64SmallThumbnail.replace(/^data:image\/\w+;base64,/, ''), 'base64')
      let bufThumbnail = new Buffer(base64Thumbnail.replace(/^data:image\/\w+;base64,/, ''), 'base64')
      let bufRootImage = new Buffer(base64RootImage.replace(/^data:image\/\w+;base64,/, ''), 'base64')
      Promise.all([processUpload(file, bufRootImage),
        processUpload(file, bufThumbnail),
        processUpload(file, bufSmallThumbnail)
      ])
        .then(values => {
          resolve(values)
        })
        .catch(err => {
          reject(err)
        })
    }
    img.src = urlImg
  })
}
export function processUpload (file, buf) {
  return new Promise(function(resolve, reject) {
    let url = ENVIRONMENT.API_DOMAIN + '/media/s3_sign_request'
    axios.post(encodeURI(url),
      {
        fileName: file.name,
        contentType: file.type
      },
      { withCredentials: true })
      .then(res => {
        uploadFile(res.data.signed_request, buf, file.type)
          .then(() => resolve(res.data.url))
          .catch(err => reject(err))
      })
      .catch(err => reject(err))
  })
}

function uploadFile (signedRequest, buf, fileType) {
  return new Promise(function (resolve, reject) {
    axios.put(signedRequest, buf,
      { headers: {
        'Content-Type': fileType,
        'ContentEncoding': 'base64'
      }
      })
      .then(res => resolve(res))
      .catch(err => reject(err))
  })
}
function doCrop (image, coords, type, quality, ratio) {
  let c = document.createElement('canvas')
  let ctx = c.getContext('2d')
  let x, y, w, h
  if (ratio) {
    x = coords.x1 * ratio
    y = coords.y1 * ratio
    w = coords.w * ratio
    h = coords.h * ratio
  } else {
    x = coords.x1
    y = coords.y1
    w = coords.w
    h = coords.h
  }
  c.width = w; c.height = h
  ctx.clearRect(0, 0, w, h)
  ctx.drawImage(image, x, y, w, h)
  return c.toDataURL(type, quality)
}
