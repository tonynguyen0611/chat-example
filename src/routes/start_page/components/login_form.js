import React from 'react'
import PropTypes from 'prop-types'
import { Redirect, Link } from 'react-router-dom'
import GoogleLogin from 'react-google-login'
import $ from 'jquery'
import './login_form.scss'
import RegisterEmailModal from '../modals/register_email_modal'

const googleButtonText = (
  <div>
    <i className='fa fa-google-plus' aria-hidden='true' />
    <span> Log In with Google+ </span>
  </div>
)

const facebookButtonText = (
  <div>
    <i className='fa fa-facebook' aria-hidden='true' />
    <span> Log In with Facebook </span>
  </div>
)

const loadingImg = (
  <img src='https://cdn.zenquiz.net/static/Assets/loading-animation.gif' alt='Loading' />
)

class LoginForm extends React.Component {

  render () {
    let _this = this

    const { from } = location.state || { from: { pathname: '/' } }
    if (_this.props.isLoginSuccess || (_this.props.userInfo && _this.props.userInfo.id)) {
      return <Redirect to={from} />
    }

    const Modal = RegisterEmailModal({
      socialEmail: _this.props.thissocialEmail,
      isNormalLoading: _this.props.isNormalLoading,
      isLoginError: _this.props.isLoginError,
      errorMessage: _this.props.errorMessage,
      loadingImg: loadingImg
    },
    _this.props.setModalState,
    _this.props.handleInputValueChange,
    _this.props.registerSocialEmail)
    return (
      <div className='login-form' onKeyPress={(e) => _this.props.handleOnKeyChange(e)}>
        { _this.props.isShowModal && Modal }
        <div className={'input-group ' + ((_this.props.isNormalLoading || _this.props.isSocialFacebookLoading || _this.props.isSocialGoogleLoading) ? 'lock' : '')}>
          <input className='form-control' placeholder='Email' type='email'
            value={_this.props.email} onChange={(e) => _this.props.handleInputValueChange(e, 'email')} />
          <input className='form-control' placeholder='Password' type='password'
            value={_this.props.password} onChange={(e) => _this.props.handleInputValueChange(e, 'password')} />
          <div className='forgot-password-container'>
            <Link to='/start/forgot-password' className='link'> Forgot password? </Link>
          </div>
          <button className='btn btn-block btn-start btn-start-gray' onClick={_this.props.login}>
            {!_this.props.isNormalLoading ? 'Log In' : loadingImg}
          </button>
          {_this.props.isLoginError &&
            <h5 id='errorLoginMsg'> {_this.props.errorMessage} </h5>
          }
          <div className='or'> <div className='text'> OR </div> </div>
          <button className='btn btn-block btn-start btn-facebook btn-social' onClick={_this.props.signinWithFb}>
            { _this.props.isSocialFacebookLoading ? loadingImg : facebookButtonText}
          </button>
          <GoogleLogin
            className='btn btn-block btn-start btn-google btn-social'
            clientId='86848834463-0l584umv3s3bbhodaifpv0kph722epqb.apps.googleusercontent.com'
            buttonText={_this.props.isSocialGoogleLoading ? loadingImg : googleButtonText}
            onSuccess={_this.props.signinWithGoogle}
            onFailure={_this.props.signinWithGoogle}
          />
          <Link to='/start/register' className='link'> Don't have an account? Sign up here </Link>
        </div>
      </div>
    )
  }
}

$(document).ready(function () {
  $.ajaxSetup({ cache: true })
  $.getScript('//connect.facebook.net/en_US/sdk.js', () => {
    FB.init({
      appId: '261644474349828',
      version: 'v2.8'
    })
    $('#loginbutton,#feedbutton').removeAttr('disabled')
    FB.getLoginStatus(response => {
      console.log('fb response', response)
    })
  })
})

LoginForm.propTypes = {
  isNormalLoading: PropTypes.bool,
  isSocialGoogleLoading: PropTypes.bool,
  isSocialFacebookLoading: PropTypes.bool,
  isLoginError: PropTypes.bool,
  isShowModal: PropTypes.bool,
  errorMessage: PropTypes.string,
  email: PropTypes.string,
  password: PropTypes.string,
  login: PropTypes.func,
  handleInputValueChange: PropTypes.func,
  signinWithFb: PropTypes.func,
  signinWithGoogle: PropTypes.func,
  userInfo: PropTypes.object,
  location: PropTypes.object,
  isLoginSuccess: PropTypes.bool,
  userSocialInfo: PropTypes.object,
  setModalState: PropTypes.func,
  socialEmail: PropTypes.string,
  registerSocialEmail: PropTypes.func
}

export default LoginForm
