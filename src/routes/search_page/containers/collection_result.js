import { connectWithLifecycle } from 'react-lifecycle-component'
import { getCollectionResultList } from '../modules/collection_result'
import CollectionResult from '../components/collection_result'

const mapDispatchToProps = {
  getCollectionResultList
}

const mapStateToProps = (state) => {
  return state.collectionResult
}

export default connectWithLifecycle(
  mapStateToProps, mapDispatchToProps
)(CollectionResult)
