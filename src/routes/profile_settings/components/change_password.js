import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import './change_password.scss'

class AccountSettings extends React.Component {
  componentWillMount () {
    this.props.initialize()
  }

  render () {
    let _this = this
    console.log('isSocialAccount', _this.props.isSocialAcount)
    console.log('>>> ', _this.props.isChangePasswordLoading)
    return (
      <div className='col-md-8 side-bar-right'>
        <div className={'profile-details change-password ' + (_this.props.isChangePasswordLoading ? 'change-password-lock' : '')}>
          <div className='header-block'>
            <h5 className='title'>
              Change Password
            </h5>
          </div>
          <div className='content-block'>
            <div className='row'>
              <div className='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
                <div className='form-group label-floating'>
                  <label className='control-label'>Confirm current password</label>
                  <div>
                    <input
                      placeholder=''
                      type='password'
                      value={_this.props.currentPassword}
                      onChange={(e) => _this.props.handleInputValueChange(e, 'currentPassword')} />
                  </div>
                </div>
              </div>
            </div>
            <div className='row'>
              <div className='col-lg-6 col-md-6 col-sm-12 col-xs-12'>
                <div className='form-group label-floating'>
                  <input
                    className='pa-2'
                    placeholder='Your new password'
                    type='password'
                    value={_this.props.newPassword}
                    onChange={(e) => _this.props.handleInputValueChange(e, 'newPassword')} />
                </div>
              </div>
              <div className='col-lg-6 col-md-6 col-sm-12 col-xs-12'>
                <div className='form-group label-floating'>
                  <input
                    className='pa-2'
                    placeholder='Confirm new password'
                    type='password'
                    value={_this.props.confirmNewPassword}
                    onChange={(e) => _this.props.handleInputValueChange(e, 'confirmNewPassword')} />
                </div>
              </div>
            </div>
            <div className='row'>
              <div className='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
                <Link to='/start/forgot-password'>Forgot my password</Link>
              </div>
            </div>
            <div className='row'>
              <div className='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
                <button className='btn btn-lg btn-bottom btn-right' onClick={_this.props.submit}>
                  {
                    _this.props.isChangePasswordLoading
                    ? <img src='https://cdn.zenquiz.net/static/Assets/loading-animation.gif' alt='Loading...' />
                    : 'Change password'
                  }
                </button>
              </div>
            </div>
            <div className='messages'>
              <p className='error'> { _this.props.isError && _this.props.errorMessage } </p>
              <p className='success'> { _this.props.isSuccess && _this.props.successMessage } </p>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

AccountSettings.propTypes = {
  isChangePasswordLoading: PropTypes.bool.isRequired
}
export default AccountSettings
