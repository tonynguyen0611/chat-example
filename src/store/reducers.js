import { combineReducers } from 'redux'
import locationReducer from './location'
import NotificationsReducer from './notifications'
import userReducer from '../routes/user_view/modules/header'
import MessageReducer from '../routes/profile_settings/modules/messages'
import { routerReducer } from 'react-router-redux'

export const makeRootReducer = (asyncReducers) => {
  return combineReducers({
    location: locationReducer,
    user: userReducer,
    router: routerReducer,
    notifications: NotificationsReducer,
    messages: MessageReducer,
    ...asyncReducers
  })
}

export const injectReducer = (store, { key, reducer }) => {
  if (Object.hasOwnProperty.call(store.asyncReducers, key)) return

  store.asyncReducers[key] = reducer
  store.replaceReducer(makeRootReducer(store.asyncReducers))
}

export default makeRootReducer
