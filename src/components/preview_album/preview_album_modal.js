import React from 'react'
import { Gateway } from 'react-gateway'
import { Dialog } from 'material-ui'
import DetailsCollection from './preview_album'
import injectTapEventPlugin from 'react-tap-event-plugin'
injectTapEventPlugin()
const customContentStyle = {
  width: '80%',
  minHeight: '80%',
  maxWidth: 'none',
  maxHeight: 'none',
  borderRadius: '10px !important'
}

let DetailsCollectionModal = function (options, onCancel) {
  options = options || {}
  return (
    <Gateway into='confirm-modal'>
      <Dialog
        modal={false}
        contentStyle={customContentStyle}
        open={options.isOpen}
        onRequestClose={onCancel}
      >
        <DetailsCollection
          collection={options.collection}
          mediaItem={options.mediaItem}
          onCancel={onCancel} />
      </Dialog>
    </Gateway>
  )
}

export default DetailsCollectionModal
