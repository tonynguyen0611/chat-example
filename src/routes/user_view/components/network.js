import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import './network.scss'

class UserViewNetwork extends React.Component {

  componentWillReceiveProps (nextProps) {
    let currentPath = this.props.location.pathname
    let nextPath = nextProps.location.pathname
    let nextLocationSplit = nextProps.location.pathname.split('/')
    let nextDir = nextLocationSplit[nextLocationSplit.length - 1]

    this.props.setCurrentDir(nextDir, nextProps.match.params.userId)

    if (nextPath !== currentPath) {
      this.props.getNetworkList(nextProps.match.params.userId, nextDir)
    }
  }

  componentWillMount () {
    let locationSplit = this.props.location.pathname.split('/')
    let dir = locationSplit[locationSplit.length - 1]
    this.props.setCurrentDir(dir, this.props.match.params.userId)
    this.props.getNetworkList(this.props.match.params.userId, dir)
  }

  render () {
    let _this = this
    let currentDir = this.props.dir
    let networkList = this.props.networkList
    return (
      <div className='col-lg-12 col-md-12 col-sm-12 col-xs-12 user-view-network'>
        <div className='network-container'>
          {
            (currentDir === 'search') &&
            <div className='search-bar-container'>
              <div className='search-bar'>
                <input
                  type='text'
                  placeholder='Search'
                  onChange={(e) => _this.props.handleInputValueChange(e, 'searchKey')}
                  onKeyPress={(e) => _this.props.handleKeyPress(e)} />
              </div>
              <div className='suggestion-tag'>
                <div className='suggestion-item'>
                  <span className='suggestion-type'>Photographers</span>
                  <span className='suggestion-count'>(111)</span>
                </div>
                <div className='suggestion-item'>
                  <span className='suggestion-type'>Stylists</span>
                  <span className='suggestion-count'>(93)</span>
                </div>
                <div className='suggestion-item'>
                  <span className='suggestion-type'>Models</span>
                  <span className='suggestion-count'>(75)</span>
                </div>
                <Link className='expand-btn' to='#'> More </Link>
              </div>
            </div>
          }
          <div className='search-result'>
            {
              !_this.props.isLoadingList && networkList &&
              networkList.map((item, index) => {
                return (
                  <div className='row result-item' key={item.id}>
                    <div className='col-lg-4 col-md-4 col-sm-12 col-xs-12 result-ava'>
                      <Link to={'/user-view/' + item.id + '/collection'}>
                        <img src={item.profile_avatar.url ?
                        item.profile_avatar.url : 'http://ocotur-dev.s3.amazonaws.com/static/assets/ava.jpg'} />
                      </Link>
                    </div>
                    <div className='col-lg-4 col-md-4 col-sm-12 col-xs-12 result-info'>
                      <div className='info-item'>
                        <Link to={'/user-view/' + item.id + '/collection'}>
                          <div className='info-name'> {item.full_name} </div>
                        </Link>
                        <div className='info-title'>
                          { item.occupation ? item.occupation[0] : '' }
                        </div>
                        {
                          item.location &&
                          <div className='info-location'>
                            {item.location.city }
                            {item.location.city && item.location.country && ', ' }
                            {item.location.country}
                          </div>
                        }
                      </div>
                    </div>
                    {
                      item.isFollowing &&
                      <div className='col-lg-4 col-md-4 col-sm-12 col-xs-12 result-btn'>
                        <button
                          className='following-btn'
                          onClick={() => _this.props.handleNetworkAction(item, 'unfollow')}>
                          Following
                        </button>
                      </div>
                    }
                    {
                      !item.isFollowing &&
                      <div className='col-lg-4 col-md-4 col-sm-12 col-xs-12 result-btn'>
                        <button
                          className='follow-btn'
                          onClick={() => _this.props.handleNetworkAction(item, 'follow')}>
                          Follow
                        </button>
                      </div>
                    }
                  </div>
                )
              })
            }
            {
              _this.props.isLoadingList &&
              <img src='https://cdn.zenquiz.net/static/Assets/loading-animation.gif' alt='Loading...' />
            }
          </div>
        </div>
      </div>
    )
  }
}

UserViewNetwork.propTypes = {
  match: PropTypes.object.isRequired,
  networkList: PropTypes.array.isRequired,
  location: PropTypes.object.isRequired,
  dir: PropTypes.string.isRequired,
  setCurrentDir: PropTypes.func.isRequired,
  getNetworkList: PropTypes.func.isRequired
}
export default UserViewNetwork
