import { connectWithLifecycle } from 'react-lifecycle-component'
import {
  getCollections,
  handleChangeAvatar,
  handleCancelChangeAvatar,
  handleUpdateAvatar,
  getUserInfo,
  getLastCareerItem,
  getSimilarList
} from '../modules/collection'
import Collection from '../components/collection'

const mapDispatchToProps = {
  getUserInfo,
  getCollections,
  handleChangeAvatar,
  handleCancelChangeAvatar,
  handleUpdateAvatar,
  getLastCareerItem,
  getSimilarList
}
const mapStateToProps = (state) => {
  return state.collection
}

export default connectWithLifecycle(
  mapStateToProps, mapDispatchToProps
)(Collection)
