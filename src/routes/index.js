import Home from '../routes/home'
import ProfileSettings from '../routes/profile_settings'
import UserView from '../routes/user_view'
import Collection from '../routes/collections'
import StartPage from '../routes/start_page'
import SearchPage from '../routes/search_page'
import ExplorePage from '../routes/explore'

const routes = [{
  path: '/start',
  component: StartPage,
  layout: 'StartLayout'
}, {
  path: '/',
  component: Home,
  layout: 'default',
  exact: true
}, {
  path: '/profile',
  component: ProfileSettings,
  layout: 'default',
  exact: false
}, {
  path: '/user-view/:userId',
  component: UserView,
  layout: 'default',
  exact: false
}, {
  path: '/search/:searchKey',
  component: SearchPage,
  layout: 'default',
  exact: false
}, {
  path: '/explore',
  component: ExplorePage,
  layout: 'default',
  exact: false
}, {
  path: '/collections',
  component: Collection,
  layout: 'default',
  exact: false
}]

export default routes
