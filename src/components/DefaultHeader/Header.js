import React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import './Header.scss'
import logo from './assets/logo.png'
import connectionIcon from './assets/connection.png'
import messagesIcon from './assets/messages.png'
import notiIcon from './assets/noti.png'
import ava from './../../assets/static/ava.jpg'
import $ from 'jquery'
import { connectSendBird } from '../../routes/profile_settings/modules/messages'
import { sendLogOutRequest } from '../../routes/start_page/modules/login'
import { handleSelectedSenderMessages, getCollaborationsRequest, handleUpdateWhenChannelChange } from '../../store/notifications'
import SendBird from 'sendbird'
import moment from 'moment'
let sb = null

$(document).ready(() => {
  let pathSplit = location.pathname.split('/')
  let searchKey = pathSplit[pathSplit.length - 1]
  if (pathSplit.indexOf('search') >= 0) {
    $('.search-input').val(searchKey)
  }
})

$(document).click((e) => {
  if (e.target.id !== 'noti-menu' || e.target.id !== 'user-menu' || e.target.id !== 'msg-menu') {
    $('#noti-menu').hide()
    $('#user-menu').hide()
    $('#msg-menu').hide()
  }
})

class Header extends React.Component {
  toggleDropdown = (item) => {
    let displayMenu = $('#' + item).css('display')
    let action = (displayMenu === 'none') ? 'block' : 'none'
    $('#' + item).css('display', action)
  }
  hideDropdown = () => {
    console.log('hide dropdown')
    $('#user-menu').css('display', 'none')
  }
  componentWillMount () {
    sb = SendBird.getInstance()
    this.props.connectSendBird(sb)
    this.props.getCollaborationsRequest(this.props.user.currentUser)
    const ChannelHandler = new sb.ChannelHandler()
    ChannelHandler.onChannelChanged = (channel) => {
      console.log('channel change', channel)
      this.props.handleUpdateWhenChannelChange(channel)
    }
    sb.addChannelHandler('ChatNoti', ChannelHandler)
  }
  handleOnKeyChange = (e) => {
    if (e.key === 'Enter') {
      this.searchWithKey()
    }
  }
  searchWithKey = () => {
    let searchKey = $('.search-input').val()
    console.log('search with key', searchKey)
    window.location.replace('/search/all/' + searchKey)
  }
  toHowLongAgo = (input) => {
    let date = moment(input)
    return date.fromNow()
  }
  render () {
    let currentUser = this.props.user.currentUser
    let listMessages = this.props.notifications.messages.listMessages
    listMessages.sort((a, b) => {
      if (!a.lastMessage && b.lastMessage) {
        a.lastMessage = {
          createdAt: Date.now()
        }
      }
      if (a.lastMessage && !b.lastMessage) {
        b.lastMessage = {
          createdAt: Date.now()
        }
      }
      if (!a.lastMessage && !b.lastMessage) {
        return true
      }
      a.Priority = a.Priority ? a.Priority : 0
      b.Priority = b.Priority ? b.Priority : 0
      return (b.lastMessage.createdAt - a.lastMessage.createdAt + b.Priority * 10 - a.Priority * 10)
    })
    return (
      <nav className='default-header'>
        <div className='header-container'>
          <div className='brand-container header-inline'>
            <Link className='navbar-brand' to={'/'}>
              <img src={logo} />
            </Link>
          </div>
          <div className='search-container header-inline'>
            <div className='search-form'>
              <div className='input-group'>
                <input
                  type='text'
                  className='form-control search-input'
                  placeholder='Search'
                  onKeyPress={(e) => this.handleOnKeyChange(e)} />
                <div className='input-group-btn right'>
                  <button className='btn btn-default custom-search-btn' onClick={this.searchWithKey} >
                    <i className='glyphicon glyphicon-search' />
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className='notification-container header-inline'>
            <div className='notification-form'>
              <div className='noti-item'>
                <img src={connectionIcon} onClick={() => this.toggleDropdown('noti-menu')} />
                {this.props.notifications.collaborations.listCollaborations.length > 0 &&
                  <span className='notification'>
                    {this.props.notifications.collaborations.listCollaborations.length}
                  </span>
                }
                <div className='notification-dropdown g-dropdown col-noti' id='noti-menu'>
                  <div className='arrow-up arrow-right' />
                  {this.props.notifications.collaborations.listCollaborations.map((collaboration, i) => (
                    <div className='row' key={i}>
                      <div className='col-md-2'>
                        <img
                          className='img-responsive img-circle'
                          src={collaboration.user ? collaboration.user.profile_avatar.url : ava} alt='avatar' />
                      </div>
                      <div className='col-md-10'>
                        <span>Accept {collaboration.user_id}</span>
                      </div>
                    </div>
                  ))}
                </div>
              </div>
              <div className='noti-item'>
                <img src={messagesIcon} onClick={() => this.toggleDropdown('msg-menu')} />
                {
                  this.props.notifications.messages.count > 0 &&
                  <span className='notification'> {this.props.notifications.messages.count} </span>
                }
                <div className='notification-dropdown g-dropdown msg-noti' id='msg-menu'>
                  <div className='arrow-up arrow-right' />
                  <div className='notification-top'>
                    Messages
                  </div>
                  {listMessages.map((message, i) => (
                    <div key={i}>
                      {
                        message && message.lastMessage && message.memberCount > 1 &&
                        <div className={message.unreadMessageCount > 0 ? 'unread-message' : ''} >
                          {
                            message.members.map((user, i) => user.userId !== currentUser.id &&
                            <div className='row' onClick={() => this.props.handleSelectedSenderMessages(message.members[0].userId === currentUser.id ? message.members[1] : message.members[0])}>
                              <div className='col-md-2 ava-container'>
                                <img
                                  className='img-circle'
                                  src={message.members[0].userId === currentUser.id
                                  ? message.members[1].profileUrl ? message.members[1].profileUrl : ava
                                  : message.members[0].profileUrl ? message.members[0].profileUrl : ava}
                                  alt='avatar' />
                              </div>
                              <div className='col-md-10 msg-container'>
                                <div className='msg-user-name'>
                                  <b>{message.members[0].userId === currentUser.id ? message.members[1].nickname : message.members[0].nickname}</b>
                                </div>
                                <div className='msg-content'>
                                  {message.lastMessage.message}
                                </div>
                                {message.lastMessage &&
                                  <div className='message-item-status'>
                                    { this.toHowLongAgo(message.lastMessage.createdAt) }
                                  </div>
                                }
                              </div>
                            </div>
                          )}
                        </div>
                      }
                    </div>
                  ))}
                  {
                    listMessages.length === 0 &&
                    <div style={{ 'textAlign': 'center', 'padding': '10px' }}> No message </div>
                  }
                  <div className='notification-bottom'>
                    <Link to='/profile/messages'>See all</Link>
                  </div>
                </div>
              </div>
              <div className='noti-item'>
                <img src={notiIcon} />
                {false &&
                  <span className='notification'> 1 </span>
                }
              </div>
            </div>
          </div>
          <div className='user-container header-inline'>
            {currentUser &&
              <div>
                <div className='user-info'>
                  <Link to={'/user-view/' + currentUser.id + '/collection'}>
                    <div className='header-inline'>
                      <img className='user-ava'
                        src={currentUser.profile_avatar.url ? currentUser.profile_avatar.url : ava} />
                    </div>
                    <div className='user-info-name header-inline'>{currentUser.first_name.substring(0, 10)}</div>
                  </Link>
                </div>
                <div className='dropdown-i'>
                  <a className='a-caret' onClick={() => this.toggleDropdown('user-menu')}>
                    <span className='caret' />
                  </a>
                  <div className='dropdown-m g-dropdown' id='user-menu'>
                    <div className='dropdown-inside'>
                      <div className='arrow-up arrow-center' />
                      <div className='dropdown-item'>
                        <Link to='/collections/upload'>
                          <span>Upload</span>
                        </Link>
                      </div>
                      <div className='dropdown-item'>
                        <Link to='/profile/info'>
                          <span>Settings</span>
                        </Link>
                      </div>
                      <a href='#' onClick={this.props.sendLogOutRequest}>
                        <div className='dropdown-item'>Log out</div>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            }
          </div>
        </div>
      </nav>
    )
  }
}

Header.propTypes = {
  sendLogOutRequest: PropTypes.func.isRequired,
  connectSendBird: PropTypes.func.isRequired,
  handleSelectedSenderMessages: PropTypes.func.isRequired,
  getCollaborationsRequest: PropTypes.func.isRequired,
  handleUpdateWhenChannelChange: PropTypes.func.isRequired,
  notifications: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired
}

const mapDispatchToProps = {
  sendLogOutRequest,
  connectSendBird,
  handleSelectedSenderMessages,
  getCollaborationsRequest,
  handleUpdateWhenChannelChange
}
const mapStateToProps = (state) => ({
  user: state.user,
  notifications: state.notifications
})
export default connect(mapStateToProps, mapDispatchToProps)(Header)
