import { connectWithLifecycle } from 'react-lifecycle-component'
import { getSearchResultList } from '../modules/search_result'
import SearchResult from '../components/search_result'

const mapDispatchToProps = {
  getSearchResultList
}

const mapStateToProps = (state) => {
  return state.searchResult
}

export default connectWithLifecycle(
  mapStateToProps, mapDispatchToProps
)(SearchResult)
