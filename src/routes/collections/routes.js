import { injectReducer } from '../../store/reducers'

const CreateContainer = (store) => {
  const CreateContainer = require('./containers/create_collection').default
  const reducer = require('./modules/create_collection').default
  /*  Add the reducer to the store  */
  injectReducer(store, { key: 'createCollection', reducer })
  return CreateContainer
}

export const CreateRoute = (store) => ({
  path: '/collections/upload',
  component: CreateContainer(store)
})

export const EditRoute = (store) => ({
  path: '/collections/edit/:collectionId',
  component: CreateContainer(store)
})
