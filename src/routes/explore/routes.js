import React from 'react'
import { injectReducer } from '../../store/reducers'

const CollectionResultContainer = (store) => {
  const reducer = require('./modules/collection_result').default
  const CollectionResult = require('./containers/collection_result').default
  injectReducer(store, { key: 'explore', reducer })
  return CollectionResult
}

export const CollectionResultRoute = (store) => ({
  path: '/explore',
  component: CollectionResultContainer(store)
})

