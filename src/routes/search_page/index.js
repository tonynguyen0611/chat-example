import React from 'react'
import { Route } from 'react-router-dom'
import { SearchResultRoute, PeopleResultRoute, CollectionResultRoute } from './routes'
import './index.scss'

export const SearchPage = ({ store, match }) => {
  const searchKey = match.params.searchKey
  const routes = [
    SearchResultRoute(store),
    PeopleResultRoute(store),
    CollectionResultRoute(store)
  ]
  return (
    <div className='col-lg-12 col-md-12 col-sm-12 col-xs-12 search-page'>
      {routes.map((route, i) => (
        <RouteWithSubRoutes key={i} {...route} searchKey={searchKey} />
      ))}
    </div>
  )
}

const RouteWithSubRoutes = (route) => {
  return (
    <Route path={route.path} render={props => (
      <route.component {...props} routes={route.routes} searchKey={route.searchKey} />
    )} />
  )
}

export default SearchPage
