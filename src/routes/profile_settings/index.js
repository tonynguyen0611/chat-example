import React from 'react'
import { Route } from 'react-router-dom'
import { AccountSettingsRoute, ProfileInfoRoute, ChangePasswordRoute, EduExpRoute, MessagesRoute, HeaderRoute } from './routes'
import './index.scss'

export const ProfileSettings = ({ store }) => {
  const routes = [
    HeaderRoute(store),
    AccountSettingsRoute(store),
    ProfileInfoRoute(store),
    ChangePasswordRoute(store),
    EduExpRoute(store),
    MessagesRoute(store)
  ]
  return (
    <div className='profile-view'>
      {routes.map((route, i) => (
        <RouteWithSubRoutes key={i} {...route} />
      ))}
    </div>
  )
}

const RouteWithSubRoutes = (route) => {
  return (
    <Route path={route.path} render={props => (
      <route.component {...props} routes={route.routes} />
    )} />
  )
}

export default ProfileSettings
