import React from 'react'
import PropTypes from 'prop-types'
import './create_collection.scss'

let timeout = null
const ImageObject = ({
  value,
  handleImageDescChange,
  removeItem,
  handleInputSearchChange,
  userList,
  handleAddCollaborators,
  handleStartAddCollaborators
}) => {
  return (
    <div className='col-md-4'>
      <div className='item'>
        {value.isUploading ? (
          <img
            className='img-responsive'
            src='http://ocotur-dev.s3.amazonaws.com/static/assets/loading-animation.gif' />
        ) : (
          <img className='img-responsive' src={value.images[1].source} />
        )}
        <div className='desc col-md-12'>
          <input type='text' placeholder='Write something about this piece' value={value.description}
            onChange={(e) => handleImageDescChange(value.index, e)} />
        </div>
        <div className='delete-item' onClick={() => removeItem(value.index)} >
          <i className='fa fa-times' aria-hidden='true' />
        </div>
        <div className='img-footer col-md-12' onClick={() => handleStartAddCollaborators(value.index)}>
          <i className='fa fa-user-circle-o' aria-hidden='true' />
          {value.collaborations &&
            <div>
              {value.collaborations.map((collaborator, i) => i < 2
              ? (
                <span> {collaborator.full_name} </span>
              )
              : i === 2 ? <span> ... </span> : ''
              )
              }
            </div>
          }
        </div>
      </div>
      {userList && value.isAddingCollaborators &&
      <div className='search-collaborators' >
        <div className='search-area'>
          <input className='input-search'
            type='text'
            onChange={handleInputSearchChange}
            placeholder='type email or nick name here...'
            />
        </div>
        <div className='search-result'>
          {userList.map((user, i) => (
            <div className='row' key={i} onClick={() => handleAddCollaborators(value.index, user)}>
              <div className='col-md-3'>
                <img src={user.profile_avatar.url} className='img-responsive img-circle' height='30' />
              </div>
              <div className='col-md-9'>
                <span>{user.full_name}</span>
              </div>
            </div>
          ))}
        </div>
      </div>
      }
    </div>
  )
}
class CreateCollectionView extends React.Component {
  componentWillMount () {
    let conditions = {}
    let query = {
      offset: 0,
      limit: 6
    }
    this.props.getUsers(query, conditions)
    if (this.props.match.params.collectionId) {
      this.props.getCollection(this.props.match.params.collectionId)
    }
  }
  handleInputSearchChange = (e) => {
    e.persist()
    let value = e.target.value
    let query = {
      offset: 0,
      limit: 6
    }
    let conditions = {
      $or: [
        {
          email: {
            $iLike: '%' + value + '%'
          }
        }, {
          full_name: {
            $iLike: '%' + value + '%'
          }
        }
      ]
    }
    clearTimeout(timeout)
    timeout = setTimeout(() => {
      this.props.getUsers(query, conditions)
    }, 500)
  }
  render () {
    return (
      <div className='row CreateCollection'>
        <div className='main col-md-8 col-centered'>
          <h3>Create Collection</h3>
          <hr />
          <div className='Content row'>
            <div className='content-header col-md-11 col-centered'>
              <h4>Name</h4>
              <input
                type='text'
                value={this.props.title}
                placeholder='title'
                onChange={this.props.handleCollectionTitleChange} />
            </div>
            <div className='content-body col-md-12 col-centered'>
              <div className='col-md-4'>
                <div className='item'>
                  <a className='a-item' rel='ignore'>
                    <div className='add-section'>
                      <input name='file'
                        type='file'
                        accept='image/*'
                        onChange={this.props.handelUploadImages} multiple
                        className='inputImage' />
                      <i className='fa fa-plus-circle' aria-hidden='true' /> <br />
                      <p>Add more photos</p>
                    </div>
                  </a>
                </div>
              </div>
              {this.props.imageList.map((value, i) => (
                <ImageObject
                  key={i}
                  value={value}
                  removeItem={this.props.removeItem}
                  userList={this.props.userList}
                  handleImageDescChange={this.props.handleImageDescChange}
                  handleStartAddCollaborators={this.props.handleStartAddCollaborators}
                  handleAddCollaborators={this.props.handleAddCollaborators}
                  handleInputSearchChange={this.handleInputSearchChange} />
              ))
              }
            </div>
            <div className='content-footer col-md-12'>
              {!this.props.isLoading && !this.props.isGroupUpload &&
                <div className='col-md-6'>
                  <button
                    className='btn col-md-5 btn-discard'
                    onClick={() => {
                      this.props.history.push('/user-view/' + this.props.collection.user_id + '/collection')
                    }}
                  >
                    Discard
                  </button>
                </div>
              }
              <div className='col-md-6'>
                {this.props.isLoading || this.props.isGroupUpload ? (
                  <img className='img-responsive' src='https://cdn.zenquiz.net/static/Assets/loading-animation.gif' />
                ) : (
                  <button className='btn col-md-5 btn-publish' onClick={this.props.publishCollection}>
                    <span>Publish collection</span>
                  </button>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

CreateCollectionView.propTypes = {
  isGroupUpload: PropTypes.bool.isRequired,
  isLoading: PropTypes.bool.isRequired,
  createAlbumSuccess: PropTypes.bool.isRequired,
  imageList: PropTypes.array,
  handelUploadImages: PropTypes.func,
  handleImageDescChange: PropTypes.func,
  publishCollection: PropTypes.func,
  handleCollectionTitleChange: PropTypes.func,
  removeItem: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired,
  getUsers: PropTypes.func.isRequired,
  userList: PropTypes.array.isRequired,
  handleStartAddCollaborators: PropTypes.func.isRequired,
  handleAddCollaborators: PropTypes.func.isRequired,
  getCollection: PropTypes.func.isRequired,
  match: PropTypes.object,
  collection: PropTypes.object,
  title: PropTypes.string
}

ImageObject.propTypes = {
  value: PropTypes.object,
  handleImageDescChange: PropTypes.func.isRequired,
  removeItem: PropTypes.func.isRequired,
  handleInputSearchChange: PropTypes.isRequired,
  userList: PropTypes.array,
  handleAddCollaborators: PropTypes.func.isRequired,
  handleStartAddCollaborators: PropTypes.func.isRequired
}

export default CreateCollectionView
