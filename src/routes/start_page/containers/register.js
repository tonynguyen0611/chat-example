import { connectWithLifecycle } from 'react-lifecycle-component'
import Register from '../components/register'
import { handleInputValueChange, submitForm, onRegisterSuccess } from '../modules/register'

const mapDispatchToProps = {
  handleInputValueChange,
  submitForm,
  onRegisterSuccess
}

const mapStateToProps = (state) => {
  return state.newUser
}

export default connectWithLifecycle(
  mapStateToProps, mapDispatchToProps
)(Register)
