import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import tempAva from './../../../assets/static/ava.jpg'

const loadingImage = <img src='https://cdn.zenquiz.net/static/Assets/loading-animation.gif' alt='Loading...' />

class PeopleResult extends React.Component {

  componentWillMount () {
    let searchKey = this.props.match.params.searchKey
    this.props.getPeopleResultList(searchKey)
  }

  render () {
    let _this = this
    let peopleList = this.props.peopleList || []
    let searchKey = this.props.match.params.searchKey
    return (
      <div className='search-container people-result'>
        <div className='search-part people-part disp-small'>
          <div className='search-title'>
            People
          </div>
          <div className='search-content'>
            {
              _this.props.isLoadingPeopleList ? loadingImage
              : <div>
                {
                  (peopleList.length === 0) ? 'No result'
                  : <div>
                    {
                      peopleList.map((item, index) => {
                        return (
                          <Link key={item.id} to={'/user-view/' + item.id + '/collection'}>
                            <div key={item.id} className='people-item'>
                              <div className='people-cover-container'>
                                {
                                  item.profile_cover && item.profile_cover.url &&
                                  <img src={item.profile_cover.url} className='people-cover-img' />
                                }
                              </div>
                              <div className='people-ava-container'>
                                <img src={item.profile_avatar.url ? item.profile_avatar.url : tempAva} className='people-ava-img' />
                              </div>
                              <div className='people-info'>
                                <div className='people-info-item'>{item.full_name}</div>
                                <div className='people-info-item'><b>{item.occupation[0] ? item.occupation[0] : ''}</b></div>
                              </div>
                            </div>
                          </Link>
                        )
                      })
                    }
                    <div className='load-more' onClick={() => _this.props.getPeopleResultList(searchKey)}>
                      { _this.props.isLoadingMore ? loadingImage : 'Load more' }
                    </div>
                  </div>
                }
              </div>
            }
          </div>
        </div>
      </div>
    )
  }
}

PeopleResult.propTypes = {
  peopleList: PropTypes.array.isRequired,
  match: PropTypes.object.isRequired,
  getPeopleResultList: PropTypes.func
}

export default PeopleResult
