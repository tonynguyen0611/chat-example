/**
 * @author ninh
 * @version 0.0.1
 * @date Sep - 08 - 2017
 *
 * @flow
 */

import fetch from './fetch'

export function getCollection (collectionId) {
  const url = '/albums/' + collectionId
  const options = {
    method: 'GET'
  }
  return fetch(url, options)
}
export function updateCollection (collectionId, data) {
  const url = '/albums/' + collectionId
  const options = {
    method: 'PUT',
    data: data
  }
  return fetch(url, options)
}
export function createCollection (data) {
  const url = '/albums'
  const options = {
    method: 'POST',
    data: data
  }
  return fetch(url, options)
}
export function deleteMediaItem (MediaItemId) {
  const url = '/media_items/' + MediaItemId
  const options = {
    method: 'DELETE'
  }
  return fetch(url, options)
}
export function updateMediaItem (MediaItemId, data) {
  const url = '/media_items/' + MediaItemId
  const options = {
    method: 'PUT',
    data: data
  }
  return fetch(url, options)
}
export function createMediaItem (data) {
  const url = '/media_items'
  const options = {
    method: 'POST',
    data: data
  }
  return fetch(url, options)
}
export default {
  getCollection,
  updateCollection,
  createCollection,
  createMediaItem,
  updateMediaItem,
  deleteMediaItem
}
