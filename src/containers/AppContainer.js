import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { MuiThemeProvider } from 'material-ui'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect,
  Switch
} from 'react-router-dom'
import { GatewayDest, GatewayProvider, Gateway } from 'react-gateway';
import {ConnectedRouter, push} from 'react-router-redux'
import {Provider} from 'react-redux'

import DefaultLayout from '../layouts/DefaultLayout'
import StartLayout from '../layouts/StartLayout'
import routes from '../routes'
import SendBird from 'sendbird'

const SENDBIRD_APP_ID = 'E085A795-4206-48CD-A598-09CCA56C6687'

let sb = new SendBird({
  appId: SENDBIRD_APP_ID
})

const RouteWithSubRoutes = (props) => {
  var route = props.route;
  var store = props.store;
  var layout = route.layout;
  if (layout == "StartLayout") {
    return (
      <StartLayout path={route.path} component={route.component} store={store} route={route}/>
    )
  } else {
    return (
      <DefaultLayout path={route.path} component={route.component} store={store} route={route} isLogin={hasLoggedIn} />
    )
  }
}
const hasLoggedIn = () => {
  var cachedUser;
  try {
    cachedUser = JSON.parse(localStorage.userinfo);
  } catch(err) {
    cachedUser = null;
  }
  return cachedUser && cachedUser.id;
}

class AppContainer extends Component {
  shouldComponentUpdate() {
    return false
  }

  render () {
    let muiTheme = getMuiTheme({
      palette: {
        primary1Color: "#4DC3CA",
        primary2Color: "#3b9fa4",
        primary3Color: "grey400",
        accent1Color: "#E47923",
        //accent2Color: "#C56619",
      },
      appBar: {
        height: 50,
      }
    })
    const {store, history} = this.props
    return (
      <MuiThemeProvider muiTheme={muiTheme}>
        <GatewayProvider>
          <Provider store={store}>
            <ConnectedRouter history={history}>
              <Switch>
                <Route render={() => {
                  return (
                    <div style={{height: '100%'}}>
                      {routes.map((route, i) => (
                        <RouteWithSubRoutes key={i} route={route} store={store}/>
                      ))}
                      <GatewayDest name="confirm-modal" className="modal-container"/>
                      <GatewayDest name="notification-modal" className="modal-container"/>
                      <GatewayDest name="snack-notification"/>
                    </div>
                  )
                }} />

              </Switch>
            </ConnectedRouter>
          </Provider>
        </GatewayProvider>
      </MuiThemeProvider>
    )
  }
}

AppContainer.propTypes = {
  store: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired
}
export default AppContainer
