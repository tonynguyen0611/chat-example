import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

export const Header = ({ currentUser }) => {
  return (
    <div className='col-md-3 side-bar-left'>
      <div className='menu-bar'>
        <ul>
          <Link to={'/user-view/' + currentUser.id + '/collection'}><li>Your Profile</li></Link>
          <hr />
          <li>Profile Settings</li>
          <hr />
          <Link to='/profile/info'><li>Profile Information</li></Link>
          <Link to='/profile/account_settings'><li>Account Settings</li></Link>
          <Link to='/profile/change_password'><li>Change Password</li></Link>
          <Link to='/profile/edu_exp'><li>Education and Experience</li></Link>
          <hr />
          <li>Notifications</li>
          <hr />
          <Link to='/profile/messages'><li>Message</li></Link>
          <Link to='/profile/network_request'><li>Network Requests</li></Link>
        </ul>
      </div>
    </div>
  )
}

Header.propTypes = {
  currentUser: PropTypes.object.isRequired
}
export default Header
