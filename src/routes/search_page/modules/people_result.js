import { sendRequestSearch } from '../../../helper/search'
// ------------------------------------
// Constants
// ------------------------------------
const IS_LOADING = 'IS_LOADING'
const SET_PEOPLE_LIST = 'SET_PEOPLE_LIST'
const IS_ERROR = 'IS_ERROR'
// ------------------------------------
// Actions
// ------------------------------------

export function getPeopleResultList (key) {
  return (dispatch, getState) => {
    let options = {}
    options.limit = 20
    options.offset = getState().peopleResult.offset
    options.searchType = 'users'
    options.conditions = {
      'full_name': {
        '$iLike': '%' + key + '%'
      }
    }

    if (options.offset === 0) {
      dispatch(setLoading('isLoadingPeopleList', true))
    } else {
      dispatch(setLoading('isLoadingMore', true))
    }
    sendRequestSearch(options)
    .then(res => {
      dispatch(setPeopleList(res))
      dispatch(setLoading('isLoadingPeopleList', false))
      dispatch(setLoading('isLoadingMore', false))
    })
    .catch(err => dispatch(setErrorMessage(err)))
  }
}

function setPeopleList (value) {
  console.log('set people', value)
  return {
    type: SET_PEOPLE_LIST,
    peopleList: value
  }
}

function setLoading (loadingType, value) {
  let loadingObj = {}
  loadingObj[loadingType] = value
  return {
    type: IS_LOADING,
    loadingObj: loadingObj
  }
}

function setErrorMessage (error) {
  return {
    type: IS_ERROR,
    errorMessage: error
  }
}
// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [IS_LOADING]: (state, action) => {
    return {
      ...state,
      ...action.loadingObj
    }
  },
  [IS_ERROR]: (state, action) => {
    return {
      ...state,
      isLoadingCollectionList: false,
      isLoadingPeopleList: false,
      isError: true,
      errorMessage: action.errorMessage
    }
  },
  [SET_PEOPLE_LIST]: (state, action) => {
    return {
      ...state,
      peopleList: [...state.peopleList, ...action.peopleList],
      offset:  state.peopleList.length + action.peopleList.length
    }
  }
}
// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  isLoadingPeopleList: false,
  isLoadingCollectionList: false,
  isLoadingMore: false,
  isError: false,
  isSuccess: false,
  errorMessage: '',
  successMessage: '',
  peopleList: [],
  collectionList: [],
  offset: 0
}

export default function peopleResultReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
