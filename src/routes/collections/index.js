import React from 'react'
import {
  BrowserRouter as Router,
  Route
} from 'react-router-dom'

import { CreateRoute, EditRoute } from './routes'

export const Collection = ({ store, match }) => {
  const userId = match.params.userId
  const routes = [
    CreateRoute(store),
    EditRoute(store)
  ]
  return (
    <div>
      {routes.map((route, i) => (
        <RouteWithSubRoutes key={i} {...route} userId={userId} />
      ))}
    </div>
  )
}

const RouteWithSubRoutes = (route) => {
  return (
    <Route path={route.path} render={props => (
      <route.component {...props} routes={route.routes} userId={route.userId} />
    )} />
  )
}

export default Collection
