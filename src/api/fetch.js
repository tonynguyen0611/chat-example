/*
* Author: ninh
* Email: ninh.uit@gmail.com
* Date created: 23/2/2017
* @flow
* Description: Wrapper function for fetching API. Please use as below:
* fetch(url, fetchOptions)
  .then(response => response.data)
  .catch(err => {
    err.response: statusCode falls out of range 2xx
    err.message: Network error
  });
*/

'use strict'

import axios from 'axios'

type fetchOptions = {
  method?: string; //Default: GET
  query?: Object; // URL query for GET method
  data?: string | Object; // Body for POST method
  headers?: Object;
  credentials?: string; // default: TRUE
  url?: string; // API Endpoint
}

const fetchInstance = axios.create({
  baseURL: ENVIRONMENT.API_DOMAIN,
  headers: {
    'Content-Type': 'application/json'
  },
  withCredentials: true
})

function _fetch (url: string, options: fetchOptions = {}) {
//   GLOBAL.XMLHttpRequest = GLOBAL.originalXMLHttpRequest || GLOBAL.XMLHttpRequest
  // Normalize url query params
  var esc = encodeURIComponent
  if (options.query) {
    var queryString = Object.keys(options.query)
        .map(k => esc(k) + '=' + esc(options.query[k]))
        .join('&')
    url = url + '?' + queryString
  }
  options.url = url

  return fetchInstance(options)
}

export default _fetch
