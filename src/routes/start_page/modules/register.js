import { validate } from './../../../utils/validate'
import UserService from '../../../api/user'
import { replace } from 'react-router-redux'

// ------------------------------------
// Constants
// ------------------------------------
const REGISTER_LOADING = 'REGISTER_LOADING'
const REGISTER_ERROR = 'REGISTER_ERROR'
const REGISTER_SUCCESS = 'REGISTER_SUCCESS'
const INPUT_VALUE = 'REGISTER_INPUT_VALUE'
const SET_CHECKBOX = 'REGISTER_SET_CHECKBOX'
const SHOW_CONFIRM_MODAL = 'REGISTER_SHOW_CONFIRM_MODAL'
const validationFieldList = ['firstName', 'lastName', 'email', 'password', 'birthday']

// ------------------------------------
// Actions
// ------------------------------------
export function onRegisterSuccess () {
  return (dispatch) => {
    dispatch(replace('/start/login'))
  }
}

export function handleInputValueChange (e, divName) {
  return (dispatch, getState) => {
    let input = {}
    if (divName === 'conditionTerm') {
      dispatch(setChecbox())
    } else if (divName === 'birthday') {
      input[divName] = e
      dispatch(setInput(input))
    } else {
      input[divName] = e.target.value
      dispatch(setInput(input))
    }
  }
}

export function submitForm () {
  return (dispatch, getState) => {
    let moduleState = getState().newUser
    let result = ''
    for (let i in validationFieldList) {
      let field = validationFieldList[i]
      result = validate(field, moduleState[field])
      if (result !== 'NO_ERROR') {
        dispatch(setErrorMessage(result))
        break
      }
    }

    if (result === 'NO_ERROR') {
      if (moduleState.conditionTerm === false) {
        return dispatch(setErrorMessage('You must accept the Terms and Conditions of the website'))
      }

      dispatch(sendRegisterRequest())
    }
  }
}

function sendRegisterRequest () {
  return (dispatch, getState) => {
    let moduleState = getState().newUser
    dispatch(setLoading(true))
    let data = {
      'auth_providers': 'local',
      'email': moduleState.email.trim(),
      'password': moduleState.password.trim(),
      'first_name': moduleState.firstName.trim(),
      'last_name': moduleState.lastName.trim(),
      'birthday': moduleState.birthday.trim(),
      'gender': moduleState.gender
    }
    UserService.Register(data)
    .then(res => {
      dispatch(registerSuccess())
    })
    .catch(err => dispatch(setErrorMessage(err.response.data.error)))
  }
}

function setErrorMessage (errMsg) {
  return {
    type: REGISTER_ERROR,
    errorMessage: errMsg
  }
}

function setInput (input) {
  return {
    type: INPUT_VALUE,
    input: input
  }
}

function setChecbox () {
  return {
    type: SET_CHECKBOX
  }
}

function setLoading (value) {
  return {
    type: REGISTER_LOADING,
    isLoading: value
  }
}

function setSuccessMessage (msg) {
  return {
    type: REGISTER_SUCCESS,
    successMessage: msg
  }
}

function showConfirmModal () {
  return {
    type: SHOW_CONFIRM_MODAL
  }
}

function registerSuccess () {
  return (dispatch) => {
    dispatch(setLoading(false))
    dispatch(showConfirmModal())
    dispatch(setSuccessMessage('Sign up successfully'))
  }
}

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [INPUT_VALUE]: (state, action) => {
    return {
      ...state,
      ...action.input
    }
  },
  [REGISTER_ERROR]: (state, action) => {
    return {
      ...state,
      isRegisterError: true,
      isLoading: false,
      errorMessage: action.errorMessage
    }
  },
  [SHOW_CONFIRM_MODAL]: (state, action) => {
    return {
      ...state,
      isOpenConfirmModal: true
    }
  },
  [REGISTER_SUCCESS]: (state, action) => {
    return {
      ...state,
      isRegisterSuccess: true,
      isLoading: false,
      successMessage: action.successMessage
    }
  },
  [REGISTER_LOADING]: (state, action) => {
    return {
      ...state,
      isLoading: action.isLoading,
      isRegisterError: false,
      isRegisterSuccess: false,
      errorMessage: '',
      successMessage: ''
    }
  },
  [SET_CHECKBOX]: (state, action) => {
    return {
      ...state,
      conditionTerm: !state.conditionTerm
    }
  }
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  isLoading: false,
  isRegisterSuccess: false,
  isRegisterError: false,
  isOpenConfirmModal: false,
  errorMessage: '',
  successMessage: '',
  inputValue: '',
  firstName: '',
  lastName: '',
  email: '',
  password: '',
  birthday: '',
  conditionTerm: false,
  gender: 'MALE'
}

export default function registerReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}

