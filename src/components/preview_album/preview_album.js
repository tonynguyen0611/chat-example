import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import axios from 'axios'
import { sendRequestNetworkAction, sendRequestNetworkList } from '../../helper/network'
import './preview_album.scss'

const loadingImage = <img src='https://cdn.zenquiz.net/static/Assets/loading-animation.gif' alt='Loading...' />

const MediaObject = ({ imageUrl, isActive, index, onSelectItem }) => {
  return (
    <div className={isActive ? 'col-md-3 item-active' : 'col-md-3'} onClick={() => onSelectItem(index)}>
      <img className='img-responsive' src={imageUrl} />
    </div>
  )
}
class DetailsCollection extends React.Component {

  constructor (props) {
    super(props)
    this.state = {
      isLoading: true,
      indexMediaItem: 0,
      collection: this.props.collection,
      mediaItems: [],
      mediaItem: this.props.mediaItem,
      isOwner: false,
      isFollowing: false,
      viewOriginalImage: false
    }
  }
  componentWillMount () {
    if (this.state.collection && this.state.collection.id) {
      this.getCollection(this.state.collection.id)
      .then(res => {
        res.media_items.map((Item, i) => {
          if (Item.id === this.state.mediaItem.id) {
            this.setState({
              indexMediaItem: i
            })
          }
        })
        this.checkFollowing(res.user)
        this.setState({
          mediaItems: res.media_items
        })
      })
      .catch()
    }
  }
  checkFollowing (userObj) { // check if current user follow album's owner or not
    let currentUserId = JSON.parse(localStorage.getItem('userinfo')).id
    if (currentUserId === userObj.id) this.setState({ isOwner: true })
    let options = {}
    options.userId = currentUserId
    options.conditions = {
      'id': userObj.id
    }
    sendRequestNetworkList(options, 'following')
    .then(res => {
      if (res.length > 0) {
        this.setState({
          isFollowing: true,
          connectionId: res[0].targeted_user_connections[0].id
        })
      } else {
        this.setState({ isFollowing: false })
      }
      this.setState({ isLoading: false })
    })
    .catch(err => console.log(err))
  }
  handleNetworkAction (collection, requestType) { // send request to follow or unfollow this album's owner
    let requestId = ''
    if (requestType === 'unfollow') {
      requestId = this.state.connectionId
    } else {
      requestId = this.state.collection.user.id
    }
    sendRequestNetworkAction(requestId, requestType)
    .then(res => {
      if (requestType === 'unfollow') {
        this.setState({ isFollowing: false })
      } else {
        this.setState({ isFollowing: true })
      }
    })
  }
  getCollection = (collectionId) => {
    return new Promise(function (resolve, reject) {
      let url = ENVIRONMENT.API_DOMAIN +
      '/albums/' + collectionId
      axios.get(encodeURI(url),
        { withCredentials: true })
        .then(res => resolve(res.data))
        .catch(err => reject(err))
    })
  }
  onNext = () => {
    this.setState({ viewOriginalImage: false }, () => {
      let index = (this.state.indexMediaItem === this.state.mediaItems.length - 1)
      ? 0 : this.state.indexMediaItem + 1
      this.setState({
        indexMediaItem: index
      })
    })
  }
  onPrevious = () => {
    this.setState({ viewOriginalImage: false }, () => {
      let index = (this.state.indexMediaItem === 0)
      ? this.state.mediaItems.length - 1 : this.state.indexMediaItem - 1
      this.setState({
        indexMediaItem: index
      })
    })
  }
  onSelectItem = (index) => {
    this.setState({ viewOriginalImage: false }, () => {
      this.setState({
        indexMediaItem: index
      })
    })
  }
  render () {
    let items = []
    let { indexMediaItem, mediaItems, collection } = this.state

    let index = (indexMediaItem + 4 >= mediaItems.length) ? mediaItems.length - 4 : indexMediaItem
    index = (index < 0) ? 0 : index
    let n = (index + 4 >= mediaItems.length) ? mediaItems.length : index + 4
    let mediaItem = mediaItems[indexMediaItem]
    for (let i = index; i < n; i++) {
      items.push(<MediaObject
        index={i}
        onSelectItem={this.onSelectItem}
        key={i}
        isActive={mediaItems[i].id === mediaItem.id}
        imageUrl={mediaItems[i].images[2].source} />)
    }
    return (
      <div className='DetailsCollection'>
        <div className='arrow-left' onClick={this.onPrevious}>
          <i className='fa fa-long-arrow-left' aria-hidden='true' />
        </div>
        <div className='arrow-right' onClick={this.onNext}>
          <i className='fa fa-long-arrow-right' aria-hidden='true' />
        </div>
        <div className='row'>
          {
            !this.state.isLoading &&
            <div>
              {
                mediaItem &&
                <div>
                  <div className='right-panel'>
                    <div className='col-md-3'>
                      <h5>{collection.title}</h5>
                      <p>{mediaItem.description}</p>
                    </div>
                    <div className='col-md-6 image-preview'>
                      <img className='img-responsive' src={this.state.viewOriginalImage ? mediaItem.images[0].source : mediaItem.images[1].source} />
                      {!this.state.viewOriginalImage &&
                        <a onClick={() => this.setState({ viewOriginalImage: true })}>View original Image</a>
                      }
                    </div>
                    <div className='col-md-3'>
                      <div className='row'>
                        <div className='col-md-4'>
                          <Link to={'/user-view/' + mediaItem.user.id + '/collection'} onClick={this.props.onCancel}>
                            <img className='img-circle img-responsive' src={mediaItem.user.profile_avatar.url} />
                          </Link>
                        </div>
                        <div className='col-md-4'>
                          <h5>{mediaItem.user.full_name}</h5>
                        </div>
                        {
                          !this.state.isOwner &&
                          <div className='col-md-4 small-btn-container'>
                            {
                              this.state.isFollowing &&
                              <button
                                className='small-btn btn-green'
                                onClick={() => this.handleNetworkAction(collection, 'unfollow')}>
                                Following
                              </button>
                            }
                            {
                              !this.state.isFollowing &&
                              <button
                                className='small-btn btn-blue'
                                onClick={() => this.handleNetworkAction(collection, 'follow')}>
                                Follow
                              </button>
                            }
                          </div>
                        }
                      </div>
                      <div className='row media-items'>
                        {items}
                      </div>
                    </div>
                  </div>
                </div>
              }
            </div>
          }
          {
            this.state.isLoading && loadingImage
          }
        </div>
      </div>
    )
  }
}

DetailsCollection.propTypes = {
  onCancel: PropTypes.func,
  collection: PropTypes.object,
  mediaItem: PropTypes.object
}

MediaObject.propTypes = {
  imageUrl: PropTypes.string,
  isActive: PropTypes.bool,
  index: PropTypes.number,
  onSelectItem: PropTypes.func
}

export default DetailsCollection
