import React from 'react'
import { Gateway } from 'react-gateway'
import { Dialog, FlatButton } from 'material-ui'
import './register_email_modal.scss'

const customContentStyle = {
  width: '50%',
  minHeight: '80%',
  maxWidth: 'none',
  maxHeight: 'none',
  borderRadius: '10px !important'
}

const inputStyle = {
  minHeight: '52px !important'
}

var RegisterEmailModal = function (options, onCancel, handleInputValueChange, registerSocialEmail) {
  options = options || {}
  var actions = [
    <FlatButton
      label='Cancel'
      primary={true}
      onClick={onCancel}
      onTouchTap={onCancel}
    />
  ]
  return (
    <Gateway into='confirm-modal'>
      <Dialog
        actions={actions}
        modal={true}
        contentStyle={customContentStyle}
        open={true}
      >
        <div className='register-email-modal'>
          <p> Please enter your email </p>
          <input
            className={'form-control ' + (options.isNormalLoading ? 'input-lock' : '')}
            style={inputStyle}
            placeholder='Email' type='email'
            value={options.socialEmail} onChange={(e) => handleInputValueChange(e, 'socialEmail')} />
          <button className='btn btn-block btn-start btn-start-gray' onClick={registerSocialEmail}>
            {!options.isNormalLoading ? 'Continue Log In' : options.loadingImg}
          </button>
          {options.isLoginError &&
            <h5 id='errorLoginMsg'> {options.errorMessage} </h5>
          }
        </div>
      </Dialog>
    </Gateway>
  )
}

export default RegisterEmailModal
