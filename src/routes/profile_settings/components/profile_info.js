import React from 'react'
import PropTypes from 'prop-types'
import 'react-date-picker/index.css'
import { DateField } from 'react-date-picker'
import './profile_info.scss'

class ProfileInfo extends React.Component {
  componentWillMount () {
    this.props.restoreAllAttributes
  }

  render () {
    let _this = this
    return (
      <div className='col-md-8 side-bar-right'>
        <div className={'profile-details profile-info ' + (_this.props.isLoading ? 'profile-info-lock' : '')}>
          <div className='header-block'>
            <h5 className='title'>
              Personal information
            </h5>
          </div>
          <div className='content-block'>
            <div className='row'>
              <div className='col-lg-6 col-md-6 col-sm-12 col-xs-12'>
                <div className='form-group label-floating'>
                  <label className='control-label'>First Name</label>
                  <div>
                    <input
                      type='text'
                      defaultValue={_this.props.first_name}
                      onChange={(e) => _this.props.handleInputValueChange(e, 'first_name')} />
                  </div>
                </div>
              </div>
              <div className='col-lg-6 col-md-6 col-sm-12 col-xs-12'>
                <div className='form-group label-floating'>
                  <label className='control-label'>Last Name</label>
                  <div>
                    <input
                      type='text'
                      defaultValue={_this.props.last_name}
                      onChange={(e) => _this.props.handleInputValueChange(e, 'last_name')} />
                  </div>
                </div>
              </div>
            </div>
            <div className='row'>
              <div className='col-lg-6 col-md-6 col-sm-12 col-xs-12'>
                <div className='form-group label-floating'>
                  <label className='control-label'>Your Email</label>
                  <div>
                    <input
                      className='lock'
                      type='email'
                      defaultValue={_this.props.email} />
                  </div>
                </div>
              </div>
              <div className='col-lg-6 col-md-6 col-sm-12 col-xs-12'>
                <div className='form-group label-floating'>
                  <label className='control-label'>Your Website</label>
                  <div>
                    <input
                      type='text'
                      defaultValue={_this.props.personal_website}
                      onChange={(e) => _this.props.handleInputValueChange(e, 'personal_website')} />
                  </div>
                </div>
              </div>
            </div>
            <div className='row'>
              <div className='col-lg-6 col-md-6 col-sm-12 col-xs-12'>
                <div className='form-group date-time-picker label-floating'>
                  <label className='control-label'>Your Birthday</label>
                  <div>
                    <DateField
                      dateFormat='DD/MM/YYYY'
                      defaultValue={_this.props.birthday}
                      onChange={(e) => _this.props.handleInputValueChange(e, 'birthday')}
                    />
                  </div>
                </div>
              </div>
              <div className='col-lg-6 col-md-6 col-sm-12 col-xs-12'>
                <div className='form-group label-floating'>
                  <label className='control-label'>Your Phone Number</label>
                  <div>
                    <input
                      className='scaled-input'
                      type='text'
                      defaultValue={_this.props.phone}
                      onChange={(e) => _this.props.handleInputValueChange(e, 'phone')} />
                  </div>
                </div>
              </div>
            </div>
            <div className='row'>
              <div className='col-lg-4 col-md-4 col-sm-12 col-xs-12'>
                <div className='form-group label-floating is-select'>
                  <label className='control-label'>Your Country</label>
                  <input
                    type='text'
                    defaultValue={_this.props.country}
                    onChange={(e) => _this.props.handleInputValueChange(e, 'country')} />
                </div>
              </div>
              <div className='col-lg-4 col-md-4 col-sm-12 col-xs-12'>
                <div className='form-group label-floating is-select'>
                  <label className='control-label'>Your State / Province</label>
                  <input
                    type='text'
                    defaultValue={_this.props.region}
                    onChange={(e) => _this.props.handleInputValueChange(e, 'region')} />
                </div>
              </div>
              <div className='col-lg-4 col-md-4 col-sm-12 col-xs-12'>
                <div className='form-group label-floating is-select'>
                  <label className='control-label'>Your City</label>
                  <input
                    type='text'
                    defaultValue={_this.props.city}
                    onChange={(e) => _this.props.handleInputValueChange(e, 'city')} />
                </div>
              </div>
            </div>
            <div className='row'>
              <div className='col-lg-6 col-md-6 col-sm-12 col-xs-12'>
                <div className='form-group label-floating full-height bio'>
                  <label className='control-label'>Bio</label>
                  <textarea
                    defaultValue={_this.props.bio}
                    onChange={(e) => _this.props.handleInputValueChange(e, 'bio')} />
                </div>
              </div>
              <div className='col-lg-6 col-md-6 col-sm-12 col-xs-12'>
                <div className='form-group label-floating'>
                  <label className='control-label'>Your Occupation</label>
                  <div>
                    <input
                      type='text'
                      defaultValue={_this.props.occupation}
                      onChange={(e) => _this.props.handleInputValueChange(e, 'occupation')} />
                  </div>
                </div>
                <div className='form-group label-floating'>
                  <label className='control-label'>Your Skills</label>
                  <div>
                    <input
                      type='text'
                      defaultValue={_this.props.skills}
                      onChange={(e) => _this.props.handleInputValueChange(e, 'skills')} />
                  </div>
                </div>
              </div>
            </div>
            <div className='row'>
              <div className='col-lg-6 col-md-6 col-sm-12 col-xs-12'>
                <div className='form-group label-floating is-select'>
                  <label className='control-label'>Your Gender</label>
                  <select
                    className='selectpicker form-control'
                    size='auto'
                    defaultValue={_this.props.gender}
                    onChange={(e) => _this.props.handleInputValueChange(e, 'gender')}>
                    <option value='MALE'>Male</option>
                    <option value='FEMALE'>Female</option>
                    <option value='OTHERS'>Others</option>
                  </select>
                </div>
              </div>
              <div className='col-lg-6 col-md-6 col-sm-12 col-xs-12'>
                <div className='form-group label-floating'>
                  <label className='control-label'>Categories</label>
                  <div>
                    <input
                      type='text'
                      defaultValue={_this.props.categories}
                      onChange={(e) => _this.props.handleInputValueChange(e, 'categories')} />
                  </div>
                </div>
              </div>
            </div>
            <div className='row'>
              <div className='col-lg-6 col-md-6 col-sm-12 col-xs-12'>
                <div className='form-group label-floating'>
                  <label className='control-label'>Tags</label>
                  <div>
                    <input
                      type='text'
                      defaultValue={_this.props.tags}
                      onChange={(e) => _this.props.handleInputValueChange(e, 'tags')} />
                  </div>
                </div>
              </div>
              <div className='col-lg-6 col-md-6 col-sm-12 col-xs-12'>
                <div className='form-group label-floating'>
                  <label className='control-label'>You in a sentence</label>
                  <div>
                    <input
                      type='text'
                      defaultValue={_this.props.description}
                      onChange={(e) => _this.props.handleInputValueChange(e, 'description')} />
                  </div>
                </div>
              </div>
            </div>
            <div className='row'>
              <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12'>
                <div className='form-group with-icon label-floating'>
                  <div className='external-page-container'>
                    <span className='external-page icon'>
                      <i className='fa fa-instagram c-instagram' aria-hidden='true' />
                    </span>
                    <span className='external-page input'>
                      <input
                        type='text'
                        defaultValue={_this.props.instagram}
                        onChange={(e) => _this.props.handleInputValueChange(e, 'instagram')} />
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div className='row'>
              <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12'>
                <div className='form-group with-icon label-floating'>
                  <div className='external-page-container'>
                    <span className='external-page icon'>
                      <i className='fa fa-linkedin c-linkedin' aria-hidden='true' />
                    </span>
                    <span className='external-page input'>
                      <input
                        type='text'
                        defaultValue={_this.props.linkedin}
                        onChange={(e) => _this.props.handleInputValueChange(e, 'linkedin')} />
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div className='row'>
              <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12'>
                <div className='form-group with-icon label-floating'>
                  <div className='external-page-container'>
                    <span className='external-page icon'>
                      <i className='fa fa-twitter c-twitter' aria-hidden='true' />
                    </span>
                    <span className='external-page input'>
                      <input
                        type='text'
                        defaultValue={_this.props.twitter}
                        onChange={(e) => _this.props.handleInputValueChange(e, 'twitter')} />
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div className='row'>
              <div className='col-lg-6 col-md-6 col-sm-12 col-xs-12'>
                <button className='btn btn-lg btn-bottom btn-left' onClick={_this.props.restoreAllAttributes}>
                  Restore all Attributes
                </button>
              </div>
              <div className='col-lg-6 col-md-6 col-sm-12 col-xs-12'>
                <button className='btn btn-lg btn-bottom btn-right'
                  onClick={_this.props.saveChanges}>
                  {
                    _this.props.isLoading
                    ? <img src='https://cdn.zenquiz.net/static/Assets/loading-animation.gif' alt='Loading...' />
                    : 'Save all Changes'
                  }
                </button>
              </div>
            </div>
            <div className='messages'>
              <p className='error'> { _this.props.isUpdateError && _this.props.errorMessage } </p>
              <p className='success'> { _this.props.isUpdateSuccess && _this.props.successMessage } </p>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

ProfileInfo.propTypes = {
  restoreAllAttributes: PropTypes.func.isRequired
}

export default ProfileInfo
