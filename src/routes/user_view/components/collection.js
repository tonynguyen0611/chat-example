import React from 'react'
import PropTypes from 'prop-types'
import tempAva from './../../../assets/static/ava.jpg'
import CollectionsList from '../../../components/collections_list'
import ChangeAvatarModal from '../modals/change_avatar_modal'
import './collection.scss'

const loadingImage = <img className='loading-img' src='https://cdn.zenquiz.net/static/Assets/loading-animation.gif' alt='Loading...' />

class UserViewCollection extends React.Component {
  componentWillMount () {
    this.props.getCollections(this.props.match.params.userId)
    this.props.getUserInfo(this.props.match.params.userId)
    this.props.getLastCareerItem()
  }
  componentDidMount () {
  }
  componentWillReceiveProps (nextProps) {
    if (nextProps.match.params.userId !== this.props.match.params.userId) {
      this.props.getCollections(nextProps.match.params.userId)
      this.props.getLastCareerItem()
      this.props.getUserInfo(nextProps.match.params.userId)
    }
  }
  customizeExternalPageUrl (url) {
    if (url.indexOf('http://') < 0 && url.indexOf('https://') < 0) {
      return 'https://' + url
    }
    return url
  }
  render () {
    let _this = this
    let userInfo = this.props.userInfo
    let similarList = this.props.similarList
    const modalChangeAvatar = ChangeAvatarModal({
      avatarUrl: _this.props.avatarUrl,
      isUploading: _this.props.isUploadingAvatar
    },
      _this.props.handleCancelChangeAvatar,
      _this.props.handleUpdateAvatar)
    return (
      <div>
        {
          userInfo && userInfo.id &&
          <div className='col-lg-12 col-md-12 col-sm-12 col-xs-12 user-view-body'>
            {this.props.isEditingAvatar && modalChangeAvatar}
            <div className='col-lg-3 col-md-3 col-sm-12 col-xs-12 left-panel'>
              {
                this.props.isLoadingUserInfo &&
                <div className='user-info'>
                  {loadingImage}
                </div>
              }
              {
                !this.props.isLoadingUserInfo &&
                <div className='user-info'>
                  <div className='user-ava ava-large'>
                    <img src={userInfo.profile_avatar.url || tempAva} alt='profile_picture' />
                    {(this.props.currentUser.id === this.props.userInfo.id) &&
                      <div className='btn-camera'>
                        <i className='fa fa-camera' aria-hidden='true' />
                        <input name='file' type='file' accept='image/*' onChange={this.props.handleChangeAvatar}
                          className='input-image' />
                      </div>
                    }
                  </div>
                  <div className='user-title'>
                    {userInfo.occupation[0]}
                  </div>
                  <div className='user-title'>
                    <div>
                      {userInfo.location.city }
                      {userInfo.location.city && userInfo.location.country && ', ' }
                      {userInfo.location.country}
                    </div>
                  </div>
                  <div className='user-info-1'>
                    {userInfo.description}
                  </div>
                  {
                    userInfo.skills && userInfo.skills.length > 0 &&
                    <div className='user-info-1'>
                      <div><b>Skills</b></div>
                      <div> {userInfo.skills[0]}</div>
                    </div>
                  }
                  {
                    this.props.lastCareerItem.title && this.props.lastCareerItem.title &&
                    <div className='user-info-1'>
                      <div><b>Education</b></div>
                      <div>
                        {this.props.lastCareerItem.title}
                        {this.props.lastCareerItem.place ? ', ' + this.props.lastCareerItem.place : ''}
                      </div>
                    </div>
                  }
                  <div className='user-pages'>
                    <div className='user-pages-icon'>
                      {
                        !userInfo.external_profiles.instagram ? ''
                          : <a href={_this.customizeExternalPageUrl(userInfo.external_profiles.instagram)}
                            target='_blank'>
                            <div className='web-icon'><i className='fa fa-instagram' aria-hidden='true' /></div>
                          </a>
                      }
                      {
                        !userInfo.external_profiles.linkedin ? ''
                          : <a href={_this.customizeExternalPageUrl(userInfo.external_profiles.linkedin)}
                            target='_blank'>
                            <div className='web-icon'><i className='fa fa-linkedin' aria-hidden='true' /></div>
                          </a>
                      }
                      {
                        !userInfo.external_profiles.twitter ? ''
                          : <a href={_this.customizeExternalPageUrl(userInfo.external_profiles.twitter)}
                            target='_blank'>
                            <div className='web-icon'><i className='fa fa-twitter' aria-hidden='true' /></div>
                          </a>
                      }
                    </div>
                  </div>
                </div>
              }
              <div className='similar-profiles'>
                <div className='similar-title'><h5> Similar profiles </h5></div>
                {
                  this.props.isLoadingSimilarList && loadingImage
                }
                {
                  !this.props.isLoadingSimilarList &&
                  <div className='similar-list'>
                    {
                      similarList && similarList.length > 0 &&
                      similarList.map((similarItem, index) => {
                        <div className='row similar-item' key={index} >
                          <div className='col-lg-4 col-md-4 col-sm-12 col-xs-12'>
                            <img className='similar-item-img' src={similarItem.profile_avatar.url} />
                          </div>
                          <div className='col-lg-8 col-md-8 col-sm-12 col-xs-12 similar-item-title'>
                            <div className='item-name'> {similarItem.full_name} </div>
                            <div className='item-title'> {similarItem.occupation[0]} </div>
                          </div>
                        </div>
                      })
                    }
                    {
                      similarList && similarList.length === 0 &&
                      <div className='row similar-item'>
                        No similar profiles
                      </div>
                    }
                  </div>
                }
              </div>
            </div>
            <div className='col-lg-9 col-md-9 col-sm-12 col-xs-12 right-panel'>
              <CollectionsList collectionsList={this.props.collectionList} numOfCol={3} canChange='YES' />
            </div>
          </div>
        }
      </div>
    )
  }
}
UserViewCollection.propTypes = {
  userInfo: PropTypes.object.isRequired,
  getUserInfo: PropTypes.func.isRequired,
  match: PropTypes.object,
  isEditingAvatar: PropTypes.bool,
  isLoadingSimilarList: PropTypes.bool,
  getCollections: PropTypes.func.isRequired,
  handleChangeAvatar: PropTypes.func.isRequired,
  getLastCareerItem: PropTypes.func.isRequired,
  lastCareerItem: PropTypes.object.isRequired,
  currentUser: PropTypes.object.isRequired,
  isLoadingUserInfo: PropTypes.bool.isRequired,
  collectionList: PropTypes.array.isRequired,
  similarList: PropTypes.array.isRequired
}

export default UserViewCollection
