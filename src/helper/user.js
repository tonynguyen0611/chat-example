import axios from 'axios'

export function getUserDetails (userId) {
  return new Promise((resolve, reject) => {
    if (!userId) {
      reject('NO ID')
    }
    axios.get(ENVIRONMENT.API_DOMAIN + '/users/' + userId, { withCredentials: true })
    .then(res => resolve(res.data))
    .catch(err => reject(err.response.data.error))
  })
}
