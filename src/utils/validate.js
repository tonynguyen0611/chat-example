import Validator from 'validator'

const rules = {
  firstName: {
    required: 'First name is required',
    maxLen: {
      value: 20,
      msg: 'First Name length must be less than or equal to {value} characters long'
    }
  },
  lastName: {
    required: 'Last name is required',
    maxLen: {
      value: 20,
      msg: 'last Name length must be less than or equal to {value} characters long'
    }
  },
  email: {
    required: 'Email is required',
    email: 'Please enter a valid email address'
  },
  phone: {
    required: 'Please enter a valid phone number'
  },
  birthday: {
    required: 'Please enter your birthday'
  },
  password: {
    required: 'Password is required',
    minLen: {
      value: 6,
      msg: 'Password length must be at least {value} characters long'
    },
    maxLen: {
      value: 32,
      msg: 'Password length must be less than or equal to {value} characters long'
    }
  },
  confirmPassword: {
    match: {
      ref: 'password',
      msg: 'Confirm password does not match'
    }
  }
}

function isValidLength (key, validLength, value) {
  let result = true
  if (key === 'minLen' && value.length < validLength) {
    result = false
  } else if (key === 'maxLen' && value.length > validLength) {
    result = false
  }
  return result
}

function getLengthErrorMsg (value, msg) {
  if (value === undefined || msg === undefined) return ''
  return msg.replace('{value}', value)
}

export function validate (field, value) {
  let fieldRule = rules[field]
  let validationList = Object.keys(fieldRule)
  for (let i in validationList) {
    let key = validationList[i]
    switch (key) {
      case 'required':
        if (Validator.isEmpty(value)) {
          return (fieldRule[key])
        }
        break
      case 'email':
        if (!Validator.isEmail(value)) {
          return (fieldRule[key])
        }
        break
      case 'minLen':
      case 'maxLen':
        if (!isValidLength(key, fieldRule[key].value, value)) {
          let errMsg = getLengthErrorMsg(fieldRule[key].value, fieldRule[key].msg)
          return (errMsg)
        }
        break
      default:
        return ('NO_ERROR')
    }
  }
  return ('NO_ERROR')
}

