import { connectWithLifecycle } from 'react-lifecycle-component'
import { getNewsFeed, setModalState, getCollectionDetail } from '../modules/home'
import { sendLogOutRequest } from '../../start_page/modules/login'
import HomeView from '../components/HomeView'

const mapDispatchToProps = {
  getNewsFeed,
  sendLogOutRequest,
  setModalState,
  getCollectionDetail
}
const mapStateToProps = (state) => {
  return state.home
}

export default connectWithLifecycle(
  mapStateToProps, mapDispatchToProps
)(HomeView)
