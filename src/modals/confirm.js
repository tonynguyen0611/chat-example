import React from 'react'
import { Gateway } from 'react-gateway'
import Dialog from 'material-ui/Dialog'
import FlatButton from 'material-ui/FlatButton'
import './modals.scss'

const customContentStyle = {
  width: '40%',
  minWidth: '40%',
  minHeight: '50%',
  borderRadius: '10px !important'
}
let ConfirmModal = function (options) {
  let isTrue = true
  let actions = [
    <FlatButton
      label='Cancel'
      primary={isTrue}
      onClick={options.onCancel}
    />,
    <FlatButton
      label='OK'
      primary={isTrue}
      disabled={isTrue}
      onClick={options.onConfirm}
    />
  ]
  return (
    <Gateway into='confirm-modal'>
      <Dialog
        modal={false}
        contentStyle={customContentStyle}
        open={options.isOpen}
        onRequestClose={options.onCancel}>
        <div className='confirm-modal'>
          <div className='confirm-content'>{options.text}</div>
          <div className='btn-container'>
            {
              options.isShowingCancel &&
              <button
                className='btn btn-default btn-cancel'
                onClick={options.onCancel}>{options.cancelText || 'Cancel'}</button>
            }
            {
              options.isShowingConfirm &&
              <button
                className='btn btn-default btn-confirm'
                onClick={options.onConfirm}>{options.confirmText || 'OK'}</button>
            }
          </div>
        </div>
      </Dialog>
    </Gateway>
  )
}

export default ConfirmModal
