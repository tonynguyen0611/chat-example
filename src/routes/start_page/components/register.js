import React from 'react'
import PropTypes from 'prop-types'
import ConfirmModal from '../../../modals/confirm'
import { Link } from 'react-router-dom'
import { DateField } from 'react-date-picker'
import 'react-date-picker/index.css'
import './register.scss'

class RegisterForm extends React.Component {
  render () {
    const confirmModal = ConfirmModal({
      isShowingCancel: false,
      isShowingConfirm: true,
      confirmText: 'Continue',
      onConfirm: this.props.onRegisterSuccess,
      text: 'Congratulation! You have signed up successfully!',
      isOpen: this.props.isOpenConfirmModal
    })
    return (
      <div className='register-form'>
        {confirmModal}
        <div className={'input-group ' + (this.props.isLoading ? 'input-group-lock' : '')}>
          <div className='row'>
            <div className='col-lg-6 col-md-6 col-sm-12 col-xs-12'>
              <div className='form-group'>
                <input
                  placeholder='First name'
                  type='text'
                  onChange={(e) => this.props.handleInputValueChange(e, 'firstName')} />
              </div>
            </div>
            <div className='col-lg-6 col-md-6 col-sm-12 col-xs-12'>
              <div className='form-group'>
                <input
                  placeholder='Last name'
                  type='text'
                  onChange={(e) => this.props.handleInputValueChange(e, 'lastName')} />
              </div>
            </div>
          </div>
          <div className='row'>
            <div className='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
              <div className='form-group'>
                <input
                  placeholder='Your Email'
                  type='text'
                  onChange={(e) => this.props.handleInputValueChange(e, 'email')} />
              </div>
            </div>
          </div>
          <div className='row'>
            <div className='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
              <div className='form-group'>
                <input
                  placeholder='Your password'
                  type='password'
                  onChange={(e) => this.props.handleInputValueChange(e, 'password')} />
              </div>
            </div>
          </div>
          <div className='row'>
            <div className='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
              <div className='form-group'>
                <div className='control-label'>Your Birthday</div>
                <DateField
                  dateFormat='DD/MM/YYYY'
                  defaultValue='dd/mm/yyyy'
                  onChange={(e) => this.props.handleInputValueChange(e, 'birthday')}
                />
              </div>
            </div>
          </div>
          <div className='row'>
            <div className='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
              <div className='form-group select-gender'>
                <div className='control-label'>Your Gender </div>
                <select
                  className='selectpicker form-control'
                  size='auto'
                  onChange={(e) => this.props.handleInputValueChange(e, 'gender')}>
                  <option value='MALE'>Male</option>
                  <option value='FEMALE'>Female</option>
                  <option value='OTHERS'>Others</option>
                </select>
              </div>
            </div>
          </div>
          <div className='row'>
            <div className='col-lg-12 col-md-12 col-sm-12 col-xs-12 conditions-box'>
              <span>
                <input
                  className='conditions-input'
                  type='checkbox'
                  onClick={(e) => this.props.handleInputValueChange(e, 'conditionTerm')} />
              </span>
              <span className='conditions-text'>
                  I accept the <Link to='/terms'> Terms and Conditions </Link> of the website
              </span>
            </div>
          </div>
          <div className='error-container'>
            <h5 className={(this.props.isRegisterError ? 'error' : '')}>
              {this.props.isRegisterError ? this.props.errorMessage : ''}
            </h5>
            <h5 className={(this.props.isRegisterSuccess ? 'success' : '')}>
              {this.props.isRegisterSuccess ? this.props.successMessage : ''}
            </h5>
          </div>
          <button
            className={'btn btn-block btn-start btn-start-gray' + (this.props.isRegisterSuccess ? 'lock' : '')}
            onClick={this.props.submitForm}>
            {!this.props.isLoading ? 'Sign up'
              : <img src='https://cdn.zenquiz.net/static/Assets/loading-animation.gif' alt='Loading' />}
          </button>
          <Link to='/start/login'> Already has an account? Log In </Link>
        </div>
      </div>
    )
  }
}

RegisterForm.propTypes = {
  isLoading: PropTypes.bool,
  isRegisterError: PropTypes.bool,
  isRegisterSuccess: PropTypes.bool,
  errorMessage: PropTypes.string,
  successMessage: PropTypes.string,
  email: PropTypes.string,
  password: PropTypes.string,
  firstName: PropTypes.string,
  lastName: PropTypes.string,
  birthday: PropTypes.string,
  gender: PropTypes.string,
  handleInputValueChange: PropTypes.func,
  inputEmail: PropTypes.func,
  inputPassword: PropTypes.func,
  submitForm: PropTypes.func,
  onRegisterSuccess: PropTypes.func,
  isOpenConfirmModal: PropTypes.bool
}

export default RegisterForm
