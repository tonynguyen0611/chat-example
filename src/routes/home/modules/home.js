import UserService from '../../../api/user'
// ------------------------------------
// Constants
// ------------------------------------
const RECEIVE_ERROR = 'RECEIVE_ERROR'
const GET_NEWS_FEED_SUCCESS = 'GET_NEWS_FEED_SUCCESS'
const RESET_STATE = 'RESET_STATE'
const IS_LOADING = 'IS_LOADING'
const GET_COLLECTION_SUCCESS = 'GET_COLLECTION_DETAIL_SUCCESS'
const SET_MODAL_STATE = 'SET_HOME_MODAL_STATE'
// ------------------------------------
// Actions
// ------------------------------------

export function resetState () {
  return {
    type: RESET_STATE
  }
}
export function getCollectionDetail (collection, mediaItem) {
  return {
    type: GET_COLLECTION_SUCCESS,
    collection: collection,
    mediaItem: mediaItem
  }
}
export function setModalState () {
  return {
    type: SET_MODAL_STATE
  }
}
export function getNewsFeed (query) {
  return (dispatch, getState) => {
    console.log('get news feed...')
    query = query || {}
    let offset = getState().home.offset
    let limit = getState().home.limit
    let query = {
      limit: limit,
      offset: offset
    }
    UserService.getNewsFeed(query)
      .then(res => dispatch(getNewsFeedSuccess(res.data)))
      .catch(err => dispatch(receiveError(err)))
  }
}

export function getNewsFeedSuccess (newsFeedData) {
  return {
    type: GET_NEWS_FEED_SUCCESS,
    data: newsFeedData,
    hasMore: newsFeedData.length > 0
  }
}

function receiveError (err) {
  return {
    type: RECEIVE_ERROR,
    err: err
  }
}
// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [SET_MODAL_STATE]: (state, action) => {
    return {
      ...state,
      isShowModal: !state.isShowModal
    }
  },
  [RESET_STATE]: (state, action) => {
    return {
      ...state,
      newsFeedData: [],
      offset: 0,
      hasMore: true,
      isShowModal: false,
      collectionActive: {},
      mediaItem: {}
    }
  },
  [GET_NEWS_FEED_SUCCESS]: (state, action) => {
    return {
      ...state,
      newsFeedData: [...state.newsFeedData, ...action.data],
      offset:  state.newsFeedData.length + action.data.length,
      hasMore: action.hasMore,
      isNewsFeedLoading: false
    }
  },
  [RECEIVE_ERROR]: (state, action) => {
    let error = action.error || ''
    let errorMessage = error.message === '400' ? 'Have problem when upload file' : 'Error'
    return {
      ...state,
      isNewsFeedLoading: false,
      errorMessage: errorMessage
    }
  },
  [IS_LOADING]: (state, action) => {
    return {
      ...state,
      ...action.loadingObj
    }
  },
  [GET_COLLECTION_SUCCESS]: (state, action) => {
    return {
      ...state,
      isShowModal: true,
      collectionActive: action.collection,
      mediaItem: action.mediaItem
    }
  }
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  isNewsFeedLoading: true,
  errorMessage: '',
  offset: 0,
  limit: 10,
  hasMore: true,
  newsFeedData: [],
  isShowModal: false,
  collectionActive: {},
  mediaItem: {}
}

export default function home (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
