import React from 'react'
import { injectReducer } from '../../store/reducers'

const SearchResultContainer = (store) => {
  const reducer = require('./modules/search_result').default
  const SearchResultContainer = require('./containers/search_result').default
  injectReducer(store, { key: 'searchResult', reducer })
  return SearchResultContainer
}

const PeopleResultContainer = (store) => {
  const reducer = require('./modules/people_result').default
  const PeopleResult = require('./containers/people_result').default
  injectReducer(store, { key: 'peopleResult', reducer })
  return PeopleResult
}

const CollectionResultContainer = (store) => {
  const reducer = require('./modules/collection_result').default
  const CollectionResult = require('./containers/collection_result').default
  injectReducer(store, { key: 'collectionResult', reducer })
  return CollectionResult
}

export const CollectionResultRoute = (store) => ({
  path: '/search/collection/:searchKey',
  component: CollectionResultContainer(store)
})

export const PeopleResultRoute = (store) => ({
  path: '/search/people/:searchKey',
  component: PeopleResultContainer(store)
})

export const SearchResultRoute = (store) => ({
  path: '/search/all/:searchKey',
  component: SearchResultContainer(store)
})

