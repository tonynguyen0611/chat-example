import React from 'react'
import {
  Route,
  Redirect
} from 'react-router-dom'

import Header from '../../components/DefaultHeader'
import './DefaultLayout.scss'

export const DefaultLayout = ({ component: Component, store: store, route: route, isLogin: isLogin, ...rest }) => {
  return (
    <Route exact={route.exact} {...rest} render={props => (
      isLogin() ? (
        <div className='DefaultLayout'>
          <Header store={store} />
          <div className='container'>
            <Component {...props} store={store} />
            <div id="button"></div>
          </div>
        </div>) : (
          <Redirect to={{
            pathname: '/start/view',
            state: { from: props.location }
          }} />
      )
    )} />
  )
}

export default DefaultLayout
