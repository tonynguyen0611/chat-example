import React from 'react'
import PropTypes from 'prop-types'
import SendBird from 'sendbird'
import $ from 'jquery'
import moment from 'moment'
import InputSearch from '../../../components/input_search'
import { Link } from 'react-router-dom'
import './messages.scss'

let sb = null

const nonAvaImg = 'http://ocotur-dev.s3.amazonaws.com/static/assets/ava.jpg'

const toHowLongAgo = (input) => {
  let date = moment(input)
  return date.fromNow()
}
const ChannelObject = ({ channel, currentUser, handleSelectChannel, handleDeleteChannel }) => {
  let user, userId = null
  channel.members.map((member, i) => {
    if (member.userId !== currentUser.id) {
      user = member
      userId = user.id || user.userId
    }
  })
  return (
    <div>
      {
        user &&
        <div className='message-item col-md-12' onClick={() => handleSelectChannel(channel.url)} >
          <div className='col-md-2 img-container'>
            <img className='' src={user.profileUrl || nonAvaImg} alt='avatar' />
          </div>
          <div className='col-md-10'>
            <Link to={'/user-view/' + userId + '/collection'} >
              <span className='user-name'> {user.nickname} </span>
            </Link>
            <div className='message-item-content'>
              {channel.lastMessage ? channel.lastMessage.message : ''}
            </div>
            <div className='message-item-status'>
              {channel.lastMessage ? toHowLongAgo(channel.lastMessage.createdAt) : '' }
            </div>
          </div>
          <div className='edit-tool' onClick={(e) => handleDeleteChannel(e, channel.url)}>
            <i className='fa fa-times' aria-hidden='true' />
          </div>
        </div>
      }
    </div>
  )
}

const MessageObject = ({ Message, isOwner, userId }) => {
  return (
    <div>
      { isOwner && Message ? (
        <div className='row owner'>
          <div className='col-md-9 message-content'>
            <p>{Message.message}</p>
          </div>
          <div className='col-md-3 avatar'>
            <img src={Message._sender.profileUrl || nonAvaImg} className='img-responsive img-circle' />
          </div>
        </div>
      ) : (
        <div className='row guest'>
          <div className='col-md-2 avatar'>
            <img src={Message._sender.profileUrl || nonAvaImg} className='img-responsive img-circle' />
          </div>
          <div className='col-md-10 message-content'>
            <p>{Message.message}</p>
          </div>
        </div>
      )
      }
    </div>
  )
}
class Messages extends React.Component {
  constructor (props) {
    super(props)
    sb = SendBird.getInstance()
  }
  componentWillMount () {
  }
  componentWillUnmount () {
  }
  componentWillReceiveProps (nextProps) {
    if (nextProps.retrievingChannelsComplete && nextProps.connectSendBirdSuccess && !nextProps.initSendBirdSuccess) {
      this.props.handleInitSendBirdSuccess()
      const ChannelHandler = new sb.ChannelHandler()
      ChannelHandler.onMessageReceived = (channel, message) => {
        this.props.receiveMessage(message, channel)
      }
      sb.addChannelHandler('ChatView', ChannelHandler)
    }
  }
  sendMessage = () => {
    if (this.props.inputReply.trim() !== '') {
      this.props.handleSendMessage()
      let $cont = $('.message-details-content')
      $cont[0].scrollTop = $cont[0].scrollHeight
    }
    $('.input-reply').val('')
  }
  handleKeyPress = (e) => {
    if (e.key === 'Enter') {
      this.sendMessage()
    }
  }
  handleSelectUserToChat = (item) => {
    this.props.handleSelectUserToChat(item, sb)
  }
  render () {
    let channelMessages = this.props.channelMessages
    let currentChannel = channelMessages[this.props.channelIndex]
    let messages = null
    let userActive = this.props.userActive
    if (currentChannel && currentChannel.messages) {
      messages = currentChannel.messages
      messages.sort((a, b) => a.createdAt - b.createdAt)
    }
    if (channelMessages.length > 0) {
      channelMessages.sort((a, b) => {
        if (!a.lastMessage && b.lastMessage) {
          a.lastMessage = {
            createdAt: Date.now()
          }
        }
        if (a.lastMessage && !b.lastMessage) {
          b.lastMessage = {
            createdAt: Date.now()
          }
        }
        if (!a.lastMessage && !b.lastMessage) {
          return true
        }
        a.Priority = a.Priority ? a.Priority : 0
        b.Priority = b.Priority ? b.Priority : 0
        return (b.lastMessage.createdAt - a.lastMessage.createdAt + b.Priority * 10 - a.Priority * 10)
      })
    }
    let $cont = $('.message-details-content')
    if ($cont[0]) {
      $cont.stop().animate({ scrollTop: $cont[0].scrollHeight + 1000 }, 10)
    }
    return (
      <div className='col-md-8 side-bar-right'>
        <div className='messages'>
          <div className='header-block'>
            <div className='row'>
              <div className='col-lg-6 col-md-6 col-sm-6 col-xs-6'>
                <h5 className='title'> Messages </h5>
              </div>
              <div className='col-lg-6 col-md-6 col-sm-6 col-xs-6'>
                <button className='btn btn-top' onClick={this.props.handleAddNewChannel} > New </button>
              </div>
            </div>
          </div>
          <div className='content-block'>
            <div className='col-lg-5 col-md-5 col-sm-12 col-xs-12 message-list'>
              {this.props.channelMessages.map((channel, i) => (
                <ChannelObject
                  key={i}
                  channel={channel}
                  handleSelectChannel={this.props.handleSelectChannel}
                  handleDeleteChannel={this.props.handleDeleteChannel}
                  currentUser={this.props.user.currentUser} />
              ))
              }
            </div>
            <div className='col-lg-7 col-md-7 col-sm-12 col-xs-12 message-details'>
              <div className='message-details-name'>
                {this.props.isSearching &&
                  <InputSearch
                    searchFunction={this.props.getUsers}
                    placeholder='type email or nick name here...'
                    handleSelectItem={this.handleSelectUserToChat}
                    arrResponses={this.props.userList} />
                }
                {!this.props.isSearching && userActive &&
                  <span>{userActive.nickname || userActive.full_name}</span>
                }
              </div>
              <hr />
              <div className='message-details-content'>
                {currentChannel && currentChannel.messages && !currentChannel.isLoading &&
                  <div>
                    {messages.map((message, i) => (
                      <MessageObject
                        key={i}
                        Message={message}
                        isOwner={message._sender.userId === this.props.user.currentUser.id} />
                    ))
                    }
                  </div>
                }
                {currentChannel && currentChannel.isLoading &&
                  <div>
                    <img className='img-responsive' src='https://cdn.zenquiz.net/static/Assets/loading-animation.gif' />
                  </div>
                }
              </div>
              <div className='message-details-reply'>
                <div className='input-container'>
                  <textarea className='input-reply'
                    rows='3'
                    placeholder='Write your reply here...'
                    onKeyPress={this.handleKeyPress}
                    onChange={this.props.handleInputReplyChange} />
                </div>
                <div><button className='btn btn-reply' onClick={this.sendMessage} >Send</button></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
ChannelObject.propTypes = {
  channel: PropTypes.object,
  currentUser: PropTypes.object,
  handleSelectChannel: PropTypes.func,
  handleDeleteChannel: PropTypes.func
}

MessageObject.propTypes = {
  Message: PropTypes.object,
  isOwner: PropTypes.bool,
  userId: PropTypes.string
}

Messages.propTypes = {
  channelIndex: PropTypes.number.isRequired,
  channelMessages: PropTypes.array.isRequired,
  handleInputReplyChange: PropTypes.func.isRequired,
  handleSendMessage: PropTypes.func.isRequired,
  connectSendBird: PropTypes.func.isRequired,
  handleInitSendBirdSuccess: PropTypes.func.isRequired,
  createNewChannel: PropTypes.func.isRequired,
  receiveMessage: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired,
  handleAddNewChannel: PropTypes.func.isRequired,
  getUsers: PropTypes.func.isRequired,
  userList: PropTypes.array.isRequired,
  handleSelectUserToChat: PropTypes.func.isRequired,
  handleSelectChannel: PropTypes.func.isRequired,
  notifications: PropTypes.object,
  handleUpdateWhenRecieveMessage: PropTypes.func.isRequired,
  handleUpdateWhenChannelChange: PropTypes.func.isRequired,
  handleDeleteChannel: PropTypes.func.isRequired,
  isSearching: PropTypes.bool.isRequired,
  userActive: PropTypes.object.isRequired,
  inputReply: PropTypes.string.isRequired
}
export default Messages
