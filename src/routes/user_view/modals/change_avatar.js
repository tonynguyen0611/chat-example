import React from 'react'
import AvatarEditor from 'react-avatar-editor'
import './change_avatar.scss'

class ChangeAvatar extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      scale: 1.5
    }
  }
  onClickSave = () => {
    if (this.editor) {
      const canvasScaled = this.editor.getImageScaledToCanvas()
      let ctx = canvasScaled.toDataURL()
      let bufImage = new Buffer(ctx.replace(/^data:image\/\w+;base64,/, ''), 'base64')
      this.props.handleUpdateAvatar(bufImage)
    }
  }
  setScale = (e) => {
    let curentValuue = parseInt(e.target.value)
    this.setState({
      scale: (curentValuue + 100) / 100
    })
  }
  setEditorRef = (editor) => this.editor = editor
  render () {
    return (
      <div className='ChangeAvatar'>
        <div className='header'>
          <h3>Create Profile Picture</h3>
        </div>
        <div className='content'>
          <div className='editor'>
            <AvatarEditor
              ref={this.setEditorRef}
              image={this.props.avatarUrl}
              width={250}
              height={250}
              border={50}
              color={[255, 255, 255, 0.6]} // RGBA
              scale={this.state.scale}
              rotate={0}
            />
            <input
              type='range'
              min={0}
              max={100}
              step={1}
              onChange={this.setScale}>
            </input>
          </div>
        </div>
        <div className='footer'>
          {this.props.isUploading ? (
              <img className='img-responsive' src='https://cdn.zenquiz.net/static/Assets/loading-animation.gif' />
          ) : (
            <button className='btn-primary' onClick={this.onClickSave}>
              <span>Save</span>
            </button>
          )}
          {!this.props.isUploading &&
            <button className='btn-default' onClick={this.props.onCancel}>Cancel</button>
          }
        </div>
      </div>
    )
  }
}

export default ChangeAvatar
