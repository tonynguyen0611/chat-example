import React from 'react'
import { injectReducer } from '../../store/reducers'

const HeaderContainer = (store) => {
  const Header = require('./containers/header').default
  const reducer = require('./modules/header').default
  injectReducer(store, { key: 'header', reducer })
  return Header
}

const MessagesContainer = (store) => {
  const Messages = require('./containers/messages').default
  return Messages
}

const EduExpContainer = (store) => {
  const EduExp = require('./containers/edu_exp').default
  const reducer = require('./modules/edu_exp').default
  injectReducer(store, { key: 'eduExp', reducer })
  return EduExp
}

const AccountSettingsContainer = (store) => {
  const AccountSettings = require('./containers/account_settings').default
  const reducer = require('./modules/account_settings').default
  injectReducer(store, { key: 'accountSettings', reducer })
  return AccountSettings
}

const ProfileInfoContainer = (store) => {
  const ProfileInfo = require('./containers/profile_info').default
  const reducer = require('./modules/profile_info').default
  injectReducer(store, { key: 'profileInfo', reducer })
  return ProfileInfo
}

const ChangePasswordContainer = (store) => {
  const ChangePassword = require('./containers/change_password').default
  const reducer = require('./modules/change_password').default
  injectReducer(store, { key: 'changePassword', reducer })
  return ChangePassword
}

export const HeaderRoute = (store) => ({
  path: '/',
  component: HeaderContainer(store)
})

export const MessagesRoute = (store) => ({
  path: '/profile/messages',
  component: MessagesContainer(store)
})

export const EduExpRoute = (store) => ({
  path: '/profile/edu_exp',
  component: EduExpContainer(store)
})

export const ChangePasswordRoute = (store) => ({
  path: '/profile/change_password',
  component: ChangePasswordContainer(store)
})

export const AccountSettingsRoute = (store) => ({
  path: '/profile/account_settings',
  component: AccountSettingsContainer(store)
})

export const ProfileInfoRoute = (store) => ({
  path: '/profile/info',
  component: ProfileInfoContainer(store)
})
