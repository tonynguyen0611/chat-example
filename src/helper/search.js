import axios from 'axios'

export function sendRequestSearch (options) {
  return new Promise((resolve, reject) => {
    if (!options) {
      reject('NO OPTIONS')
    }

    if (!options.searchType) {
      reject('NO TYPE')
    }

    let url = ENVIRONMENT.API_DOMAIN + '/' + options.searchType +
              '?conditions=' + JSON.stringify(options.conditions || {}) +
              '&offset=' + (options.offset || 0) +
              '&limit=' + (options.limit || 20) +
              '&sort=' + (options.sort || '')
    console.log('url', url)
    axios.get(encodeURI(url), { withCredentials: true })
    .then(res => resolve(res.data))
    .catch(err => reject(err.response.data.error))
  })
}
