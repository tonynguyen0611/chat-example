import moment from 'moment'
import axios from 'axios'
// ------------------------------------
// Constants
// ------------------------------------
const SET_USER_INFO = 'ABOUT_SET_USER_INFO'
const IS_ERROR = 'ABOUT_IS_ERROR'
const IS_LOADING = 'ABOUT_IS_LOADING'
// ------------------------------------
// Actions
// ------------------------------------

export function initialize (userId) {
  return (dispatch, getState) => {
    dispatch(setLoading(true))
    axios.get(ENVIRONMENT.API_DOMAIN + '/users/' + userId, { withCredentials: true })
    .then(res => {
      let userInfo = res.data
      if (userInfo.career_histories && userInfo.career_histories.length > 0) {
        let careerList = userInfo.career_histories
        for (let i in careerList) {
          reformatTime(careerList[i])
        }
      }
      dispatch(getUserInfoSuccess(userInfo))
      dispatch(setLoading(false))
    })
    .catch(err => dispatch(setErrorMessage(err)))
  }
}

function setLoading (value) {
  return {
    type: IS_LOADING,
    isLoading: value
  }
}

function getUserInfoSuccess (newUserInfo) {
  return {
    type: SET_USER_INFO,
    userInfo: newUserInfo
  }
}

function setErrorMessage (err) {
  return {
    type: IS_ERROR,
    error: err
  }
}

function reformatTime (careerItem) {
  let startTimeString = careerItem.start_time.day + '/' + careerItem.start_time.month + '/' + careerItem.start_time.year
  let endTimeString = careerItem.end_time.day + '/' + careerItem.end_time.month + '/' + careerItem.end_time.year
  let unixStartTime = moment(startTimeString, 'DD/MM/YYYY').valueOf()
  let unixEndTime = moment(endTimeString, 'DD/MM/YYYY').valueOf()
  careerItem.startTimeString = moment(unixStartTime).format('MMM YYYY')
  careerItem.endTimeString = moment(unixEndTime).format('MMM YYYY')
  let diff = unixEndTime - unixStartTime
  let durationYears = moment.duration(diff).years()
  let durationMonths = moment.duration(diff).months()
  let durationString = ''
  durationString += (durationYears > 0) ? moment.duration(durationYears, 'years').humanize() : ''
  durationString += (durationMonths > 0) ? moment.duration(durationMonths, 'months').humanize() : ''
  careerItem.duration = durationString
}
// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [SET_USER_INFO]: (state, action) => {
    return {
      ...state,
      userInfo: action.userInfo
    }
  },
  [IS_LOADING]: (state, action) => {
    return {
      ...state,
      isLoading: action.isLoading
    }
  },
  [IS_ERROR]: (state, action) => {
    return {
      ...state,
      errorMessage: action.error
    }
  }
}
// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  isLoading: false,
  errorMessage: '',
  userInfo: {}
}

export default function aboutReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
