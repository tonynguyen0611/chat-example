import { connectWithLifecycle } from 'react-lifecycle-component'

import StartView from '../components/start_view'

const mapDispatchToProps = {
}

const mapStateToProps = (state) => {
  return {}
}

export default connectWithLifecycle(
  mapStateToProps, mapDispatchToProps
)(StartView)
