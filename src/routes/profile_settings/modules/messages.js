import UserService from '../../../api/user'
import SendBird from 'sendbird'
import { getUserSendbird } from '../../../helper/send_bird'
import { handleUpdateNotificationMessages, handleAddNewChannelInNotification } from '../../../store/notifications'
// ------------------------------------
// Constants
// ------------------------------------
const CREATE_CHANNEL_SUCCESS = 'CREATE_CHANNEL_SUCCESS'
const RECEIVE_ERROR = 'RECEIVE_ERROR'
const GET_USER_SUCCESS = 'GET_USER_SUCCESS'
const INPUT_REPLY_CHANGE = 'INPUT_REPLY_CHANGE'
const SEND_MESSAGE_SUCCESS = 'SEND_MESSAGE_SUCCESS'
const CONNECT_SEND_BIRD_SUCCESS = 'CONNECT_SEND_BIRD_SUCCESS'
const INIT_SEND_BIRD_SUCCESS = 'INIT_SEND_BIRD_SUCCESS'
const SECLECT_CHANNEL = 'SECLECT_CHANNEL'
const FETCH_CHANNELS_SUCCESS = 'FETCH_CHANNELS_SUCCESS'
const GET_PREVIOUS_MESSAGES_SUCCESS = 'GET_PREVIOUS_MESSAGES_SUCCESS'
const START_LOAD_MESSAGE = 'START_LOAD_MESSAGE'
const ADD_NEW_CHANNEL = 'ADD_NEW_CHANNEL'
const GET_USERS_SUCCESS = 'GET_USERS_SUCCESS'
const RECEIVE_MESSAGE_SUCCESS = 'RECEIVE_MESSAGE_SUCCESS'
const SELECT_CHANNEL_BY_URL = 'SELECT_CHANNEL_BY_URL'

// ------------------------------------
// Actions
// ------------------------------------
export function connectSendBird (sb) {
  return (dispatch, getState) => {
    // '8bab4ab8-dc9d-4d30-b7a8-d327e3570652', '0fe6c83754104e4920e02b51eb1aa0b37599a849'
    if (getState().messages.channelList.length === 0) {
      getUserSendbird()
      .then(res => {
        sb.connect(res.user_id, res.access_token,
          (user, error) => {
            if (error) {
              dispatch(receiveError(error))
              return
            }
            dispatch(getUserSuccess(user))
            dispatch(ConnectSendBirdSuccess())
            dispatch(fetchGroupChannels(sb, user))
          })
      })
      .catch(err => dispatch(receiveError(err)))
    }
  }
}
export function handleDeleteChannel (e, channelUrl) {
  e.stopPropagation()
  return (dispatch, getState) => {
    let moduleState = getState().messages
    let sb = SendBird.getInstance()
    moduleState.channelList.map((channel, i) => {
      if (channel.url === channelUrl) {
        channel.leave((response, error) => {
          if (error) {
            console.error(error)
            return
          }
          dispatch(fetchGroupChannels(sb, moduleState.userSendBird))
        })
      }
    })
  }
}
function ConnectSendBirdSuccess () {
  return {
    type: CONNECT_SEND_BIRD_SUCCESS
  }
}
export function getPreviousMessages (channel) {
  return (dispatch, getState) => {
    dispatch(startLoadMessages())
    let messageListQuery = channel.createPreviousMessageListQuery()
    messageListQuery.load(30, true, (messageList, error) => {
      if (error) {
        return
      }
      dispatch(getPreviousMessagesSuccess(messageList))
    })
  }
}
export function getUsers (conditions) {
  return (dispatch, getState) => {
    let query = {
      offset: 0,
      limit: 6
    }
    UserService(query, conditions)
      .then(res => {
        console.log(res.data)
        dispatch(getUsersSuccess(res.data))
      })
      .catch(err => dispatch(receiveError(err)))
  }
}
function getUsersSuccess (userList) {
  return {
    type: GET_USERS_SUCCESS,
    payload: userList
  }
}
function startLoadMessages () {
  return {
    type: START_LOAD_MESSAGE
  }
}
function getPreviousMessagesSuccess (data, channelUrl) {
  return {
    type: GET_PREVIOUS_MESSAGES_SUCCESS,
    payload: data
  }
}
export function selectChanel (index, user) {
  return (dispatch, getState) => {
    let moduleState = getState().messages
    moduleState.channelList[index].markAsRead()
    dispatch(getPreviousMessages(moduleState.channelList[index]))
    dispatch(selectChanelSuccess(index, user))
  }
}
function selectChanelSuccess (index, user) {
  return {
    type: SECLECT_CHANNEL,
    payload: index,
    user: user
  }
}
export function createNewChannel (userTargetID) {
  return (dispatch, getState) => {
    CreateGroupChanelOneByOne(userTargetID)
    .then(createdChannel => dispatch(createChanelSuccess(createdChannel, getState().messages.userSendBird.userId)))
    .catch(err => dispatch(receiveError(err)))
  }
}

export function fetchGroupChannels (sb, user) {
  return (dispatch, getState) => {
    let channelListQuery = sb.GroupChannel.createMyGroupChannelListQuery()
    let userIds = []
    userIds.push(user.userId)
    channelListQuery.userIdsFilter = userIds
    channelListQuery.includeEmpty = true
    channelListQuery.queryType = 'OR'
    if (channelListQuery.hasNext) {
      channelListQuery.next(function (channelList, error) {
        if (error) {
          console.error(error)
          dispatch(receiveError(error))
          return
        }
        // returns channelA only.
        console.log('all channel', channelList)
        let unreadMessageCount = 0
        let notificationList = []
        channelList.map((channel, i) => {
          notificationList.push(channel)
          if (channel.unreadMessageCount > 0 && channel.lastMessage._sender.userId !== user.userId) {
            unreadMessageCount += 1
          }
        })
        dispatch(handleUpdateNotificationMessages({
          listMessages: notificationList,
          count: unreadMessageCount
        }))
        dispatch(fetchChannelsSuccess(channelList))
        if (channelList[0]) {
          dispatch(getPreviousMessages(channelList[0]))
        }
      })
    }
  }
}
export function fetchChannelsSuccess (data) {
  return {
    type: FETCH_CHANNELS_SUCCESS,
    payload: data
  }
}
function CreateGroupChanelOneByOne (userTargetID, sb) {
  return new Promise(function (resolve, reject) {
    sb.GroupChannel.createChannelWithUserIds([userTargetID], true,
     (createdChannel, error) => {
       if (error) {
         reject(error)
       }
       resolve(createdChannel)
     })
  })
}
export function handleInputReplyChange (event) {
  return {
    type: INPUT_REPLY_CHANGE,
    payload: event.target.value
  }
}
export function handleSendMessage () {
  return (dispatch, getState) => {
    let moduleState = getState().messages
    moduleState.channelList[moduleState.channelIndex].sendUserMessage(moduleState.inputReply,
      (message, error) => {
        if (error) {
          dispatch(receiveError(error))
          return
        }
        moduleState.channelList[moduleState.channelIndex].markAsRead()
        dispatch(sendMessageSuccess(message))
      })
  }
}
export function handleInitSendBirdSuccess () {
  return {
    type: INIT_SEND_BIRD_SUCCESS
  }
}
export function handleAddNewChannel () {
  return {
    type: ADD_NEW_CHANNEL
  }
}
export function receiveMessage (message, channel) {
  return {
    type: RECEIVE_MESSAGE_SUCCESS,
    message: message,
    channel: channel
  }
}
export function handleSelectChannel (channelUrl) {
  return (dispatch, getState) => {
    let moduleState = getState().messages
    let user = {}
    moduleState.channelList.map((channel, i) => {
      if (channel.url === channelUrl) {
        user = getUserActive(channel, moduleState.userSendBird.userId)
        dispatch(SelectChannelByUrl(i, user))
        channel.markAsRead()
        dispatch(getPreviousMessages(channel))
      }
    })
  }
}

function SelectChannelByUrl (index, user) {
  return {
    type: SELECT_CHANNEL_BY_URL,
    payload: index,
    user: user
  }
}
export function handleSelectUserToChat (item, sb) {
  return (dispatch, getState) => {
    let moduleState = getState().messages
    let isChannelExist = false
    let userId = item.id || item.userId
    let index = 0
    console.log('item', item)
    moduleState.channelList.map((channel, i) => {
      channel.members.map((member, j) => {
        if (member.userId === userId && !isChannelExist) {
          isChannelExist = true
          index = i
          dispatch(selectChanel(i, item))
        }
      })
    })
    if (!isChannelExist) {
      CreateGroupChanelOneByOne(userId, sb)
      .then(createdChannel => {
        dispatch(createChanelSuccess(createdChannel, moduleState.userSendBird.userId))
        dispatch(handleAddNewChannelInNotification(createdChannel))
      })
      .catch(err => dispatch(receiveError(err)))
    }
  }
}
function sendMessageSuccess (message) {
  return {
    type: SEND_MESSAGE_SUCCESS,
    payload: message
  }
}
export function getUserActive (channel, currentUserId) {
  let user = null
  if (channel) {
    channel.members.map(member => {
      if (member.userId !== currentUserId) {
        user = member
      }
    })
  }
  return user
}
function createChanelSuccess (data, currentUserId) {
  return {
    type: CREATE_CHANNEL_SUCCESS,
    payload: data,
    user: getUserActive(data, currentUserId)
  }
}
function getUserSuccess (data) {
  return {
    type: GET_USER_SUCCESS,
    payload: data
  }
}
function receiveError (error) {
  console.log(error)
  return {
    type: RECEIVE_ERROR,
    error: error
  }
}
// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [RECEIVE_MESSAGE_SUCCESS]: (state, action) => {
    var d = new Date()
    var currentTimestamp = d.getTime()
    return {
      ...state,
      channelMessages: state.channelMessages.map((channel, i) => channel.url === action.channel.url
      ? { ...channel, messages: [...channel.messages, action.message], lastMessage: action.channel.lastMessage, Priority: currentTimestamp }
      : channel)
    }
  },
  [SEND_MESSAGE_SUCCESS]: (state, action) => {
    var d = new Date()
    var currentTimestamp = d.getTime()
    return {
      ...state,
      inputReply: '',
      channelMessages: state.channelMessages.map((channel, i) => i === state.channelIndex
      ? { ...channel,
        messages: channel.messages ? [...channel.messages, action.payload] : [action.payload],
        Priority: currentTimestamp,
        lastMessage: action.payload
      }
      : channel)
    }
  },
  [INPUT_REPLY_CHANGE]: (state, action) => {
    return {
      ...state,
      inputReply: action.payload
    }
  },
  [RECEIVE_ERROR]: (state, action) => {
    return {
      ...state,
      isLoading: false,
      isLoadingError: true
    }
  },
  [GET_USER_SUCCESS]: (state, action) => {
    return {
      ...state,
      userSendBird: action.payload
    }
  },
  [CREATE_CHANNEL_SUCCESS]: (state, action) => {
    return {
      ...state,
      retrievingChannelsComplete: true,
      channelList: [action.payload, ...state.channelList],
      channelMessages: [action.payload, ...state.channelMessages],
      channelIndex: 0,
      userActive: action.user
    }
  },
  [CONNECT_SEND_BIRD_SUCCESS]: (state, action) => {
    return {
      ...state,
      connectSendBirdSuccess: true
    }
  },
  [INIT_SEND_BIRD_SUCCESS]: (state, action) => {
    return {
      ...state,
      initSendBirdSuccess: true
    }
  },
  [FETCH_CHANNELS_SUCCESS]: (state, action) => {
    return {
      ...state,
      channelList: action.payload,
      channelMessages: action.payload,
      channelIndex: 0,
      retrievingChannelsComplete: true,
      userActive: getUserActive(action.payload[0], state.userSendBird.userId)
    }
  },
  [SECLECT_CHANNEL]: (state, action) => {
    var d = new Date()
    var currentTimestamp = d.getTime()
    return {
      ...state,
      channelIndex: action.payload,
      isSearching: false,
      channelMessages: state.channelMessages.map((channel, i) => channel.url === state.channelList[action.payload].url
      ? { ...channel, Priority: currentTimestamp }
      : channel),
      userActive: action.user
    }
  },
  [SELECT_CHANNEL_BY_URL]: (state, action) => {
    return {
      ...state,
      channelIndex: action.payload,
      isSearching: false,
      userActive: action.user
    }
  },
  [GET_PREVIOUS_MESSAGES_SUCCESS]: (state, action) => {
    return {
      ...state,
      channelMessages: state.channelMessages.map((channel, i) => channel.url === state.channelList[state.channelIndex].url
      ? { ...channel, messages: action.payload, isLoading: false }
      : channel)
    }
  },
  [START_LOAD_MESSAGE]: (state, action) => {
    return {
      ...state,
      channelMessages: state.channelMessages.map((channel, i) => channel.url === state.channelList[state.channelIndex].url
      ? { ...channel, isLoading: true }
      : channel)
    }
  },
  [ADD_NEW_CHANNEL]: (state, action) => {
    return {
      ...state,
      isSearching: true
    }
  },
  [GET_USERS_SUCCESS]: (state, action) => {
    return {
      ...state,
      userList: action.payload
    }
  }
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  channelMessages: [],
  retrievingChannelsComplete: false,
  connectSendBirdSuccess: false,
  initSendBirdSuccess: false,
  isLoading: false,
  isLoadingError: false,
  userSendBird: {},
  channelIndex: 0,
  inputReply: '',
  channelList: [],
  isSearching: false,
  userList: [],
  userActive: {}
}

export default function messagesReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
