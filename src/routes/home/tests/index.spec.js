import React from 'react'
import { shallow, mount } from 'enzyme'
import { Provider } from 'react-redux'
import createStore from '../../../store/createStore'
import createHistory from 'history/createBrowserHistory'
import { ConnectedRouter } from 'react-router-redux'
import configureStore from 'redux-mock-store'

import { injectReducer } from '../../../store/reducers'
import ConnectedHome from '../containers/home'
import HomeView from '../components/HomeView'
import { getNewsFeed, getNewsFeedSuccess } from '../modules/home'

describe('>>>H O M E --- TEST COMPONENT', () => {
  let wrapper
  const history = createHistory()
  const initialState = window.__INITIAL_STATE__
  const store = createStore(initialState, history)

  beforeEach(() => {
    const reducer = require('../modules/home').default
    injectReducer(store, { key: 'home', reducer })
    wrapper = mount(
      <Provider store={store} >
        <ConnectedRouter history={history}>
          <ConnectedHome />
        </ConnectedRouter>
      </Provider>
    )
  })

  it('+++ check Prop matches with initialState', () => {
    expect(wrapper.find(HomeView).prop('newsFeedData')).toHaveLength(0)
  })
})

describe('>>>H O M E --- REACT-REDUX (Mount + wrapping in <Provider>)', () => {
  const initialState = {
    isLoading: false,
    errorMessage: '',
    title: '',
    offset: 0,
    limit: 4,
    hasMore: true,
    newsFeedData: []
  }
  const mockStore = configureStore()
  let store = mockStore(initialState)
  it('+++ check action on dispatching ', () => {
    let action
    store.dispatch(getNewsFeedSuccess([]))
    action = store.getActions()
    expect(action[0].type).toBe("GET_NEWS_FEED_SUCCESS")
  })
})
