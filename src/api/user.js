/**
 * @author ninh
 * @version 0.0.1
 * @date Sep - 08 - 2017
 *
 * @flow
 */

import fetch from './fetch'

export function getNewsFeed (query, conditions) {
  const url = '/newsfeed_item/personalized'
  const options = {
    method: 'GET',
    query: {
      ...query,
      ...conditions
    }
  }
  return fetch(url, options)
}

export function getUsers (query, conditions) {
  const url = '/users'
  const options = {
    method: 'GET',
    query: {
      ...query,
      ...conditions
    }
  }
  return fetch(url, options)
}

export function getUser (userId) {
  const url = '/users/' + userId
  const options = {
    method: 'GET'
  }
  return fetch(url, options)
}

export function updateUser (userId, data) {
  const url = '/users/' + userId
  const options = {
    method: 'PUT',
    data: data
  }
  return fetch(url, options)
}
export function logOut () {
  const url = '/auth'
  const options = {
    method: 'DELETE'
  }
  return fetch(url, options)
}

export function logIn (method, data) {
  const url = '/auth/' + method
  const options = {
    method: 'POST',
    data: data
  }
  return fetch(url, options)
}

export function Register (data) {
  const url = '/users'
  const options = {
    method: 'POST',
    data: data
  }
  return fetch(url, options)
}

export default {
  getNewsFeed,
  getUsers,
  logOut,
  logIn,
  Register,
  getUser,
  updateUser
}
