import axios from 'axios'
import UserService from '../../../api/user'
import moment from 'moment'
import { processUpload } from '../../../helper/image'
import { sendRequestSearch } from '../../../helper/search'
import { getUserDetails } from '../../../helper/user'
import { getUserInfoSuccess } from './header'
// ------------------------------------
// Constants
// ------------------------------------
const RECEIVE_ERROR = 'RECEIVE_ERROR'
const GET_COLLECTIONS_SUCCESS = 'GET_COLLECTIONS_SUCCESS'
const START_CHANGE_AVATAR = 'START_CHANGE_AVATAR'
const CANCEL_CHANGE_AVATAR = 'CANCEL_CHANGE_AVATAR'
const CHANGE_AVATAR_SUCCESS = 'CHANGE_AVATAR_SUCCESS'
const IS_UPLOADING_AVATAR = 'IS_UPLOADING_AVATAR'
const LAST_CAREER_ITEM = 'LAST_CAREER_ITEM'
const IS_LOADING = 'IS_LOADING'
const SET_SIMILAR_LIST = 'SET_SIMILAR_LIST'
const SET_USER_INFO = 'COLLECTION_SET_USER_INFO'
// ------------------------------------
// Actions
// ------------------------------------
export function getUserInfo (userId) {
  return (dispatch, getState) => {
    dispatch(setLoading('isLoadingUserInfo', true))
    getUserDetails(userId)
    .then(res => {
      dispatch(setUserInfo(res))
      dispatch(setLoading('isLoadingUserInfo', false))
      dispatch(getSimilarList(res))
    })
    .catch(err => dispatch(receiveError(err)))
  }
}

function getSimilarList (userInfo) {
  return (dispatch, getState) => {
    if (!userInfo.occupation) return

    let options = {}
    options.limit = 5
    options.searchType = 'users'
    options.conditions = {
      'id': { '$ne': userInfo.id },
      'occupation': {
        '$contains': [ userInfo.occupation[0] ]
      }
    }
    dispatch(setLoading('isLoadingSimilarList', true))
    sendRequestSearch(options)
    .then(res => {
      dispatch(setSimilarList(res))
      dispatch(setLoading('isLoadingSimilarList', false))
    })
    .catch(err => dispatch(receiveError(err)))
  }
}

function setSimilarList (similarList) {
  return {
    type: SET_SIMILAR_LIST,
    similarList: similarList
  }
}

function setLoading (loadingType, value) {
  let loadingObj = {}
  loadingObj[loadingType] = value
  return {
    type: IS_LOADING,
    loadingObj: loadingObj
  }
}

export function handleUpdateAvatar (bufImage) {
  return (dispatch, getState) => {
    let moduleUser = getState().user
    dispatch(uploadingAvatar())
    processUpload(getState().collection.avatarFile, bufImage)
      .then(urlImage => {
        let data = {
          profile_avatar: {
            url: urlImage
          }
        }
        UserService.updateUser(moduleUser.userInfo.id, data)
        .then(res => {
          dispatch(changeAvatarSuccess())
          dispatch(getUserInfoSuccess(res.data)) // change avatar in header
          dispatch(setUserInfo(res.data))
        })
        .catch(err => dispatch(receiveError(err)))
      })
      .catch(err => {
        dispatch(receiveError(err))
      })
  }
}
function uploadingAvatar () {
  return {
    type: IS_UPLOADING_AVATAR
  }
}
export function getCollections (userId) {
  return (dispatch, getState) => {
    let conditions = {
      user_id: userId
    }
    let url = ENVIRONMENT.API_DOMAIN +
      '/albums?offset=' + getState().collection.offset +
      '&limit=' + getState().collection.limit +
      '&conditions=' + JSON.stringify(conditions)
    axios.get(encodeURI(url),
      { withCredentials: true })
      .then(res => dispatch(getCollectionsSuccess(res.data, userId)))
      .catch(err => dispatch(receiveError(err)))
  }
}
export function handleChangeAvatar (event) {
  let file = event.target.files[0]
  return {
    type: START_CHANGE_AVATAR,
    url: URL.createObjectURL(file),
    file: file
  }
}
export function handleCancelChangeAvatar () {
  return {
    type: CANCEL_CHANGE_AVATAR
  }
}
export function getLastCareerItem () {
  return (dispatch, getState) => {
    let careerList = getState().collection.userInfo.career_histories

    if (careerList && careerList.length > 0) {
      for (let i in careerList) {
        careerList[i].diffNow = calculateDiffWithNow(careerList[i])
      }
      let shortedValue = careerList[0].diffNow
      let shortedIndex = 0
      for (let i in careerList) {
        if (careerList[i].diffNow < shortedValue) {
          shortedValue = careerList[i].diffNow
          shortedIndex = i
        }
      }
      dispatch(setLastCareerItem(careerList[shortedIndex]))
    }
  }
}
function calculateDiffWithNow (careerItem) {
  let endTimeString = careerItem.end_time.day + '/' + careerItem.end_time.month + '/' + careerItem.end_time.year
  let unixEndTime = moment(endTimeString, 'DD/MM/YYYY').valueOf()
  let now = moment()
  let diff = now - unixEndTime
  if (diff <= 0) {
    return 0
  } else {
    return diff
  }
}
function setLastCareerItem (careerItem) {
  return {
    type: LAST_CAREER_ITEM,
    lastCareerItem: careerItem
  }
}
function changeAvatarSuccess () {
  return {
    type: CHANGE_AVATAR_SUCCESS
  }
}
function getCollectionsSuccess (data, userId) {
  return {
    type: GET_COLLECTIONS_SUCCESS,
    data: data,
    userId: userId
  }
}
function receiveError (err) {
  return {
    type: RECEIVE_ERROR,
    err: err
  }
}
function setUserInfo (userInfo) {
  return {
    type: SET_USER_INFO,
    userInfo: userInfo
  }
}

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [GET_COLLECTIONS_SUCCESS]: (state, action) => {
    return {
      ...state,
      collectionList: action.data
    }
  },
  [RECEIVE_ERROR]: (state, action) => {
    let error = action.error || ''
    let errorMessage = error.message === '400' ? 'Have problem when upload file' : 'Error'
    return {
      ...state,
      isLoading: false,
      errorMessage: errorMessage
    }
  },
  [START_CHANGE_AVATAR]: (state, action) => {
    return {
      ...state,
      isEditingAvatar: true,
      avatarUrl: action.url,
      avatarFile: action.file
    }
  },
  [CANCEL_CHANGE_AVATAR]: (state) => {
    return {
      ...state,
      isEditingAvatar: false
    }
  },
  [CHANGE_AVATAR_SUCCESS]: (state) => {
    return {
      ...state,
      isEditingAvatar: false,
      isUploadingAvatar: false
    }
  },
  [IS_UPLOADING_AVATAR]: (state) => {
    return {
      ...state,
      isUploadingAvatar: true
    }
  },
  [LAST_CAREER_ITEM]: (state, action) => {
    return {
      ...state,
      lastCareerItem: action.lastCareerItem
    }
  },
  [IS_LOADING]: (state, action) => {
    return {
      ...state,
      ...action.loadingObj
    }
  },
  [SET_SIMILAR_LIST]: (state, action) => {
    return {
      ...state,
      similarList: action.similarList
    }
  },
  [SET_USER_INFO]: (state, action) => {
    return {
      ...state,
      userInfo: action.userInfo
    }
  }
}
// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  isEditingAvatar: false,
  isUploadingAvatar: false,
  isLoadingSimilarList: false,
  isLoadingUserInfo: false,
  avatarUrl: '',
  avatarFile: {},
  offset: 0,
  limit: 10,
  isLoading: false,
  collectionList: [],
  lastCareerItem: {},
  userId: '',
  similarList: [],
  userInfo: {},
  currentUser: JSON.parse(localStorage.getItem('userinfo'))
}

export default function collectionReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
