// Here is where you can define configuration overrides based on the execution environment.
// Supply a key to the default export matching the NODE_ENV that you wish to target, and
// the base configuration will apply your overrides before exporting itself.
module.exports = {
  // compiler_public_path : 'http://${config.server_host}:${config.server_port}/'
  api_domain: process.env.API_DOMAIN || 'https://oct-api-staging.inspicorp.com',
  API_TOKEN_SENDBIRD: process.env.API_TOKEN_SENDBIRD || '84f8fe024ff2f672a2f165c01fb039aef8bce7bf'
}
