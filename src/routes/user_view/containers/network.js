import { connectWithLifecycle } from 'react-lifecycle-component'
import {
  getNetworkList,
  handleNetworkAction,
  setCurrentDir,
  handleInputValueChange,
  handleKeyPress } from '../modules/network'
import NetWork from '../components/network'

const mapDispatchToProps = {
  getNetworkList,
  handleNetworkAction,
  setCurrentDir,
  handleInputValueChange,
  handleKeyPress
}

const mapStateToProps = (state) => {
  return state.network
}

export default connectWithLifecycle(
  mapStateToProps, mapDispatchToProps
)(NetWork)
