import React from 'react'
import { Gateway } from 'react-gateway'
import { Dialog, FlatButton } from 'material-ui'
import DetailsCollection from '../components/details_collection'
import injectTapEventPlugin from 'react-tap-event-plugin'
injectTapEventPlugin()
const customContentStyle = {
  width: '80%',
  minHeight: '80%',
  maxWidth: 'none',
  maxHeight: 'none',
  borderRadius: '10px !important'
}

let DetailsCollectionModal = function(options, onCancel, onNext, onPrevious, onSelectItem) {
  options = options || {}
  return (
    <Gateway into="confirm-modal">
      <Dialog
        modal={false}
        contentStyle={customContentStyle}
        open={options.isOpen}
        onRequestClose={onCancel}
      >
        <DetailsCollection
          collection={options.collection}
          userInfo={options.userInfo}
          mediaItems={options.mediaItems}
          indexMediaItem={options.indexMediaItem}
          onNext={onNext}
          onPrevious={onPrevious}
          onSelectItem={onSelectItem} />
      </Dialog>
    </Gateway>
  )
}

export default DetailsCollectionModal;
