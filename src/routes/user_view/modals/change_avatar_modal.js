import React from 'react'
import { Gateway } from 'react-gateway'
import { Dialog, FlatButton } from 'material-ui'
import ChangeAvatar from './change_avatar'
const customContentStyle = {
  width: '40%',
  minHeight: '80%',
  maxWidth: 'none',
  maxHeight: 'none',
  borderRadius: '10px !important',
}
const style = {
  borderRadius: '10px !important'
}
let ChangeAvatarModal = function (options, onCancel, handleUpdateAvatar) {
  options = options || {}
  let actions = [
    <FlatButton
      label='Cancel'
      primary={true}
      onClick={onCancel}
      onTouchTap={onCancel}
    />
  ]
  return (
    <Gateway into="confirm-modal">
      <Dialog
        // actions={actions}
        modal={true}
        style={style}
        contentStyle={customContentStyle}
        open={true}
      >
        <ChangeAvatar
          avatarUrl={options.avatarUrl}
          handleUpdateAvatar={handleUpdateAvatar}
          onCancel={onCancel}
          isUploading={options.isUploading}
          />
      </Dialog>
    </Gateway>
  )
}

export default ChangeAvatarModal
