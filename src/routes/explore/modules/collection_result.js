import { sendRequestSearch } from '../../../helper/search'
// ------------------------------------
// Constants
// ------------------------------------
const IS_LOADING = 'IS_LOADING'
const SET_COLLECTION_LIST = 'SET_COLLECTION_LIST'
const IS_ERROR = 'IS_ERROR'
// ------------------------------------
// Actions
// ------------------------------------

export function getCollectionResultList (key) {
  return (dispatch, getState) => {
    let options = {}
    options.limit = 50
    options.searchType = 'albums'
    options.sort = '-time_created'
    dispatch(setLoading('isLoadingCollectionList', true))
    sendRequestSearch(options)
    .then(res => {
      dispatch(setCollectionList(res))
      dispatch(setLoading('isLoadingCollectionList', false))
    })
    .catch(err => dispatch(setErrorMessage(err)))
  }
}

function setCollectionList (value) {
  return {
    type: SET_COLLECTION_LIST,
    collectionList: value
  }
}

function setLoading (loadingType, value) {
  let loadingObj = {}
  loadingObj[loadingType] = value
  return {
    type: IS_LOADING,
    loadingObj: loadingObj
  }
}

function setErrorMessage (error) {
  return {
    type: IS_ERROR,
    errorMessage: error
  }
}
// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [IS_LOADING]: (state, action) => {
    return {
      ...state,
      ...action.loadingObj
    }
  },
  [IS_ERROR]: (state, action) => {
    return {
      ...state,
      isLoadingCollectionList: false,
      isLoadingPeopleList: false,
      isError: true,
      errorMessage: action.errorMessage
    }
  },
  [SET_COLLECTION_LIST]: (state, action) => {
    return {
      ...state,
      collectionList: action.collectionList
    }
  }
}
// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  isLoadingCollectionList: false,
  isError: false,
  isSuccess: false,
  errorMessage: '',
  successMessage: '',
  collectionList: []
}

export default function exploreReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
