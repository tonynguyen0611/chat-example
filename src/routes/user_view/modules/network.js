import { sendRequestNetworkList, sendRequestNetworkAction } from '../../../helper/network'
// ------------------------------------
// Constants
// ------------------------------------
const INPUT_VALUE = 'INPUT_VALUE'
const IS_SUCCESS = 'IS_SUCCESS'
const IS_ERROR = 'IS_ERROR'
const IS_LOADING = 'IS_LOADING'
const SET_NETWORK_LIST = 'SET_NETWORK_LIST'
const SET_CURRENT_DIR = 'SET_CURRENT_DIR'
// ------------------------------------
// Actions
// ------------------------------------
export function getNetworkList (userId, requestType) {
  return (dispatch, getState) => {
    let options = {}
    options.userId = userId

    if (requestType === 'search') {
      dispatch(setNetworkList([]))
      return
    }
    dispatch(setLoading('isLoadingList'))
    sendRequestNetworkList(options, requestType)
    .then(res => {
      customizeNetworkList(res)
      dispatch(setNetworkList(res))
      dispatch(setSuccessMessage(''))
    })
    .catch(err => dispatch(setErrorMessage(err)))
  }
}

export function handleInputValueChange (e, divName) {
  return (dispatch, getState) => {
    let input = {}
    input[divName] = e.target.value
    dispatch(setInput(input))
  }
}

export function handleKeyPress (e) {
  return (dispatch, getState) => {
    if (e.key === 'Enter') {
      let moduleState = getState().network
      let userId = moduleState.userId
      let searchKey = moduleState.searchKey
      let options = {}
      options.userId = userId
      options.conditions = {
        'full_name': { '$iLike' : '%' + searchKey + '%' }
      }

      dispatch(setLoading('isLoadingList'))

      sendRequestNetworkList(options, 'collaborators')
      .then(res => {
        customizeNetworkList(res)
        dispatch(setNetworkList(res.slice()))
        dispatch(setSuccessMessage(''))
      })
      .catch(err => dispatch(setErrorMessage(err)))
    }
  }
}

export function handleNetworkAction (networkItem, requestType) {
  return (dispatch, getState) => {
    let networkList = getState().network.networkList
    let id = ''
    if (requestType === 'unfollow') {
      id = networkItem.targeted_user_connections[0].id
    } else {
      id = networkItem.id
    }
    sendRequestNetworkAction(id, requestType)
    .then(res => {
      let indexOfItem = networkList.indexOf(networkItem)
      networkList[indexOfItem].isFollowing = !networkList[indexOfItem].isFollowing
      dispatch(setNetworkList(networkList.slice()))
    })
    .catch(err => dispatch(setErrorMessage(err)))
  }
}

export function setCurrentDir (dir, id) {
  return {
    type: SET_CURRENT_DIR,
    dir: dir,
    userId: id
  }
}

function customizeNetworkList (networkList) {
  for (let i in networkList) {
    if (networkList[i].targeted_user_connections &&
        networkList[i].targeted_user_connections[0] &&
        networkList[i].targeted_user_connections[0].status === 'active') {
      networkList[i].isFollowing = true
    }
  }
}

function setNetworkList (newList) {
  return {
    type: SET_NETWORK_LIST,
    networkList: newList
  }
}

function setLoading (loadingType) {
  let loadingObj = {}
  loadingObj[loadingType] = true
  return {
    type: IS_LOADING,
    loadingObj: loadingObj
  }
}

function setInput (input) {
  return {
    type: INPUT_VALUE,
    input: input
  }
}

function setSuccessMessage (msg) {
  return {
    type: IS_SUCCESS,
    successMessage: msg
  }
}

function setErrorMessage (err) {
  return {
    type: IS_ERROR,
    error: err
  }
}
// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [INPUT_VALUE]: (state, action) => {
    return {
      ...state,
      ...action.input
    }
  },
  [IS_LOADING]: (state, action) => {
    return {
      ...state,
      ...action.loadingObj,
      isError: false,
      isSuccess: false,
      successMessage: '',
      errorMessage: ''
    }
  },
  [IS_ERROR]: (state, action) => {
    return {
      ...state,
      isLoadingList: false,
      isError: true,
      errorMessage: action.error
    }
  },
  [IS_SUCCESS]: (state, action) => {
    return {
      ...state,
      isLoadingList: false,
      isSuccess: true,
      successMessage: action.successMessage
    }
  },
  [SET_NETWORK_LIST]: (state, action) => {
    return {
      ...state,
      networkList: action.networkList
    }
  },
  [SET_CURRENT_DIR]: (state, action) => {
    return {
      ...state,
      dir: action.dir,
      userId: action.userId
    }
  }
}
// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  isLoadingList: false,
  isError: false,
  isSuccess: false,
  errorMessage: '',
  successMessage: '',
  searchKeyString: '',
  result: [],
  networkList: [],
  dir: '',
  userId: ''
}

export default function networkReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
