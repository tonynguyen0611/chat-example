import axios from 'axios'
import { validate } from './../../../utils/validate'
// ------------------------------------
// Constants
// ------------------------------------
const INPUT_VALUE = 'INPUT_VALUE'
const IS_SUCCESS = 'IS_SUCCESS'
const IS_ERROR = 'IS_ERROR'
const IS_LOADING = 'IS_LOADING'
const INITIALIZE = 'INITIALIZE'
const CLEAR_INPUT = 'CLEAR_INPUT'
const userInfo = JSON.parse(localStorage.getItem('userinfo'))
// ------------------------------------
// Actions
// ------------------------------------
export function initialize () {
  return {
    type: INITIALIZE
  }
}

export function handleInputValueChange (e, divName) {
  return (dispatch, getState) => {
    let input = {}
    input[divName] = e.target.value
    dispatch(setInput(input))
  }
}

export function submit () {
  return (dispatch, getState) => {
    let moduleState = getState().changePassword

    dispatch(setLoading())

    if (!moduleState['currentPassword']) {
      return dispatch(setErrorMessage('Please enter your current password'))
    }

    let validationMsg = validate('password', moduleState['newPassword'])
    if (validationMsg !== 'NO_ERROR') {
      return dispatch(setErrorMessage('New ' + validationMsg))
    }

    if (moduleState['newPassword'] !== moduleState['confirmNewPassword']) {
      return dispatch(setErrorMessage('Confirm new password did not match new password'))
    }

    dispatch(sendRequestChangePassword())
  }
}

function sendRequestChangePassword () {
  return (dispatch, getState) => {
    let moduleState = getState().changePassword
    console.log('send request change password')
    axios.post(ENVIRONMENT.API_DOMAIN + '/users/current_user/change_password', {
      'old_password': moduleState.currentPassword,
      'new_password': moduleState.newPassword
    }, { withCredentials: true })
    .then(res => dispatch(changePasswordSuccess(res.data)))
    .catch(err => {
      let errorMsg = err.response.data.error || 'ERROR'
      if (errorMsg === 'WRONG_PASSWORD') {
        errorMsg = 'Current password is wrong'
        dispatch(clearInput())
      }
      dispatch(setErrorMessage(errorMsg))
    })
  }
}

function clearInput () {
  return {
    type: CLEAR_INPUT
  }
}

function changePasswordSuccess (response) {
  return (dispatch, getState) => {
    dispatch(setSuccessMessage('Change password successfully'))
  }
}

function setLoading () {
  return {
    type: IS_LOADING
  }
}

function setInput (input) {
  return {
    type: INPUT_VALUE,
    input: input
  }
}

function setSuccessMessage (msg) {
  return {
    type: IS_SUCCESS,
    successMessage: msg
  }
}

function setErrorMessage (err) {
  return {
    type: IS_ERROR,
    error: err
  }
}
// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [INPUT_VALUE]: (state, action) => {
    return {
      ...state,
      ...action.input
    }
  },
  [IS_LOADING]: (state, action) => {
    return {
      ...state,
      isChangePasswordLoading: true,
      isError: false,
      isSuccess: false,
      successMessage: '',
      errorMessage: ''
    }
  },
  [IS_ERROR]: (state, action) => {
    return {
      ...state,
      isChangePasswordLoading: false,
      isError: true,
      errorMessage: action.error
    }
  },
  [IS_SUCCESS]: (state, action) => {
    return {
      ...state,
      isChangePasswordLoading: false,
      isSuccess: true,
      successMessage: action.successMessage,
      currentPassword: '',
      newPassword: '',
      confirmNewPassword: ''
    }
  },
  [CLEAR_INPUT]: (state, action) => {
    return {
      ...state,
      currentPassword: '',
      newPassword: '',
      confirmNewPassword: ''
    }
  },
  [INITIALIZE]: (state, action) => {
    return {
      ...state,
      ...initialState
    }
  }
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  isChangePasswordLoading: false,
  isError: false,
  isSuccess: false,
  errorMessage: '',
  successMessage: '',
  currentPassword: '',
  newPassword: '',
  confirmNewPassword: '',
  isSocialAcount: Boolean(userInfo.facebook_id) || Boolean(userInfo.google_id)
}

export default function changePasswordReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
