import axios from 'axios'

const API_TOKEN = '84f8fe024ff2f672a2f165c01fb039aef8bce7bf'
const API_SENDBIRD = 'https://api.sendbird.com/v3'

export function getUserSendbird () {
  return new Promise(function (resolve, reject) {
    console.log('start get user sendbird')
    axios.get(ENVIRONMENT.API_DOMAIN + '/users/sendbird', { withCredentials: true })
      .then(res => {
        if (res.data.message === 'User not found.') {
          createUserSendbird()
            .then(data => {
              resolve(data)
            })
            .catch(err => {
              reject(err)
            })
        } else {
          resolve(res.data)
        }
      })
      .catch(err => {
        reject(err)
      })
  })
}
export function createUserSendbird() {
  return new Promise(function (resolve, reject) {
    axios.post(ENVIRONMENT.API_DOMAIN + '/users/sendbird', { },
    { withCredentials: true })
    .then(res => {
      resolve(res.data)
    })
    .catch(err => reject(err.response.data))
  })
}
export function createUser (userData) {
  return new Promise(function (resolve, reject) {
    axios.post(API_SENDBIRD + '/users', {
      ...userData
    }, {
      headers: {
        'Content-Type': 'application/json, charset=utf8',
        'Api-Token': API_TOKEN
      }
    })
      .then(res => {
        resolve(res.data)
      })
      .catch(err => reject(err.response.data))
  })
}

export function CreateGroupChanel (userList) {
  return new Promise(function (resolve, reject) {
    axios.post(API_SENDBIRD + '/group_channels', {
      user_ids: userList,
      is_distinct: true
    }, {
      headers: {
        'Content-Type': 'application/json, charset=utf8',
        'Api-Token': API_TOKEN
      }
    })
      .then(res => {
        resolve(res.data)
      })
      .catch(err => reject(err.response.data))
  })
}

export function sendMessage (channelUrl, message) {
  return new Promise(function (resolve, reject) {
    axios.post(API_SENDBIRD + '/group_channels/' + channelUrl + '/message', {
      ...message
    }, {
      headers: {
        'Content-Type': 'application/json, charset=utf8',
        'Api-Token': API_TOKEN
      }
    })
      .then(res => {
        resolve(res.data)
      })
      .catch(err => reject(err.response.data))
  })
}