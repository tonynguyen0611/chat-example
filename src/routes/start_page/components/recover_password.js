import React from 'react'
import PropTypes from 'prop-types'
import './recover_password.scss'
import { Link } from 'react-router-dom'

export const RecoverPassword =
({
  isLoading,
  isRecoverError,
  isRecoverSuccess,
  errorMessage,
  email,
  handleEmailChange,
  submit
}) => {
  return (
    <div className='recover-password'>
      {
        isRecoverSuccess &&
        <div className='recover-success'>
          <p> Password reset instruction sent! <br /> Please follow the instruction in your email </p>
          <Link to='/start/login'> Back to Log in </Link>
        </div>
      }
      {
        !isRecoverSuccess &&
        <div className='input-group'>
          <input className={'form-control ' + (isLoading ? 'input-lock' : '')}
            placeholder='Enter your email to receive the password reset instruction'
            type='email'
            onChange={handleEmailChange} />
          <h5> {isRecoverError ? errorMessage : ''} </h5>
          <button className='btn btn-block btn-start btn-start-gray' onClick={submit}>
            {!isLoading ? 'Submit'
              : <img src='https://cdn.zenquiz.net/static/Assets/loading-animation.gif' alt='Loading' />}
          </button>
          <Link to='/start/login'> Back to Log in </Link>
        </div>
      }
    </div>
  )
}

RecoverPassword.propTypes = {
  isLoading: PropTypes.bool,
  isRecoverError: PropTypes.bool,
  isRecoverSuccess: PropTypes.bool,
  errorMessage: PropTypes.string,
  email: PropTypes.string,
  handleEmailChange: PropTypes.func,
  submit: PropTypes.func
}

export default RecoverPassword

