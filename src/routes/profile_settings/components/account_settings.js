import React from 'react'
import PropTypes from 'prop-types'

export const AccountSettings = ({ title }) => {
  return (
    <div className='col-md-8 side-bar-right'>
      <div className='profile-details account-settings'>
        <div className='header-block'>
          <h5 className='title'>
            Account Settings
          </h5>
        </div>
        <div className='content-block'>
          <div className='row'>
            <div className='col-lg-6 col-md-6 col-sm-12 col-xs-12'>
              <div className='form-group label-floating is-select'>
                <label className='control-label'>Who can friend you?</label>
                <select className='selectpicker form-control' size='auto'>
                  <option value='ALL'>Everyone</option>
                  <option value='FOF'>Friends of friends</option>
                </select>
              </div>
            </div>
            <div className='col-lg-6 col-md-6 col-sm-12 col-xs-12'>
              <div className='form-group label-floating is-select'>
                <label className='control-label'>Who can view your post?</label>
                <select className='selectpicker form-control' size='auto'>
                  <option value='FO'>Friends Only</option>
                  <option value='ALL'>Every one</option>
                </select>
              </div>
            </div>
          </div>
          <div className='row'>
            <div className='col-lg-9 col-md-9 col-sm-12 col-xs-12'>
              <div className='col-element'>Notification Email</div>
              <div className='col-element'>
                We'll send you an email to your account each time you receive a new activity
              </div>
            </div>
            <div className='col-lg-3 col-md-3 col-sm-12 col-xs-12'>
              <label className='switch'>
                <input onClick='' type='checkbox' />
                <div className='slider round' />
              </label>
            </div>
          </div>
          <div className='row'>
            <div className='col-lg-6 col-md-6 col-sm-12 col-xs-12'>
              <button className='btn btn-lg btn-bottom btn-left'>Restore all Attributes</button>
            </div>
            <div className='col-lg-6 col-md-6 col-sm-12 col-xs-12'>
              <button className='btn btn-lg btn-bottom btn-right'>Save all Changes</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

AccountSettings.propTypes = {
  title: PropTypes.string.isRequired
}
export default AccountSettings
