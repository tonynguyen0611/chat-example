import { connectWithLifecycle } from 'react-lifecycle-component'
import {
  getUserInfo,
  handelUploadCoverPhoto,
  handleSaveChangeCoverPhoto,
  handleCancelChangeCoverPhoto,
  handleNetworkAction } from '../modules/header'
import { handleSelectedSenderMessages } from '../../../store/notifications'
import Header from '../components/header'

const mapDispatchToProps = (dispatch, ownProps) => {
  dispatch(getUserInfo(ownProps.userId))
  return {
    handleSelectedSenderMessages: (user) => { dispatch(handleSelectedSenderMessages(user)) },
    handelUploadCoverPhoto: (event) => { dispatch(handelUploadCoverPhoto(event)) },
    handleSaveChangeCoverPhoto: (bufImage) => { dispatch(handleSaveChangeCoverPhoto(bufImage)) },
    handleCancelChangeCoverPhoto: () => { dispatch(handleCancelChangeCoverPhoto()) },
    handleNetworkAction: (userId, requestType) => { dispatch(handleNetworkAction(userId, requestType)) }
  }
}

const mapStateToProps = (state) => {
  return state.user
}

export default connectWithLifecycle(
  mapStateToProps, mapDispatchToProps
)(Header)
