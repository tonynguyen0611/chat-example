import React from 'react'
import PropTypes from 'prop-types'
import './input_search.scss'

let timeout = null
class InputSearch extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      isShow: false
    }
  }
  handleInputSearchChange = (e) => {
    e.persist()
    let value = e.target.value
    let conditions = {
      $or: [
        {
          email: {
            $iLike: '%' + value + '%'
          }
        }, {
          full_name: {
            $iLike: '%' + value + '%'
          }
        }
      ]
    }
    clearTimeout(timeout)
    timeout = setTimeout(() => {
      this.props.searchFunction(conditions)
    }, 500)
  }
  ToggleShowSearchResult = (isShow) => {
    setTimeout(() => {
      this.setState({
        isShow: isShow
      })
    }, 200)
  }
  handleSelectItem = (item) => {
    this.props.handleSelectItem(item)
  }
  render () {
    return (
      <div className='InputSearch'>
        <input className='input-search'
          type='text'
          placeholder={this.props.placeholder}
          onFocus={() => this.ToggleShowSearchResult(true)}
          onBlur={() => this.ToggleShowSearchResult(false)}
          onChange={(e) => this.handleInputSearchChange(e)} />
        {this.props.arrResponses.length > 0 && this.state.isShow &&
          <div className='search-result'>
            {this.props.arrResponses.map((item, i) => (
              <div className='item col-md-12' key={i} onClick={() => this.handleSelectItem(item)} >
                <div className='col-md-2'>
                  <img src={item.profile_avatar.url} className='img-responsive' height='30' />
                </div>
                <div className='col-md-10'>
                  <span>{item.full_name}</span>
                </div>
              </div>
            ))
            }
          </div>
        }
      </div>
    )
  }
}

InputSearch.propTypes = {
  searchFunction: PropTypes.func.isRequired,
  handleSelectItem: PropTypes.func.isRequired,
  placeholder: PropTypes.string,
  arrResponses: PropTypes.array
}

export default InputSearch
