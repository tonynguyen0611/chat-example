import update from 'immutability-helper'
import UserService from '../../../api/user'
import { processUpload } from '../../../helper/image'
import { sendRequestNetworkList, sendRequestNetworkAction, sendRequestNetworkCount } from '../../../helper/network'
// ------------------------------------
// Constants
// ------------------------------------
const GET_USER_INFO_SUCCESS = 'GET_USER_INFO_SUCCESS'
const RECEIVE_ERROR = 'RECEIVE_ERROR'
const DATA_LOADING = 'DATA_LOADING'
const UPLOAD_COVER_PHOTO_SUCCESS = 'UPLOAD_COVER_PHOTO_SUCCESS'
const USER_INFO = 'userinfo'
const CHANGE_COVER_PHOTO_SUCCESS = 'CHANGE_COVER_PHOTO_SUCCESS'
const CANCEL_CHANGE_COVER_PHOTO = 'CANCEL_CHANGE_COVER_PHOTO'
const START_CHANGE_COVER_PHOTO = 'START_CHANGE_COVER_PHOTO'
const SET_CURRENT_USER = 'SET_CURRENT_USER'
const SET_FOLLOWING = 'SET_FOLLOWING'
const SET_NETWORK_COUNT = 'SET_NETWORK_COUNT'
// ------------------------------------
// Actions
// ------------------------------------
export function getUserInfo (userId) {
  return (dispatch, getState) => {
    const moduleState = getState().user
    const isOwner = getState().user.isOwner
    const currentUserId = getState().user.currentUser.id
    // userId existed means we already loaded user data and set it to userInfo in state
    if (userId === moduleState.userInfo.id) return
    dispatch(dataLoading())
    UserService.getUser(userId)
    .then(res => {
      if (!isOwner) {
        dispatch(checkFollowing(currentUserId, userId))
      }
      dispatch(countNetwork(userId))
      dispatch(getUserInfoSuccess(res.data))
    })
    .catch(err => dispatch(receiveError(err)))
  }
}

function countNetwork (userId) {
  return (dispatch, getState) => {
    sendRequestNetworkCount(userId, 'followers')
    .then(res => {
      dispatch(setNetworkCount('followersCount', res.count))
    })
    .catch(err => dispatch(receiveError(err)))

    sendRequestNetworkCount(userId, 'following')
    .then(res => {
      dispatch(setNetworkCount('followingCount', res.count))
    })
    .catch(err => dispatch(receiveError(err)))
  }
}

function setNetworkCount (networkType, value) {
  let networkCount = {}
  networkCount[networkType] = value
  return {
    type: SET_NETWORK_COUNT,
    networkCount: networkCount
  }
}

function setFollowing (isFollowing, connectionId) {
  return {
    type: SET_FOLLOWING,
    isFollowing: isFollowing,
    connectionId: connectionId
  }
}

function checkFollowing (currentUserId, checkingUserId) {
  return (dispatch, getState) => {
    let options = {}
    options.userId = currentUserId
    options.conditions = {
      'id': checkingUserId
    }
    sendRequestNetworkList(options, 'following')
    .then(res => {
      if (res.length > 0) {
        dispatch(setFollowing(true, res[0].targeted_user_connections[0].id))
      } else {
        dispatch(setFollowing(false))
      }
    })
    .catch(err => dispatch(receiveError(err)))
  }
}

export function handleNetworkAction (userId, requestType) {
  return (dispatch, getState) => {
    let connectionId = getState().user.connectionId
    let id = ''

    if (requestType === 'follow') {
      id = userId
    } else {
      id = connectionId
    }
    sendRequestNetworkAction(id, requestType)
    .then(res => {
      if (requestType === 'unfollow') {
        dispatch(setFollowing(false))
      } else {
        dispatch(setFollowing(true, res.id))
      }
      dispatch(countNetwork(userId))
    })
    .catch(err => dispatch(receiveError(err)))
  }
}

export function handleCancelChangeCoverPhoto () {
  return {
    type: CANCEL_CHANGE_COVER_PHOTO
  }
}
export function handelUploadCoverPhoto (event) {
  return (dispatch) => {
    let file = event.target.files[0]
    if (file) {
      let url = URL.createObjectURL(file)
      let img = new Image()
      img.onload = () => {
        console.log('img.width', img.width, img.height)
        if (img.width < 700 || img.height < 200) {
          return dispatch(receiveError('IMAGE DIMENSION NOT VALID'))
        } else {
          dispatch(uploadCoverPhotoSuccess(url, file))
          // upLoadImage(file)
          //   .then((url) => dispatch(uploadCoverPhotoSuccess(url)))
          //   .catch((err) => dispatch(receiveError(err)))
        }
      }
      img.src = url
    }
  }
}
export function handleSaveChangeCoverPhoto (bufImage) {
  return (dispatch, getState) => {
    const moduleState = getState().user
    dispatch(startChangeCoverPhoto())
    processUpload(getState().user.coverFile, bufImage)
      .then(urlImage => {
        let data = {
          profile_cover: {
            url: urlImage
          }
        }
        UserService.updateUser(moduleState.userInfo.id, data)
          .then(res => dispatch(changeCoverPhotoSuccess(urlImage)))
          .catch(err => dispatch(receiveError(err)))
      })
      .catch(err => {
        dispatch(receiveError(err))
      })
  }
}
export function setCurrentUSer (currentUser) {
  return {
    type: SET_CURRENT_USER,
    payload: currentUser
  }
}
function startChangeCoverPhoto () {
  return {
    type: START_CHANGE_COVER_PHOTO
  }
}
function changeCoverPhotoSuccess (urlImage) {
  return {
    type: CHANGE_COVER_PHOTO_SUCCESS,
    coverPhoto: urlImage
  }
}
function uploadCoverPhotoSuccess (url, file) {
  return {
    type: UPLOAD_COVER_PHOTO_SUCCESS,
    url: url,
    file: file
  }
}

export function getUserInfoSuccess (user) {
  return {
    type: GET_USER_INFO_SUCCESS,
    userInfo: user
  }
}

function receiveError (error) {
  console.log(error)
  return {
    type: RECEIVE_ERROR,
    error: error
  }
}

function dataLoading () {
  return {
    type: DATA_LOADING
  }
}

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [SET_CURRENT_USER]: (state, action) => {
    if (state && state.currentUser) {
      localStorage.setItem(USER_INFO, JSON.stringify(Object.assign(state.currentUser, action.payload)))
    } else {
      localStorage.setItem(USER_INFO, JSON.stringify(action.payload))
    }
    return {
      ...state,
      currentUser: action.payload
    }
  },
  [CANCEL_CHANGE_COVER_PHOTO]: (state) => {
    return {
      ...state,
      isEditingCoverPhoto: false
    }
  },
  [CHANGE_COVER_PHOTO_SUCCESS]: (state, action) => {
    return {
      ...state,
      isEditingCoverPhoto: false,
      isUploading: false,
      userInfo: {
        ...state.userInfo,
        profile_cover: {
          ...state.userInfo.profile_cover,
          url: action.coverPhoto
        }
      }
    }
  },
  [UPLOAD_COVER_PHOTO_SUCCESS]: (state, action) => {
    return {
      ...state,
      coverPhoto: action.url,
      coverFile: action.file,
      isEditingCoverPhoto: true
    }
  },
  [GET_USER_INFO_SUCCESS]: (state, action) => {
    if (state.currentUser.id === action.userInfo.id) {
      localStorage.setItem(USER_INFO, JSON.stringify(Object.assign(state.currentUser, action.userInfo)))
    }
    return {
      ...state,
      isLoading: false,
      userInfo: action.userInfo,
      isOwner: state.currentUser.id === action.userInfo.id,
      currentUser: (state.currentUser.id === action.userInfo.id) ? action.userInfo : state.currentUser
    }
  },
  [DATA_LOADING]: (state) => {
    state = update(state, {
      isLoading: { $set: true }
    })
    return state
  },
  [RECEIVE_ERROR]: (state, action) => {
    return {
      ...state,
      isLoading: false,
      isLoadingError: true,
      errorMessage: action.error
    }
  },
  [START_CHANGE_COVER_PHOTO]: (state, action) => {
    return {
      ...state,
      isUploading: true
    }
  },
  [SET_FOLLOWING]: (state, action) => {
    return {
      ...state,
      isFollowing: action.isFollowing,
      connectionId: action.connectionId
    }
  },
  [SET_NETWORK_COUNT]: (state, action) => {
    return {
      ...state,
      ...action.networkCount
    }
  }
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  coverPhoto: '',
  coverFile: {},
  isOwner: false,
  isEditingCoverPhoto: false,
  isUploading: false,
  isLoading: false,
  isLoadingError: false,
  isFollowing: false,
  connectionId: '',
  errorMessage: '',
  userId: '',
  userInfo: {},
  currentUser: JSON.parse(localStorage.getItem(USER_INFO)),
  isShowModal: false,
  followersCount: 0,
  followingCount: 0
}

export default function headerReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
