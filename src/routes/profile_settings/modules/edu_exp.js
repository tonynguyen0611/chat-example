import axios from 'axios'
import { setCurrentUSer } from '../../user_view/modules/header'
import moment from 'moment'
// ------------------------------------
// Constants
// ------------------------------------
const IS_SUCCESS = 'IS_SUCCESS'
const IS_ERROR = 'IS_ERROR'
const IS_LOADING = 'IS_LOADING'
const UPDATE_LIST = 'UPDATE_LIST'
const INIT_LIST = 'INIT_LIST'
const CLEAR_INPUT = 'CLEAR_INPUT'
const SET_BACKUP_LIST = 'SET_BACKUP_LIST'
const userInfo = JSON.parse(localStorage.getItem('userinfo'))
var initialCareerList = []
// ------------------------------------
// Actions
// ------------------------------------
function getNewCareerItem () {
  return ({
    title: '',
    place: '',
    description: '',
    start_time: {
      year: '',
      month: '',
      day: ''
    },
    end_time: {
      year: '',
      month: '',
      day: ''
    }
  })
}

export function addCareerItem () {
  return (dispatch, getState) => {
    let careerList = getState().eduExp.careerList
    // let newCareerItem = getNewCareerItem()
    careerList.push(getNewCareerItem())
    dispatch(updateCareerList(careerList))
  }
}

export function getUserDetails () {
  return (dispatch, getState) => {
    dispatch(setLoading('isDetailsLoading', true))
    axios.get(ENVIRONMENT.API_DOMAIN + '/users/' + userInfo.id, { withCredentials: true })
    .then(res => {
      dispatch(setBackupList(res.data.career_histories.slice()))
      dispatch(setCareerList(res.data.career_histories.slice()))
      dispatch(setLoading('isDetailsLoading', false))
    })
    .catch(err => dispatch(setErrorMessage(err.response ? err.response.data.error : err)))
  }
}

export function handleInputValueChange (e, index, divName) {
  return (dispatch, getState) => {
    let careerList = getState().eduExp.careerList
    if (divName === 'start_time' || divName === 'end_time') {
      let tempObj = {}
      let splittedInput = e.split('/')
      tempObj.day = splittedInput[0]
      tempObj.month = splittedInput[1]
      tempObj.year = splittedInput[2]
      careerList[index][divName] = tempObj
    } else {
      careerList[index][divName] = e.target.value.trim()
    }
    dispatch(updateCareerList(careerList))
  }
}

export function clearInput () {
  return (dispatch, getState) => {
    let backupList = getState().eduExp.backupList
    console.log('clear input', initialCareerList)
    dispatch(setCareerList(backupList.slice()))
  }
}

export function saveChanges () {
  return (dispatch, getState) => {
    let careerList = getState().eduExp.careerList
    dispatch(setLoading('isSaveLoading', true))
    if (careerList.length > 0) {
      let newCareerList = []
      for (let i in careerList) {
        let isValidationPassed = true

        // Validate all fields are filled in or not
        if (!areAllFilled(careerList[i])) {
          isValidationPassed = false
          dispatch(setErrorMessage('Please fill in all fields'))
        }

        // Validate start_time and end_time
        if (!isChosenTimeCorrect(careerList[i])) {
          isValidationPassed = false
          dispatch(setErrorMessage('Start time should be less than end time'))
        }

        // if all validations passed, start sending request
        if (isValidationPassed) {
          sendRequestForCareer(careerList[i])
          .then(res => {
            newCareerList.push(res)
            if (newCareerList.length === careerList.length) {
              dispatch(updateSuccess(newCareerList))
            }
          })
          .catch(err => dispatch(setErrorMessage(err)))
        }
      }
    } else {
      dispatch(setErrorMessage('NO_INFO'))
    }
  }
}

function sendRequestForCareer (data, requestType) {
  return new Promise((resolve, reject) => {
    let basedUrl = ENVIRONMENT.API_DOMAIN + '/career_histories'
    let finalUrl = data.id ? (basedUrl + '/' + data.id) : basedUrl

    if (requestType === 'delete') {
      axios.delete(finalUrl, { withCredentials: true })
      .then(res => resolve(res.data))
      .catch(err => reject(err.response.data.error))
    } else {
      if (data.id) {
        axios.put(finalUrl, data, { withCredentials: true })
        .then(res => resolve(res.data))
        .catch(err => reject(err.response.data.error))
      } else {
        axios.post(finalUrl, data, { withCredentials: true })
        .then(res => resolve(res.data))
        .catch(err => reject(err.response.data.error))
      }
    }
  })
}

export function deleteItem (item) {
  return (dispatch, getState) => {
    console.log('delete careerItem', item)
    let careerList = getState().eduExp.careerList
    let indexOfItem = careerList.indexOf(item)
    console.log('index', indexOfItem)
    careerList.splice(indexOfItem, 1)
    console.log('after delete', careerList)

    if (item.id) {
      sendRequestForCareer(item, 'delete')
      .then(res => {
        console.log('response delete', res)
        dispatch(setCareerList(careerList.slice()))
        dispatch(setBackupList(careerList.slice()))
      })
      .catch(err => {
        dispatch(setErrorMessage(err))
      })
    } else {
      dispatch(setCareerList(careerList.slice()))
      dispatch(setBackupList(careerList.slice()))
    }
  }
}

function updateCareerList (newCareerList) {
  return {
    type: UPDATE_LIST,
    careerList: newCareerList
  }
}

function updateSuccess (newCareerList) {
  return (dispatch, getState) => {
    let newUserInfo = Object.assign({}, userInfo)
    newUserInfo.career_history = newCareerList
    dispatch(setCurrentUSer(newUserInfo))
    dispatch(setBackupList(newCareerList.slice()))
    dispatch(updateCareerList(newCareerList.slice()))
    dispatch(setSuccessMessage('Updated successfully'))
  }
}

function setCareerList (newCareerList) {
  return {
    type: INIT_LIST,
    careerList: newCareerList
  }
}

function setBackupList (newList) {
  return {
    type: SET_BACKUP_LIST,
    backupList: newList
  }
}

function areAllFilled (careerItem) {
  for (let i in careerItem) {
    console.log('check input', i, careerItem[i])
    if (!careerItem[i] || ((i === 'start_time' || i === 'end_time') && !careerItem[i].day)) {
      console.log('is null :', i)
      return false
    }
  }
  return true
}

function isChosenTimeCorrect (careerItem) {
  let startTimeString = careerItem.start_time.day + '/' + careerItem.start_time.month + '/' + careerItem.start_time.year
  let endTimeString = careerItem.end_time.day + '/' + careerItem.end_time.month + '/' + careerItem.end_time.year
  let unixStartTime = moment(startTimeString, 'DD/MM/YYYY').valueOf()
  let unixEndTime = moment(endTimeString, 'DD/MM/YYYY').valueOf()
  console.log('unix time', unixStartTime, unixEndTime)
  if (unixStartTime < unixEndTime) {
    return true
  }
  return false
}

function setLoading (loadingType, value) {
  let loading = {}
  loading[loadingType] = value
  return {
    type: IS_LOADING,
    loading: loading
  }
}

function setSuccessMessage (msg) {
  return {
    type: IS_SUCCESS,
    successMessage: msg
  }
}

function setErrorMessage (err) {
  return {
    type: IS_ERROR,
    error: err
  }
}
// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [UPDATE_LIST]: (state, action) => {
    return {
      ...state,
      ...action.careerList
    }
  },
  [INIT_LIST]: (state, action) => {
    return {
      ...state,
      careerList: action.careerList
    }
  },
  [SET_BACKUP_LIST]: (state, action) => {
    return {
      ...state,
      backupList: action.backupList
    }
  },
  [IS_LOADING]: (state, action) => {
    return {
      ...state,
      ...action.loading,
      successMessage: '',
      errorMessage: ''
    }
  },
  [IS_ERROR]: (state, action) => {
    return {
      ...state,
      isSaveLoading: false,
      isCancelLoading: false,
      isError: true,
      errorMessage: action.error
    }
  },
  [IS_SUCCESS]: (state, action) => {
    return {
      ...state,
      isSaveLoading: false,
      isCancelLoading: false,
      isSuccess: true,
      successMessage: action.successMessage
    }
  },
  [CLEAR_INPUT]: (state, action) => {
    return {
      ...state,
      isSaveLoading: false,
      isCancelLoading: false,
      isError: true,
      isSuccess: false,
      isChangeSuccess: false,
      errorMessage: 'No info is updated',
      successMessage: '',
      careerList: initialCareerList
    }
  }
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  isSaveLoading: false,
  isCancelLoading: false,
  isDetailsLoading: false,
  isError: false,
  isSuccess: false,
  isChangeSuccess: false,
  errorMessage: '',
  successMessage: '',
  careerList: [],
  backupList: []
}

export default function eduExpReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
