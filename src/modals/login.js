import React from 'react'
import { Gateway } from 'react-gateway'
import { Dialog, FlatButton } from 'material-ui'

const customContentStyle = {
  width: '75%',
  maxWidth: 'none'
}
var loginModal = function(options, onConfirm, onCancel) {
  options = options || {};
  console.log("loginmodal");
  var actions = [
    <FlatButton
      label="Cancel"
      primary={true}
      onClick={onCancel}
    />
  ];
  return (
    <Gateway into="confirm-modal">
      <Dialog
        title={'Login'}
        actions={actions}
        modal={true}
        contentStyle={customContentStyle}
        open={true}
      >
        <div className="form-group">
          <label for="username" className="sr-only">User Name</label>
          <input type="text" name="username" className="form-control" placeholder="User Name" required autoFocus />
        </div>
        <div className="form-group">
          <label for="password" className="sr-only">Password</label>
          <input type="password" name="password" className="form-control" placeholder="Password" required />
        </div>
        <div className="form-group">
          <button className="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
        </div>
      </Dialog>
    </Gateway>
  )
}

export default loginModal;
