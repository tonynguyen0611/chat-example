import { connectWithLifecycle } from 'react-lifecycle-component'
import { getPeopleResultList } from '../modules/people_result'
import PeopleResult from '../components/people_result'

const mapDispatchToProps = {
  getPeopleResultList
}

const mapStateToProps = (state) => {
  return state.peopleResult
}

export default connectWithLifecycle(
  mapStateToProps, mapDispatchToProps
)(PeopleResult)
