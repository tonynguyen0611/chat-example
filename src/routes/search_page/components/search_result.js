import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import tempAva from './../../../assets/static/ava.jpg'
import CollectionsList from '../../../components/collections_list'

const loadingImage = <img className='loading-img' src='https://cdn.zenquiz.net/static/Assets/loading-animation.gif' alt='Loading...' />

class SearchResult extends React.Component {

  componentWillMount () {
    let searchKey = this.props.match.params.searchKey
    this.props.getSearchResultList(searchKey)
  }

  render () {
    let _this = this
    let peopleList = this.props.peopleList || []
    let collectionList = this.props.collectionList || []
    return (
      <div className='search-container search-result'>
        <div className='search-part people-part disp-small'>
          <div className='search-title'>
            People
          </div>
          <div className='search-content'>
            {
              _this.props.isLoadingPeopleList ? loadingImage
              : <div>
                {
                  (peopleList.length === 0) ? 'No result'
                  : <div>
                    {
                      peopleList.map((item, index) => {
                        return (
                          <Link key={item.id} to={'/user-view/' + item.id + '/collection'}>
                            <div key={item.id} className='people-item'>
                              <div className='people-cover-container'>
                                {
                                  item.profile_cover && item.profile_cover.url &&
                                  <img src={item.profile_cover.url} className='people-cover-img' />
                                }
                              </div>
                              <div className='people-ava-container'>
                                <img src={item.profile_avatar.url ? item.profile_avatar.url : tempAva} className='people-ava-img' />
                              </div>
                              <div className='people-info'>
                                <div className='people-info-item'>{item.full_name}</div>
                                <div className='people-info-item'><b>{item.occupation[0] ? item.occupation[0] : ''}</b></div>
                              </div>
                            </div>
                          </Link>
                        )
                      })
                    }
                    <div className='see-all'>
                      <Link to={'/search/people/' + this.props.match.params.searchKey}>
                        See all
                      </Link>
                    </div>
                  </div>
                }
              </div>
            }
          </div>
        </div>
        <div className='search-part collection-part disp-small collection-small'>
          <div className='search-title'>
            Collections
          </div>
          <div className='search-content'>
            {
              _this.props.isLoadingCollectionList ? loadingImage
              : <div>
                <CollectionsList
                  collectionsList={collectionList} numOfCol={4} />
                <div className='see-all'>
                  <Link to={'/search/collection/' + this.props.match.params.searchKey}>
                    See all
                  </Link>
                </div>
              </div>
            }
          </div>
        </div>
      </div>
    )
  }
}

SearchResult.propTypes = {
  peopleList: PropTypes.array.isRequired,
  collectionList: PropTypes.array.isRequired,
  match: PropTypes.object.isRequired,
  getSearchResultList: PropTypes.func
}

export default SearchResult
