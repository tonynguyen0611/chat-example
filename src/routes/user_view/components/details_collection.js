import React from 'react'
import { Link } from 'react-router-dom'
import './details_collection.scss'
const MediaObject = ({ imageUrl, isActive, index, onSelectItem }) => {
  return (
    <div className={isActive ? 'col-md-3 item-active' : 'col-md-3'} onClick={() => onSelectItem(index)}>
      <img className='img-responsive' src={imageUrl} />
    </div>
  )
}

const DetailsCollection = (
  {
    collection,
    userInfo,
    onNext, onPrevious, onSelectItem, mediaItems, indexMediaItem }) => {
  let items = []
  let index = (indexMediaItem + 4 >= mediaItems.length) ? mediaItems.length - 4 : indexMediaItem
  index = (index < 0) ? 0 : index
  let n = (index + 4 >= mediaItems.length) ? mediaItems.length : index + 4
  for (let i = index; i < n; i++) {
    items.push(<MediaObject
      index={i}
      onSelectItem={onSelectItem}
      key={i}
      isActive={mediaItems[i].id === mediaItems[indexMediaItem].id}
      imageUrl={mediaItems[i].images[2].source} />)
  }
  return (
  <div className='DetailsCollection'>
    <div className="arrow-left" onClick={onPrevious}>
      <i className="fa fa-long-arrow-left" aria-hidden="true"></i>
    </div>
    <div className="arrow-right" onClick={onNext}>
      <i className="fa fa-long-arrow-right" aria-hidden="true"></i>
    </div>
    <div className='row'>
      <div className='right-panel'>
        <div className="col-md-3">
          <h5>{collection.title}</h5>
          <p>{mediaItems[indexMediaItem].description}</p>
        </div>
        <div className='col-md-6 image-preview'>
          <img className='img-responsive' src={mediaItems[indexMediaItem].images[0].source} />
        </div>
        <div className='col-md-3'>
          <div className='row'>
            <div className='col-md-4'>
              <Link to={'/user-view/' + userInfo.id + '/collection'} >
                <img className='img-circle img-responsive' src={userInfo.profile_avatar.url} />
              </Link>
            </div>
            <div className='col-md-4'>
              <h5>{userInfo.full_name}</h5>
            </div>
            <div className='col-md-4 small-btn-container'>
              <button className='small-btn btn-green'>Following</button>
            </div>
          </div>
          <div className='row media-items'>
            {items}
          </div>
        </div>
      </div>
    </div>
  </div>
)}

export default DetailsCollection
