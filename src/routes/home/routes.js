import React from 'react'
import { injectReducer } from '../../store/reducers'

const HomeContainer = (store)=> {
      const Home = require('./containers/home').default
      const reducer = require('./modules/home').default
      /*  Add the reducer to the store  */
      injectReducer(store, { key: 'home', reducer })
      return Home;
}

export const HomeRoute = (store) => ({
  path: '/',
  component: HomeContainer(store)
})
