import { connectWithLifecycle } from 'react-lifecycle-component'

import AccountSettings from '../components/account_settings'

const mapDispatchToProps = {
}

const mapStateToProps = (state) => {
  return state.accountSettings
}

export default connectWithLifecycle(
  mapStateToProps, mapDispatchToProps
)(AccountSettings)
