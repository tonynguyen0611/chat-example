import React from 'react'
import { injectReducer } from '../../store/reducers'

const NetworkContainer = (store) => {
  const reducer = require('./modules/network').default
  const Network = require('./containers/network').default
  injectReducer(store, { key: 'network', reducer })
  return Network
}

const AboutContainer = (store) => {
  const reducer = require('./modules/about').default
  const About = require('./containers/about').default
  injectReducer(store, { key: 'about', reducer })
  return About
}

const CollectionContainer = (store) => {
  const reducer = require('./modules/collection').default
  const Collection = require('./containers/collection').default
  injectReducer(store, { key: 'collection', reducer })
  return Collection
}

const HeaderContainer = (store) => {
  const Header = require('./containers/header').default
  return Header
}

export const HeaderRoute = (store) => ({
  path: '/',
  component: HeaderContainer(store)
})

export const CollectionRoute = (store) => ({
  path: '/user-view/:userId/collection',
  component: CollectionContainer(store)
})

export const AboutRoute = (store) => ({
  path: '/user-view/:userId/about',
  component: AboutContainer(store)
})

export const NetworkRoute = (store) => ({
  path: '/user-view/:userId/network',
  component: NetworkContainer(store)
})

