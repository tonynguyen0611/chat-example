import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import AvatarEditor from 'react-avatar-editor'
import $ from 'jquery'
import tempAva from './../../../assets/static/ava.jpg'
import './header.scss'

class HeaderUserView extends React.Component {
  setEditorRef = (editor) => {
    this.editor = editor
  }
  handleSaveChangeCoverPhoto = () => {
    if (this.editor) {
      const canvasScaled = this.editor.getImageScaledToCanvas()
      let ctx = canvasScaled.toDataURL()
      let bufImage = new Buffer(ctx.replace(/^data:image\/\w+;base64,/, ''), 'base64')
      this.props.handleSaveChangeCoverPhoto(bufImage)
    }
  }
  componentWillMount () {
  }
  componentWillReceiveProps (nextProps) {
  }
  render () {
    let pathSplit = location.pathname.split('/')
    let chosenTab = pathSplit[pathSplit.length - 1]
    return (
      <div>
        {
          this.props.isLoading &&
          <img src='https://cdn.zenquiz.net/static/Assets/loading-animation.gif' alt='Loading' />
        }
        {
          !this.props.isLoading && this.props.userInfo.id &&
          <div className='user-view-header'>
            <div className='user-cover'>
              {!this.props.isEditingCoverPhoto &&
                <div>
                  {
                    this.props.userInfo.profile_cover && this.props.userInfo.profile_cover.url &&
                    <img className='cover-photo'
                      src={this.props.userInfo.profile_cover.url} />
                  }
                </div>
              }
              {this.props.isEditingCoverPhoto &&
                <AvatarEditor
                  ref={this.setEditorRef}
                  image={this.props.coverPhoto}
                  width={$('.user-cover').width()}
                  height={350}
                  border={1}
                  color={[255, 255, 255, 0.6]} // RGBA
                  scale={1}
                  rotate={0}
                />
              }
              {this.props.isOwner &&
                <div className='btn-camera'>
                  <i className='fa fa-camera' aria-hidden='true' />
                  <input name='file' type='file' accept='image/*' onChange={(e) => this.props.handelUploadCoverPhoto(e)}
                    className='input-image' />
                </div>
              }
              {!this.props.isOwner &&
                <div className='cover-btn-container'>
                  <button
                    className='cover-btn btn-gray'
                    onClick={() => this.props.handleSelectedSenderMessages(this.props.userInfo)}>
                    <b> Message </b>
                  </button>
                  {
                    this.props.isFollowing &&
                    <button
                      className='cover-btn btn-green'
                      onClick={() => this.props.handleNetworkAction(this.props.userInfo.id, 'unfollow')}>
                      <b> Following </b>
                    </button>
                  }
                  {
                    !this.props.isFollowing &&
                    <button
                      className='cover-btn btn-blue'
                      onClick={() => this.props.handleNetworkAction(this.props.userInfo.id, 'follow')}>
                      <b> Follow </b>
                    </button>
                  }
                </div>
              }
              {this.props.isOwner && this.props.isEditingCoverPhoto &&
                <div className='cover-btn-container'>
                  {!this.props.isUploading &&
                    <button className='cover-btn btn-gray' onClick={this.props.handleCancelChangeCoverPhoto}>
                      <b> Cancel </b>
                    </button>
                  }
                  {this.props.isUploading ? (
                    <img src='https://cdn.zenquiz.net/static/Assets/loading-animation.gif' />
                  ) : (
                    <button className='cover-btn btn-green' onClick={this.handleSaveChangeCoverPhoto}>
                      <b> Save Changes </b>
                    </button>
                  )}
                </div>
              }
            </div>
            <div className='user-menu'>
              <div className='user-short'>
                <Link className='link-to' to={`/user-view/${this.props.userInfo.id}/collection`}>
                  <span className='user-ava'>
                    <img src={this.props.userInfo.profile_avatar.url || tempAva} alt='profile_picture' />
                  </span>
                  <span className='user-name'> <b> {this.props.userInfo.full_name} </b> </span>
                </Link>
              </div>
              <div className='menu-list'>
                <Link className='link-to' to={`/user-view/${this.props.userInfo.id}/about`}>
                  <span className={'menu-item ' + (chosenTab === 'about' ? 'chosen' : '')}> About</span>
                </Link>
                <Link className='link-to' to={`/user-view/${this.props.userInfo.id}/network/search`}>
                  <span className={'menu-item ' + (chosenTab === 'search' ? 'chosen' : '')}> Network</span>
                </Link>
                <Link className='link-to' to={`/user-view/${this.props.userInfo.id}/network/followers`}>
                  <span className={'menu-item ' + (chosenTab === 'followers' ? 'chosen' : '')}> Followers ({this.props.followersCount})</span>
                </Link>
                <Link className='link-to' to={`/user-view/${this.props.userInfo.id}/network/following`}>
                  <span className={'menu-item ' + (chosenTab === 'following' ? 'chosen' : '')}> Following ({this.props.followingCount})</span>
                </Link>
              </div>
            </div>
          </div>
        }
      </div>
    )
  }
}

HeaderUserView.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  isOwner: PropTypes.bool.isRequired,
  isUploading: PropTypes.bool.isRequired,
  isFollowing: PropTypes.bool.isRequired,
  handleNetworkAction: PropTypes.func.isRequired,
  coverPhoto: PropTypes.string,
  isEditingCoverPhoto: PropTypes.bool.isRequired,
  userInfo: PropTypes.object,
  handelUploadCoverPhoto: PropTypes.func.isRequired,
  handleSaveChangeCoverPhoto: PropTypes.func.isRequired,
  handleCancelChangeCoverPhoto: PropTypes.func.isRequired,
  followersCount: PropTypes.number,
  handleSelectedSenderMessages: PropTypes.func
}
export default HeaderUserView
