import React from 'react'
import PropTypes from 'prop-types'
import CollectionsList from '../../../components/collections_list'
import './collection_result.scss'

const loadingImage = <img src='https://cdn.zenquiz.net/static/Assets/loading-animation.gif' alt='Loading...' />

class CollectionResult extends React.Component {

  componentWillMount () {
    let searchKey = this.props.match.params.searchKey
    this.props.getCollectionResultList(searchKey)
  }
  render () {
    let _this = this
    return (
      <div className='explore-container'>
        {
          _this.props.isLoadingCollectionList ? loadingImage
          : <CollectionsList collectionsList={_this.props.collectionList} numOfCol={4} />
        }
      </div>
    )
  }
}

CollectionResult.propTypes = {
  match: PropTypes.object.isRequired,
  getCollectionResultList: PropTypes.func
}

export default CollectionResult
