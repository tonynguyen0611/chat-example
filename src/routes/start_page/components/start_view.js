import React from 'react'
import { Link } from 'react-router-dom'
import './start_view.scss'

const StartView = () => (
  <div className='start-view'>
    <div className='input-group'>
      <Link to='/start' className='link'>
        <button className='btn btn-block btn-start btn-start-blue'> Get Started</button>
      </Link>
      <Link to='/start/login' className='link'>
        <button className='btn btn-block btn-start btn-start-gray'> Log In</button>
      </Link>
    </div>
  </div>
)

export default StartView
