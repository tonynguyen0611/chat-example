import { connectWithLifecycle } from 'react-lifecycle-component'
import ChangePassword from '../components/change_password'
import { handleInputValueChange, submit, initialize } from '../modules/change_password'

const mapDispatchToProps = {
  handleInputValueChange,
  submit,
  initialize
}

const mapStateToProps = (state) => {
  return state.changePassword
}

export default connectWithLifecycle(
  mapStateToProps, mapDispatchToProps
)(ChangePassword)
