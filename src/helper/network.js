import axios from 'axios'

export function sendRequestNetworkList (options, requestType) {
  return new Promise((resolve, reject) => {
    let url = ENVIRONMENT.API_DOMAIN + '/users/' + options.userId + '/' + requestType +
              '?conditions=' + JSON.stringify(options.conditions || {}) +
              '&offset=' + (options.offset || 0) +
              '&limit=' + (options.limit || 20) +
              '&sort=' + (options.sort || '')
    axios.get(encodeURI(url), { withCredentials: true })
    .then(res => resolve(res.data))
    .catch(err => reject(err.response.data.error))
  })
}

export function sendRequestNetworkCount (userId, requestType) {
  return new Promise((resolve, reject) => {
    let url = ENVIRONMENT.API_DOMAIN + '/users/' + userId + '/' + requestType + '/count'
    axios.get(url, { withCredentials: true })
    .then(res => resolve(res.data))
    .catch(err => reject(err.response.data.error))
  })
}

export function sendRequestNetworkAction (userId, requestType) {
  return new Promise((resolve, reject) => {
    let url = ENVIRONMENT.API_DOMAIN + '/following'
    if (requestType === 'unfollow') {
      url += '/' + userId
      axios.delete(url, { withCredentials: true })
      .then(res => resolve(res.data))
      .catch(err => reject(err.response.data.error))
    } else {
      axios.post(url, {
        'target_user_id': userId // id of the user we want to follow
      }, { withCredentials: true })
      .then(res => resolve(res.data))
      .catch(err => reject(err.response.data.error))
    }
  })
}
