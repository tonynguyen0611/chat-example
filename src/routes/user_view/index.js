import React from 'react'
import { Route } from 'react-router-dom'
import { HeaderRoute, CollectionRoute, AboutRoute, NetworkRoute } from './routes'
import './index.scss'

export const Header = ({ store, match }) => {
  const userId = match.params.userId
  const routes = [
    HeaderRoute(store),
    CollectionRoute(store),
    AboutRoute(store),
    NetworkRoute(store)
  ]
  return (
    <div className='col-lg-12 col-md-12 col-sm-12 col-xs-12 user-view'>
      {routes.map((route, i) => (
        <RouteWithSubRoutes key={i} {...route} userId={userId} />
      ))}
    </div>
  )
}

const RouteWithSubRoutes = (route) => {
  return (
    <Route path={route.path} render={props => (
      <route.component {...props} routes={route.routes} userId={route.userId} />
    )} />
  )
}

export default Header
