import { connectWithLifecycle } from 'react-lifecycle-component'
import LoginForm from '../components/login_form'
import {
  login,
  handleInputValueChange,
  signinWithFb,
  signinWithGoogle,
  setModalState,
  registerSocialEmail,
  handleOnKeyChange
 } from '../modules/login'

const mapDispatchToProps = {
  login,
  handleInputValueChange,
  signinWithFb,
  signinWithGoogle,
  setModalState,
  registerSocialEmail,
  handleOnKeyChange
}

const mapStateToProps = (state) => {
  return state.currentUser
}

export default connectWithLifecycle(
  mapStateToProps, mapDispatchToProps
)(LoginForm)
