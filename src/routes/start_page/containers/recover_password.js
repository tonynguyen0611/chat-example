import { connectWithLifecycle } from 'react-lifecycle-component'
import RecoverPassword from '../components/recover_password'
import { handleEmailChange, submit } from '../modules/recover_password'

const mapDispatchToProps = {
  handleEmailChange,
  submit
}

const mapStateToProps = (state) => {
  return state.recoverPassword
}

export default connectWithLifecycle(
  mapStateToProps, mapDispatchToProps
)(RecoverPassword)
