import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import moment from 'moment'
import InfiniteScroll from 'react-infinite-scroller'
import DetailsCollectionModal from '../../../components/preview_album'
import './HomeView.scss'

const loadingImage = <img src='https://cdn.zenquiz.net/static/Assets/loading-animation.gif' alt='Loading...' />

const toHowLongAgo = (input) => {
  let date = moment(input)
  return date.fromNow()
}

const NewsFeedItem = ({ data, getCollection }) => {
  return (
    <div>
      <div className='side-bar-item row'>
        <div className='col-md-2'>
          <Link to={'user-view/' + data.user.id + '/collection'} >
            <img
              className='img-circle img-responsive'
              src={data.user.profile_avatar.url ? data.user.profile_avatar.url
              : 'http://ocotur-dev.s3.amazonaws.com/static/assets/ava.jpg'} />
          </Link>
        </div>
        <div className='col-md-10'>
          <Link to={'user-view/' + data.user.id + '/collection'} >
            <span className='user-name'> {data.user.full_name} </span>
          </Link>
          {data.type === 'ALBUM_CREATE' &&
            <span className='user-status'>uploaded a new collection </span>
          }
          {data.type === 'ALBUM_PIECE_PHOTO_CREATE' && data.metadata && data.metadata.media_item_count > 1 &&
            <span className='user-status'>uploaded {data.metadata.media_item_count} new pictures to the collection </span>
          }
          {data.type === 'ALBUM_PIECE_PHOTO_CREATE' && data.metadata && data.metadata.media_item_count === 1 &&
            <span className='user-status'>uploaded a new picture to the collection </span>
          }
          {data.type === 'CAREER_HISTORY_CREATE' &&
            <span className='user-status'>started a new job </span>
          }
          {data.type !== 'CAREER_HISTORY_CREATE' &&
            <span className='user-collection'
              onClick={() => getCollection(data.album, data.album.preview_media_item)}
            >{data.album ? data.album.title : ''}</span>
          }
          <p>{toHowLongAgo(data.time_action)}</p>
          {data.type === 'CAREER_HISTORY_CREATE' &&
            <div className='career-desc'>
              <span> {data.career_history.title || ''} at {data.career_history.place || ''}</span>
            </div>
          }
        </div>
        <div className='col-md-12'>
          {data.album && data.album.preview_media_item && data.album.preview_media_item.images.length > 0 && data.type === 'ALBUM_CREATE' &&
            <div className='col-md-12' onClick={() => getCollection(data.album, data.album.preview_media_item)} >
              <img className='img-responsive' src={data.album.preview_media_item.images[0].source} />
            </div>
          }
          {data.album && data.album.preview_media_item && data.media_item && data.type === 'ALBUM_PIECE_PHOTO_CREATE' && data.metadata &&
            <div className='col-md-12'>
              {data.metadata.display_media_items[0] &&
                <div className='col-md-4' onClick={() => getCollection(data.album, data.metadata.display_media_items[0])}>
                  <img className='img-responsive' src={data.metadata.display_media_items[0].images[1].source || data.metadata.display_media_items[0].images[0].source} />
                </div>
              }
              {data.metadata.display_media_items[1] &&
                <div className='col-md-4' onClick={() => getCollection(data.album, data.metadata.display_media_items[1])}>
                  <img className='img-responsive' src={data.metadata.display_media_items[1].images[1].source || data.metadata.display_media_items[1].images[0].source} />
                </div>
              }
              {data.metadata.display_media_items[2] &&
                <div className='col-md-4' >
                  <img onClick={() => getCollection(data.album, data.metadata.display_media_items[2])} className='img-responsive' src={data.metadata.display_media_items[2].images[1].source || data.metadata.display_media_items[2].images[0].source} />
                  <div className='has-more' onClick={() => getCollection(data.album, data.album.preview_media_item)}>
                    <i className="fa fa-long-arrow-right" aria-hidden="true"></i>
                  </div>
                </div>
              }
            </div>
          }
        </div>
      </div>
    </div>
  )
}

const noDataInNewsFeed = (
  <div className='no-data'>
    <div className='text'>Nothing to show yet!</div>
    <div className='text'>Let's get started...</div>
    <div className='row btn-connect-container'>
      <span className='col-md-4'>
        <Link to='/explore'>
          <button className='btn-start btn-start-purple'>
            Explore
          </button>
        </Link>
      </span>
      <span className='col-md-4'>
        <Link to='/'>
          <button className='btn-start btn-start-blue'>
            Connect
          </button>
        </Link>
      </span>
      <span className='col-md-4'>
        <Link to='/collections/upload'>
          <button className='btn-start btn-start-green'>
            Upload
          </button>
        </Link>
      </span>
    </div>
  </div>
)
class HomeView extends React.Component {
  componentWillMount () {
    this.props.getNewsFeed()
  }
  
  render () {
    let _this = this
    const modalDetailCollection = DetailsCollectionModal({
      collection: _this.props.collectionActive,
      mediaItem: _this.props.mediaItem,
      isOpen: _this.props.isShowModal
    }, _this.props.setModalState
    )
    let newsFeedData = this.props.newsFeedData
    return (
      <div className='row HomeView'>
        {modalDetailCollection}
        <div className='col-md-3 side-bar-left'>
          <div className='menu side-bar-item'>
            <ul>
              <Link to='/'>
                <li>Home</li>
              </Link>
              <Link to='/profile/messages'>
                <li>Messages</li>
              </Link>
              <Link to='/collections/upload'>
                <li>Upload</li>
              </Link>
              <Link to='/profile/info'>
                <li>Settings</li>
              </Link>
              <a href='#' onClick={this.props.sendLogOutRequest}>
                <li>Log out</li>
              </a>
            </ul>
          </div>
          <div className='side-bar-item'>
            <Link to='/explore' className='btn'>Explore</Link>
          </div>

        </div>
        { !this.props.isNewsFeedLoading &&
          <div className='col-md-6 side-bar-right'>
            {
              newsFeedData && (newsFeedData.length > 0) &&
              <InfiniteScroll
                pageStart={0}
                loadMore={this.props.getNewsFeed}
                hasMore={this.props.hasMore}
                loader={null}>
                {
                  newsFeedData.map((value, i) => (
                    <NewsFeedItem key={i} data={value} getCollection={this.props.getCollectionDetail} />
                  ))
                }
              </InfiniteScroll>
            }
            {
            !newsFeedData || (newsFeedData.length === 0) &&
              noDataInNewsFeed
            }
          </div>
        }
        { this.props.isNewsFeedLoading && loadingImage }
      </div>
    )
  }
}

HomeView.propTypes = {
  isNewsFeedLoading: PropTypes.bool.isRequired,
  getNewsFeed: PropTypes.func,
  newsFeedData: PropTypes.array,
  hasMore: PropTypes.bool,
  sendLogOutRequest: PropTypes.func,
  getCollectionDetail: PropTypes.func.isRequired,
  setModalState: PropTypes.func.isRequired
}
export default HomeView
