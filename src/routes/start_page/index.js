import React from 'react'
import {
  Route,
  Switch
} from 'react-router-dom'

import { StartViewRoute, LoginRoute, RegisterRoute, RecoverRoute } from './routes'

export const StartPage = ({ store }) => {
  const routes = [
    LoginRoute(store),
    StartViewRoute(store),
    RegisterRoute(store),
    RecoverRoute(store)
  ]
  return (
    <div>
      <Switch>
        {routes.map((route, i) => (
          <Route key={i} path={route.path} component={route.component} />
        ))}
      </Switch>
    </div>
  )
}

export default StartPage
