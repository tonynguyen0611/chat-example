import { validate } from './../../../utils/validate'
import axios from 'axios'
import { replace } from 'react-router-redux'

// ------------------------------------
// Constants
// ------------------------------------
const RECOVER_LOADING = 'RECOVER_LOADING'
const RECOVER_ERROR = 'RECOVER_ERROR'
const RECOVER_SUCCESS = 'RECOVER_SUCCESS'
const INPUT_VALUE = 'INPUT_VALUE'

// ------------------------------------
// Actions
// ------------------------------------
export function handleEmailChange (e) {
  return {
    type: INPUT_VALUE,
    email: e.target.value
  }
}

export function submit () {
  return (dispatch, getState) => {
    let moduleState = getState().recoverPassword
    let validationMsg = validate('email', moduleState.email) || ''

    if (validationMsg !== 'NO_ERROR') {
      dispatch(setErrorMessage(validationMsg))
    } else {
      dispatch(sendRecoverRequest())
    }
  }
}

function sendRecoverRequest () {
  return (dispatch, getState) => {
    console.log('send request')
    let moduleState = getState().recoverPassword

    dispatch(setLoading(true))

    axios.post(ENVIRONMENT.API_DOMAIN + '/users/reset_password',
      {
        'email': moduleState.email
      }, { withCredentials: true })
    .then(res => dispatch(recoverSuccess()))
    .catch(err => dispatch(setErrorMessage(err.response.data.error)))
  }
}

function setErrorMessage (errMsg) {
  console.log('error message', errMsg)
  return {
    type: RECOVER_ERROR,
    errorMessage: errMsg
  }
}

function setLoading (value) {
  return {
    type: RECOVER_LOADING,
    isLoading: value
  }
}

function recoverSuccess () {
  return {
    type: RECOVER_SUCCESS
  }
}

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [INPUT_VALUE]: (state, action) => {
    return {
      ...state,
      email: action.email
    }
  },
  [RECOVER_ERROR]: (state, action) => {
    return {
      ...state,
      isRecoverError: true,
      isLoading: false,
      errorMessage: action.errorMessage
    }
  },
  [RECOVER_SUCCESS]: (state, action) => {
    return {
      ...state,
      isRecoverSuccess: true,
      isLoading: false
    }
  }
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  isLoading: false,
  isRecoverError: false,
  isRecoverSuccess: false,
  errorMessage: '',
  email: ''
}

export default function recoverPasswordReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}

