import React from 'react'
import {
  BrowserRouter as Router,
  Route,
  Link,
  Switch
} from 'react-router-dom'

import {HomeRoute} from './routes'


export const Home = ({store}) => {
    
    const routes= [
      HomeRoute(store)
      ]
  return (
      <div>
             {routes.map((route, i) => (
              <RouteWithSubRoutes key={i} {...route}/>
            ))}
      </div>
)}

const RouteWithSubRoutes = (route) => {
  return (
  <Route path={route.path} render={props => (
    <route.component {...props} routes={route.routes}/>
  )}/>
)}

export default Home
