import update from 'immutability-helper'
import { upLoadImageThumbnail } from '../../../helper/image'
import { resetState } from '../../home/modules/home'
import { replace } from 'react-router-redux'
import CollectionService from '../../../api/collection'
import UserService from '../../../api/user'
import async from 'async'
// ------------------------------------
// Constants
// ------------------------------------
const RECEIVE_ERROR = 'RECEIVE_ERROR'
const ADD_IMAGE_DATA_TO_COLLECTION = 'ADD_IMAGE_TO_COLLECTION'
const UPDATE_IMAGE_DATA = 'UPDATE_IMAGE_DATA'
const UPDATE_LOADING_STATUS = 'UPDATE_LOADING_STATUS'
const IMAGE_DESC_CHANGE = 'IMAGE_DESC_CHANGE'
const COLLECTION_TITLE_CHANGE = 'COLLECTION_TITLE_CHANGE'
const CREATE_MEDIA_ITEM_SUCCESS = 'CREATE_MEDIA_ITEM_SUCCESS'
const GET_USER_SUCCESS = 'CREATE_COLLECTION_GET_USER_SUCCESS'
const RESET_STATE = 'CREATE_COLLECTION_RESET_STATE'
const REMOVE_ITEM = 'REMOVE_ITEM'
const START_ADD_COLLABORATORS = 'START_ADD_COLLABORATORS'
const ADD_COLLABORATORS = 'ADD_COLLABORATORS'
const GET_COLLECTION_SUCCESS = 'CREATA_GET_COLLECTION_SUCCESS'
const UPDATE_GROUP_UPLOAD = 'UPDATE_GROUP_UPLOAD'
// ------------------------------------
// Actions
// ------------------------------------
export function getCollection (collectionId) {
  return (dispatch, getState) => {
    dispatch(resetCurrentState())
    CollectionService.getCollection(collectionId)
      .then(res => {
        res.data.media_items.map((mediaItem, i) => {
          dispatch(addImageDataToCollection(i, {
            index: i,
            isUploading: false,
            description: mediaItem.description,
            oldDescription: mediaItem.description,
            album_id: mediaItem.album_id,
            type: mediaItem.type,
            images: mediaItem.images,
            id: mediaItem.id
          }))
        })
        dispatch(getCollectionSuccess(res.data))
      })
      .catch(err => dispatch(receiveError(err)))
  }
}
function getCollectionSuccess (data) {
  return {
    type: GET_COLLECTION_SUCCESS,
    payload: data
  }
}
export function removeItem (index) {
  return {
    type: REMOVE_ITEM,
    index: index
  }
}
export function publishCollection () {
  return (dispatch, getState) => {
    dispatch(updateLoadingStatus())
    let moduleState = getState().createCollection
    dispatch(updateLoadingStatus())
    if (moduleState.collection.id) {
      if (moduleState.title !== moduleState.collection.title) {
        updateAlbum(moduleState.collection.id, moduleState.title)
      }
      dispatch(createMediaItems(moduleState.collection))
    } else {
      console.log('create album')
      dispatch(createNewAlbum(getState().createCollection.title))
    }
  }
}
function updateAlbum (collectionId, newTitle) {
  return new Promise(function (resolve, reject) {
    CollectionService.updateCollection(collectionId, { title: newTitle })
    .then(res => resolve(res.data))
    .catch(err => reject(err))
  })
}
function updateGroupUpload (isGroupUpload) {
  return {
    type: UPDATE_GROUP_UPLOAD,
    payload: isGroupUpload
  }
}
export function handelUploadImages (event) {
  return (dispatch, getState) => {
    let index = getState().createCollection.imageList.length
    let countFile = event.target.files.length
    dispatch(updateGroupUpload(true))
    for (let i = 0; i < countFile; i++) {
      let currentIndex = index + i
      dispatch(addImageDataToCollection(currentIndex))
      upLoadImageThumbnail(event.target.files[i])
        .then((arrImages) => {
          dispatch(updateImageData(arrImages, currentIndex))
          if (i === countFile - 1) {
            dispatch(updateGroupUpload(false))
          }
        })
        .catch((err) => {
          dispatch(receiveError(err))
        })
    }
  }
}
export function getUsers (query, conditions) {
  return (dispatch, getState) => {
    UserService.getUsers(query, conditions)
      .then(res => {
        dispatch(getUsersSuccess(res.data))
      })
      .catch(err => dispatch(receiveError(err)))
  }
}
export function handleImageDescChange (index, event) {
  return {
    type: IMAGE_DESC_CHANGE,
    imageData: {
      index: index,
      description: event.target.value
    }
  }
}
export function handleCollectionTitleChange (event) {
  return {
    type: COLLECTION_TITLE_CHANGE,
    title: event.target.value
  }
}
export function handleStartAddCollaborators (index) {
  return {
    type: START_ADD_COLLABORATORS,
    payload: index
  }
}
export function handleAddCollaborators (index, user) {
  let collaborator = {
    user_id: user.id,
    role: 'collaborator',
    metadata: {},
    full_name: user.full_name
  }
  return {
    type: ADD_COLLABORATORS,
    payload: collaborator,
    index: index
  }
}
function getUsersSuccess (userData) {
  return {
    type: GET_USER_SUCCESS,
    data: userData
  }
}
function createNewAlbum (title) {
  return (dispatch) => {
    let data = {
      title: title,
      metadata: {}
    }
    CollectionService.createCollection(data)
    .then(res => {
      dispatch(createMediaItems(res.data))
    })
    .catch(err => dispatch(receiveError(err)))
  }
}
function deleteMediaItem (imageData) {
  return new Promise(function (resolve, reject) {
    console.log('deleteMediaItem', imageData)
    CollectionService.deleteMediaItem(imageData.id)
    .then(res => resolve(res.data))
    .catch(err => reject(err))
  })
}
function updateMediaItem (imageData, albumId) {
  return new Promise(function (resolve, reject) {
    let data = {
      ...imageData,
      album_id: albumId
    }
    CollectionService.updateMediaItem(imageData.id, data)
    .then(res => resolve(res.data))
    .catch(err => reject(err))
  })
}
function createMediaItems (album) {
  return (dispatch, getState) => {
    let imageList = getState().createCollection.imageList
    let removeItemsList = getState().createCollection.removeItemsList
    async.eachLimit(imageList, 1, (item, cb) => {
      if (item.id === '') {
        createMediaItem(item, album.id)
        .then(() => {
          cb()
        })
        .catch(err => {
          cb(err)
        })
      } else {
        if (item.description !== item.oldDescription) {
          updateMediaItem(item, album.id)
          .then(() => {
            cb()
          })
          .catch(err => {
            cb(err)
          })
        } else {
          cb()
        }
      }
    }, function (err) {
      if (err) {
        dispatch(receiveError(err))
      } else {
        async.eachLimit(removeItemsList, 1, (item, cb) => {
          if (item.id !== '') {
            deleteMediaItem(item, album.id)
            .then(() => {
              cb()
            })
            .catch(err => {
              cb(err)
            })
          }
        }, err => {
          if (err) {
            dispatch(receiveError(err))
          } else {
            dispatch(replace('/user-view/' + album.user_id + '/collection'))
          }
          dispatch(resetState())
          dispatch(resetCurrentState())
        })
      }
    })
  }
}
function resetCurrentState () {
  return {
    type: RESET_STATE
  }
}
function createMediaItem (imageData, albumId) {
  return new Promise(function (resolve, reject) {
    let data = {
      ...imageData,
      album_id: albumId
    }
    CollectionService.createMediaItem(data)
    .then(res => resolve(res.data))
    .catch(err => reject(err))
  })
}
function updateLoadingStatus () {
  return {
    type: UPDATE_LOADING_STATUS
  }
}
function addImageDataToCollection (index, data) {
  let imageData = data ? data : {
    index:index,
    isUploading: true,
    description: '',
    album_id: '',
    type: 'ALBUM_PIECE_PHOTO',
    id: '',
    images: [{
      source: '',
      ratio: 1
    }]
  }
  return {
    type: ADD_IMAGE_DATA_TO_COLLECTION,
    imageData: imageData
  }
}
function updateImageData (arrImages, index) {
  return {
    type: UPDATE_IMAGE_DATA,
    imageData: {
      index: index,
      isUploading: false,
      images: [{
        source: arrImages[0],
        ratio: 1
      }, {
        source: arrImages[1],
        ratio: 0.5
      }, {
        source: arrImages[2],
        ratio: 0.25
      }
      ]
    }
  }
}
function receiveError(err) {
  console.log(err)
  return {
    type: RECEIVE_ERROR,
    err: err
  }
}
function createMediaItemsSuccess () {
  return {
    type: CREATE_MEDIA_ITEM_SUCCESS
  }
}
function resetCurrentState () {
  return {
    type: RESET_STATE
  }
}
// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [GET_USER_SUCCESS]: (state, action) => {
    return {
      ...state,
      userList: [...action.data]
    }
  },
  [UPDATE_LOADING_STATUS]: (state) => {
    return {
      ...state,
      isLoading: true,
      countCreatedMedia: 0
    }
  },
  [CREATE_MEDIA_ITEM_SUCCESS]: (state) => {
    return {
      ...state,
      isLoading: false,
      createAlbumSuccess: true
    }
  },
  [RESET_STATE]: (state) => {
    return {
      ...state,
      ...initialState
    }
  },
  [COLLECTION_TITLE_CHANGE]: (state, action) => {
    return {
      ...state,
      title: action.title
    }
  },
  [IMAGE_DESC_CHANGE]: (state, action) => {
    return {
      ...state,
      imageList: state.imageList.map(
        (imageData, i) => imageData.index === action.imageData.index
          ? { ...imageData, description: action.imageData.description }
          : imageData
      )
    }
  },
  [ADD_IMAGE_DATA_TO_COLLECTION]: (state, action) => {
    return {
      ...state,
      imageList: [...state.imageList, action.imageData]
    }
  },
  [UPDATE_IMAGE_DATA]: (state, action) => {
    return {
      ...state,
      imageList: state.imageList.map(
        (imageData, i) => imageData.index === action.imageData.index
          ? { ...imageData, isUploading: false, images: action.imageData.images }
          : imageData
      )
    }
  },
  [RECEIVE_ERROR]: (state, action) => {
    let error = action.error || ''
    let errorMessage = error.message === '400' ? 'Have problem when upload file' : 'Error'
    state = update(state, {
      isUploading: { $set: action.state },
      errorMessage: { $set: errorMessage }
    })
    return state
  },
  [REMOVE_ITEM]: (state, action) => {
    return {
      ...state,
      imageList: [...state.imageList.slice(0, action.index),
        ...state.imageList.slice(action.index + 1)],
      removeItemsList: [...state.removeItemsList, state.imageList[action.index]]
    }
  },
  [START_ADD_COLLABORATORS]: (state, action) => {
    return {
      ...state,
      imageList: state.imageList.map(
        (imageData, i) => imageData.index === action.payload
        ? { ...imageData, isAddingCollaborators: !imageData.isAddingCollaborators, collaborations: imageData.collaborations ? imageData.collaborations : [] }
        : { ...imageData, isAddingCollaborators: false }
      )
    }
  },
  [ADD_COLLABORATORS]: (state, action) => {
    return {
      ...state,
      imageList: state.imageList.map(
        (imageData, i) => imageData.index === action.index
        ? { ...imageData, isAddingCollaborators: false, collaborations: [action.payload, ...imageData.collaborations] }
        : { ...imageData, isAddingCollaborators: false }
      )
    }
  },
  [GET_COLLECTION_SUCCESS]: (state, action) => {
    return {
      ...state,
      collection: action.payload,
      title: action.payload.title,
      createAlbumSuccess: false
    }
  },
  [RESET_STATE]: (state) => {
    return {
      ...state,
      ...initialState
    }
  },
  [UPDATE_GROUP_UPLOAD]: (state, action) => {
    return {
      ...state,
      isGroupUpload: action.payload
    }
  }
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  countCreatedMedia: 0,
  isLoading: false,
  createAlbumSuccess: false,
  imageList: [],
  errorMessage: '',
  title: '',
  description: '',
  userList: [],
  collection: {},
  isGroupUpload: false,
  removeItemsList: []
}

export default function collection (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
