import { connectWithLifecycle } from 'react-lifecycle-component'
import { handelUploadImages,
         handleImageDescChange,
         publishCollection,
         handleCollectionTitleChange,
         handleAddCollaborators,
         handleStartAddCollaborators,
         removeItem,
         getCollection,
         getUsers } from '../modules/create_collection'

import CreateCollection from '../components/create_collection'

const mapDispatchToProps = {
  handelUploadImages,
  handleImageDescChange,
  publishCollection,
  handleCollectionTitleChange,
  handleAddCollaborators,
  handleStartAddCollaborators,
  getUsers,
  removeItem,
  getCollection
}

const mapStateToProps = (state) => {
  return state.createCollection
}

export default connectWithLifecycle(
  mapStateToProps, mapDispatchToProps
)(CreateCollection)
