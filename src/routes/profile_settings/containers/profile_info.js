import { connectWithLifecycle } from 'react-lifecycle-component'
import ProfileInfo from '../components/profile_info'
import { handleInputValueChange, saveChanges, restoreAllAttributes } from '../modules/profile_info'

const mapDispatchToProps = {
  handleInputValueChange,
  saveChanges,
  restoreAllAttributes
}

const mapStateToProps = (state) => {
  return state.profileInfo
}

export default connectWithLifecycle(
  mapStateToProps, mapDispatchToProps
)(ProfileInfo)
