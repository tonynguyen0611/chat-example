import { connectWithLifecycle } from 'react-lifecycle-component'
import EduExp from '../components/edu_exp'
import { handleInputValueChange, addCareerItem, clearInput, saveChanges, getUserDetails, deleteItem } from '../modules/edu_exp'

const mapDispatchToProps = {
  handleInputValueChange,
  addCareerItem,
  clearInput,
  saveChanges,
  getUserDetails,
  deleteItem
}

const mapStateToProps = (state) => {
  return state.eduExp
}

export default connectWithLifecycle(
  mapStateToProps, mapDispatchToProps
)(EduExp)
