import { connectWithLifecycle } from 'react-lifecycle-component'

import Header from '../components/header'
const USER_INFO = 'userinfo'
const mapDispatchToProps = {
}

const mapStateToProps = (state) => {
  return {
    currentUser: JSON.parse(localStorage.getItem(USER_INFO))
  }
}

export default connectWithLifecycle(
  mapStateToProps, mapDispatchToProps
)(Header)
