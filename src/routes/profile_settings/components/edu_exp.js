import React from 'react'
import PropTypes from 'prop-types'
import 'react-date-picker/index.css'
import { DateField } from 'react-date-picker'
import './edu_exp.scss'

const loadingImage = <img src='https://cdn.zenquiz.net/static/Assets/loading-animation.gif' alt='Loading...' />

class EduExp extends React.Component {
  componentWillMount () {
    this.props.getUserDetails()
  }

  customizeDate (timeObj) {
    if (timeObj) {
      return timeObj.day + '/' + timeObj.month + '/' + timeObj.year
    } else {
      return 'dd/mm/yyyy'
    }
  }

  render () {
    let _this = this
    let careerList = _this.props.careerList
    console.log('careerList', careerList)
    return (
      <div className='col-md-8 side-bar-right'>
        <div className='profile-details edu-exp'>
          <div className='header-block'>
            <h5 className='title'>
              Your Education History
            </h5>
          </div>
          {
            !_this.props.isDetailsLoading &&
            <div className={'content-block ' + ((_this.props.isSaveLoading || _this.props.isCancelLoading) ? 'edu-lock' : '')}>
              {
                careerList.map((careerItem, index) => {
                  return (
                    <div className={'edu-item ' + (careerItem.id ? '' : 'new-item')} key={index}>
                      <div className='row'>
                        <div className='col-lg-12 col-md-12 col-sm-12 col-xs-12 erase-container'>
                          <button onClick={() => _this.props.deleteItem(careerItem)} >
                            <i className='fa fa-trash-o erase-icon' aria-hidden='true' />
                          </button>
                        </div>
                      </div>
                      <div className='row'>
                        <div className='col-lg-6 col-md-6 col-sm-12 col-xs-12'>
                          <div className='form-group label-floating'>
                            <label className='control-label'>Title</label>
                            <div>
                              <input
                                type='text'
                                defaultValue={careerItem.title}
                                onChange={(e) => _this.props.handleInputValueChange(e, index, 'title')} />
                            </div>
                          </div>
                        </div>
                        <div className='col-lg-6 col-md-6 col-sm-12 col-xs-12'>
                          <div className='form-group label-floating'>
                            <label className='control-label'>Place</label>
                            <div>
                              <input
                                type='text'
                                defaultValue={careerItem.place}
                                onChange={(e) => _this.props.handleInputValueChange(e, index, 'place')} />
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className='row'>
                        <div className='col-lg-6 col-md-6 col-sm-12 col-xs-12'>
                          <div className='form-group label-floating'>
                            <label className='control-label'>Start time</label>
                            <div>
                              <DateField
                                dateFormat='DD/MM/YYYY'
                                defaultValue={_this.customizeDate(careerItem.start_time)}
                                onChange={(e) => _this.props.handleInputValueChange(e, index, 'start_time')}
                              />
                            </div>
                          </div>
                        </div>
                        <div className='col-lg-6 col-md-6 col-sm-12 col-xs-12'>
                          <div className='form-group label-floating'>
                            <label className='control-label'>End time</label>
                            <div>
                              <DateField
                                dateFormat='DD/MM/YYYY'
                                defaultValue={_this.customizeDate(careerItem.end_time)}
                                onChange={(e) => _this.props.handleInputValueChange(e, index, 'end_time')}
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className='row'>
                        <div className='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
                          <div className='form-group label-floating full-height'>
                            <label className='control-label'>Bio</label>
                            <textarea
                              placeholder=''
                              defaultValue={careerItem.description}
                              onChange={(e) => _this.props.handleInputValueChange(e, index, 'description')} />
                          </div>
                        </div>
                      </div>
                    </div>
                  )
                })
              }
              <div className='action-container'>
                <div className='row'>
                  <div className='col-lg-3 col-md-3 col-sm-12 col-xs-12'>
                    <div className='add-edu-btn' onClick={_this.props.addCareerItem}>
                      <div>
                        <i className='fa fa-plus' aria-hidden='true' />  Add Education Field</div>
                    </div>
                  </div>
                </div>
                <div className='row'>
                  <div className='col-lg-6 col-md-6 col-sm-12 col-xs-12'>
                    <button className='btn btn-lg btn-bottom btn-left' onClick={_this.props.clearInput}>
                      {
                        _this.props.isCancelLoading ? loadingImage : 'Cancel'
                      }
                    </button>
                  </div>
                  <div className='col-lg-6 col-md-6 col-sm-12 col-xs-12'>
                    <button className='btn btn-lg btn-bottom btn-right' onClick={_this.props.saveChanges}>
                      {
                        _this.props.isSaveLoading ? loadingImage : 'Save all Changes'
                      }
                    </button>
                  </div>
                </div>
                <div className='messages'>
                  <p className='error'> { _this.props.isError && _this.props.errorMessage } </p>
                  <p className='success'> { _this.props.isSuccess && _this.props.successMessage } </p>
                </div>
              </div>
            </div>
          }
          {
            _this.props.isDetailsLoading && loadingImage
          }
        </div>
      </div>
    )
  }
}

EduExp.propTypes = {
}
export default EduExp
