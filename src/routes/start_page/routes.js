import React from 'react'
import { injectReducer } from '../../store/reducers'
import StartView from './components/start_view'

const LoginForm = (store) => {
  const LoginForm = require('./containers/login_container').default
  const reducer = require('./modules/login').default
  injectReducer(store, { key: 'currentUser', reducer })
  return LoginForm
}

const RegisterForm = (store) => {
  const RegisterForm = require('./containers/register').default
  const reducer = require('./modules/register').default
  injectReducer(store, { key: 'newUser', reducer })
  return RegisterForm
}

const RecoverForm = (store) => {
  const RecoverForm = require('./containers/recover_password').default
  const reducer = require('./modules/recover_password').default
  injectReducer(store, { key: 'recoverPassword', reducer })
  return RecoverForm
}

export const StartViewRoute = (store) => ({
  path: '/start/view',
  component: StartView
})

export const LoginRoute = (store) => ({
  path: '/start/login',
  component: LoginForm(store)
})

export const RegisterRoute = (store) => ({
  path: '/start/register',
  component: RegisterForm(store)
})

export const RecoverRoute = (store) => ({
  path: '/start/forgot-password',
  component: RecoverForm(store)
})
