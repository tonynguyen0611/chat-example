import axios from 'axios'
import { replace } from 'react-router-redux'
import SendBird from 'sendbird'
import { handleSelectUserToChat } from '../routes/profile_settings/modules/messages'
// ------------------------------------
// Constants
// ------------------------------------
const SELECTED_SENDER_MESSAGE = 'SELECTED_SENDER_MESSAGE'
const UPDATE_NOTIFICATION_MESSAGES = 'UPDATE_NOTIFICATION_MESSAGES'
const UPDATE_WHEN_CHANNEL_CHANGE = 'UPDATE_WHEN_CHANNEL_CHANGE'
const ADD_NEW_CHANNEL = 'ADD_NEW_CHANNEL'
const GET_COLLABORATIONS_SUCCESS = 'GET_COLLABORATIONS_SUCCESS'
// ------------------------------------
// Actions
// ------------------------------------
export function handleUpdateNotificationMessages (data) {
  return {
    type: UPDATE_NOTIFICATION_MESSAGES,
    payload: data
  }
}
export function handleUpdateWhenChannelChange (selectedChannel) {
  console.log('=======>', selectedChannel)
  return (dispatch, getState) => {
    let modulesState = getState().notifications
    let count = 0
    modulesState.messages.listMessages.map((channel, i) => {
      if (channel && channel.unreadMessageCount && channel.unreadMessageCount > 0) {
        count += 1
      }
      if (channel && channel.url === selectedChannel.url) {
        dispatch(UpdateWhenChannelChange(i, count, selectedChannel))
      }
    })
  }
}
export function handleUpdateWhenRecieveMessage (selectedChannel) {
  return (dispatch, getState) => {
    let modulesState = getState().notifications
    modulesState.messages.listMessages.map((channel, i) => {
      if (channel.url === selectedChannel.url) {
        if (channel.unreadMessageCount > 0) {
          dispatch(UpdateWhenChannelChange(i, 0, selectedChannel))
        } else {
          dispatch(UpdateWhenChannelChange(i, 1, selectedChannel))
        }
      }
    })
  }
}
export function getCollaborationsRequest (currentUser) {
  return (dispatch, getState) => {
    fetchCollaborations(currentUser.id)
    .then(res => dispatch(getCollaborationsSuccess(res)))
    .catch(err => console.log(err))
  }
}
function getCollaborationsSuccess (data) {
  return {
    type: GET_COLLABORATIONS_SUCCESS,
    payload: data,
    count: data.length
  }
}
function fetchCollaborations(userId) {
  return new Promise(function (resolve, reject) {
    axios.get(ENVIRONMENT.API_DOMAIN + '/users/' + userId + '/collaborations?status=pending',
    { withCredentials: true })
    .then(res => {
      console.log(res.data)
      resolve(res.data)
    })
    .catch(err => reject(err.response.data))
  })
}
function UpdateWhenChannelChange (index, count, channel) {
  return {
    type: UPDATE_WHEN_CHANNEL_CHANGE,
    index: index,
    count: count,
    channel: channel
  }
}
function selectSenderMessage (data) {
  return {
    type: SELECTED_SENDER_MESSAGE,
    payload: data
  }
}
export function handleSelectedSenderMessages (data) {
  return (dispatch, getState) => {
    let sb = SendBird.getInstance()
    dispatch(selectSenderMessage(data))
    console.log(data, 'handleSelectUserToChat')
    dispatch(handleSelectUserToChat(data, sb))
    dispatch(replace('/profile/messages'))
  }
}
export function handleAddNewChannelInNotification (channel) {
  return {
    type: ADD_NEW_CHANNEL,
    payload: channel
  }
}
// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [ADD_NEW_CHANNEL]: (state, action) => {
    return {
      ...state,
      messages: {
        ...state.messages,
        listMessages: [action.payload, ...state.messages.listMessages]
      }
    }
  },
  [UPDATE_NOTIFICATION_MESSAGES]: (state, action) => {
    return {
      ...state,
      messages: action.payload
    }
  },
  [SELECTED_SENDER_MESSAGE]: (state, action) => {
    return {
      ...state,
      messages: {
        ...state.messages, selectedSender: action.payload
      }
    }
  },
  [UPDATE_WHEN_CHANNEL_CHANGE]: (state, action) => {
    return {
      ...state,
      messages: {
        ...state.messages,
        count: action.count
      }
    }
  },
  [GET_COLLABORATIONS_SUCCESS]: (state, action) => {
    return {
      ...state,
      collaborations: {
        ...state.collaborations,
        listCollaborations: action.payload,
        count: action.count
      }
    }
  }
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  messages: {
    count: 0,
    listMessages: [],
    selectedSender: {}
  },
  collaborations: {
    count: 0,
    listCollaborations: [],
    selectedSender: {}
  }
}

export default function changePasswordReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
