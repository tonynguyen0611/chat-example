import { connectWithLifecycle } from 'react-lifecycle-component'
import { initialize } from '../modules/about'
import About from '../components/about'

const mapDispatchToProps = {
  initialize
}

const mapStateToProps = (state) => {
  return state.about
}

export default connectWithLifecycle(
  mapStateToProps, mapDispatchToProps
)(About)
