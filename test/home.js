import { renderComponent, expect } from '../helper/test_helper'

import { injectReducer } from '../store/reducers'
import ConnectedHome from '../routes/home/containers/home'
import HomeView from '../routes/home/components/HomeView'
import { getNewsFeed, getNewsFeedSuccess } from '../routes/home/modules/home'

describe('>>>H O M E --- TEST COMPONENT', () => {

    beforeEach(() => {
      const reducer = require('../modules/home').default
      component = renderComponent(CommentList, null, props);
    })
  
    it('+++ check Prop matches with initialState', () => {
      expect(wrapper.find(HomeView).prop('newsFeedData')).toHaveLength(0)
    })
})