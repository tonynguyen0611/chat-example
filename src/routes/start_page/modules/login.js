import UserService from '../../../api/user'
import { validate } from './../../../utils/validate'
import { setCurrentUSer } from '../../user_view/modules/header'
// ------------------------------------
// Constants
// ------------------------------------
const NORMAL_LOADING = 'NORMAL_LOADING'
const SOCIAL_LOADING = 'SOCIAL_LOADING'
const LOGIN_ERROR = 'LOGIN_ERROR'
const LOGIN_SUCCESS = 'LOGIN_SUCCESS'
const INPUT_VALUE = 'INPUT_VALUE'
const SET_MODAL_STATE = 'SET_MODAL_STATE'
const SET_SOCIAL_USER = 'SET_SOCIAL_USER'
const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS'
const SET_LOGIN_CHANNEL = 'SET_LOGGIN_CHANNEL'
const USER_INFO = 'userinfo'

// ------------------------------------
// Actions
// ------------------------------------
function sendLoginRequest (method, data) {
  return new Promise((resolve, reject) => {
    UserService.logIn(method, data)
    .then(res => resolve(res.data))
    .catch(err => reject(err.response.data.error))
  })
}

export function sendLogOutRequest () {
  return (dispatch, getState) => {
    UserService.logOut()
    .then(res => {
      dispatch(logOutSuccess())
      localStorage.removeItem('userinfo')
      window.location.reload()
    })
  }
}

export function login (event) {
  return (dispatch, getState) => {
    dispatch(loginLocal())
  }
}

export function signinWithFb () {
  return (dispatch, getState) => {
    FB.login(response => {
      if (response.status !== 'connected') {
        dispatch(setErrorMessage('Not authorized to login with Facebook'))
      } else { // connected
        FB.api('/' + response.authResponse.userID, { fields: 'about, first_name, last_name, birthday, email, website, id, cover, education' }, function (userObj) {
          dispatch(setSocialLoading('facebook'))
          dispatch(setLoginChannel('facebook'))
          userObj.accessToken = response.authResponse.accessToken
          dispatch(setSocialUser(userObj))
          sendLoginRequest('facebook', {
            'access_token': userObj.accessToken
          })
          .then(res => {
            dispatch(loginSuccess(res))
          }
          )
          .catch(err => {
            if (err !== 'UNREGISTERED_USER') {
              dispatch(setErrorMessage(err))
            } else {
              dispatch(setModalState())
            }
          })
        })
      }
    }, { scope: 'public_profile,email' })
  }
}

export function signinWithGoogle (response) {
  return (dispatch, getState) => {
    if (response.accessToken) {
      let userObj = {}
      userObj.accessToken = response.accessToken
      userObj.email = response.profileObj.email
      userObj.first_name = response.profileObj.givenName
      userObj.last_name = response.profileObj.familyName
      dispatch(setSocialUser(userObj))
      dispatch(setSocialLoading('google'))
      dispatch(setLoginChannel('google'))
      sendLoginRequest('google', {
        'access_token': userObj.accessToken
      })
      .then(res => dispatch(loginSuccess(res)))
      .catch(err => {
        if (err !== 'UNREGISTERED_USER') {
          dispatch(setErrorMessage(err))
        } else {
          dispatch(setModalState())
        }
      })
    }
  }
}

export function registerSocialEmail () {
  return (dispatch, getState) => {
    dispatch(normalLoading())
    let moduleState = getState().currentUser
    let validationMsg = validate('email', moduleState.socialEmail)
    if (validationMsg !== 'NO_ERROR') {
      return dispatch(setErrorMessage(validationMsg))
    } else {
      dispatch(sendRegisterSocialRequest(moduleState.loginChannel))
    }
  }
}

function sendRegisterSocialRequest (socialType) {
  return (dispatch, getState) => {
    let moduleState = getState().currentUser
    let socialUser = moduleState.userSocialInfo
    let data = {
      'auth_providers': socialType,
      'email': moduleState.socialEmail,
      'first_name': socialUser.first_name || '',
      'last_name': socialUser.last_name || '',
      'birthday': socialUser.birthday || '',
      'access_token': socialUser.accessToken || ''
    }
    UserService.Register(data)
    .then(res1 => {
      // if register email successfully, send login requst with access_token above to login
      sendLoginRequest(socialType, {
        'access_token': socialUser.accessToken
      })
      .then(res2 => dispatch(loginSuccess(res2)))
      .catch(err => dispatch(setErrorMessage(err)))
    })
    .catch(err => {
      dispatch(setErrorMessage(err.response.data.error))
    })
  }
}

export function handleInputValueChange (e, divName) {
  return (dispatch, getState) => {
    let input = {}
    input[divName] = e.target.value
    dispatch(setInput(input))
  }
}
export function handleOnKeyChange (e) {
  return (dispatch, getState) => {
    if (e.key === 'Enter') {
      dispatch(loginLocal())
    }
  }
}

export function setModalState () {
  return (dispatch, getState) => {
    dispatch(updateModalState(!getState().currentUser.isShowModal))
  }
}

function loginLocal () {
  return (dispatch, getState) => {
    const moduleState = getState().currentUser
    dispatch(normalLoading())
    sendLoginRequest('local', {
      'email': moduleState.email,
      'password': moduleState.password
    })
    .then(res => dispatch(loginSuccess(res)))
    .catch(err => dispatch(setErrorMessage(err)))
  }
}

function updateModalState (value) {
  return {
    type: SET_MODAL_STATE,
    isShowModal: value
  }
}

function setErrorMessage (err) {
  return {
    type: LOGIN_ERROR,
    error: err
  }
}

function normalLoading () {
  return {
    type: NORMAL_LOADING
  }
}

function setSocialLoading (socialType) {
  return {
    type: SOCIAL_LOADING,
    socialType: socialType
  }
}

function loginSuccess (user) {
  return (dispatch) => {
    if (user.profile_avatar && !user.profile_avatar.url) {
      user.profile_avatar.url = 'http://ocotur-dev.s3.amazonaws.com/static/assets/ava.jpg'
    }
    dispatch(setCurrentUSer(user))
    dispatch(sendEventLoginSuccess(user))
  }
}

function sendEventLoginSuccess (user) {
  return {
    type: LOGIN_SUCCESS,
    user: user
  }
}

function setInput (input) {
  return {
    type: INPUT_VALUE,
    input: input
  }
}

function setSocialUser (userObj) {
  return {
    type: SET_SOCIAL_USER,
    userSocialInfo: userObj
  }
}

function logOutSuccess () {
  return {
    type: LOGOUT_SUCCESS
  }
}

function setLoginChannel (loginChannel) {
  return {
    type: SET_LOGIN_CHANNEL,
    loginChannel: loginChannel
  }
}

export function setCurrentUser (user) {
  localStorage.setItem(USER_INFO, JSON.stringify(user))
}
// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [NORMAL_LOADING]: (state, action) => {
    return {
      ...state,
      isNormalLoading: true,
      isLoginError: false,
      errorMessage: ''
    }
  },
  [SOCIAL_LOADING]: (state, action) => {
    return {
      ...state,
      isSocialGoogleLoading: action.socialType === 'google',
      isSocialFacebookLoading: action.socialType === 'facebook',
      isLoginError: false,
      errorMessage: ''
    }
  },
  [INPUT_VALUE]: (state, action) => {
    return {
      ...state,
      ...action.input
    }
  },
  [LOGIN_SUCCESS]: (state, action) => {
    // handle succeed login
    return {
      ...state,
      isNormalLoading: false,
      isSocialGoogleLoading: false,
      isSocialFacebookLoading: false,
      isLoginSuccess: true,
      userInfo: action.user
    }
  },
  [LOGIN_ERROR]: (state, action) => {
    let apiErrorCodes = ['INVALID_CREDENTIAL', 'NO_VALID_DATA']
    let errorMessage = apiErrorCodes.indexOf(action.error) >= 0 ? 'Invalid email or password' : action.error
    return {
      ...state,
      isNormalLoading: false,
      isSocialGoogleLoading: false,
      isSocialFacebookLoading: false,
      isLoginError: true,
      errorMessage: errorMessage
    }
  },
  [SET_MODAL_STATE]: (state, action) => {
    return {
      ...state,
      isShowModal: action.isShowModal,
      isSocialFacebookLoading: false,
      isSocialGoogleLoading: false,
      isLoginError: false,
      errorMessage: ''
    }
  },
  [SET_SOCIAL_USER]: (state, action) => {
    return {
      ...state,
      userSocialInfo: action.userSocialInfo
    }
  },
  [LOGOUT_SUCCESS]: (state, action) => {
    return {
      ...state,
      isLoginSuccess: false,
      userInfo: {}
    }
  },
  [SET_LOGIN_CHANNEL]: (state, action) => {
    return {
      ...state,
      loginChannel: action.loginChannel,
      userInfo: {}
    }
  }
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  isNormalLoading: false,
  isSocialGoogleLoading: false,
  isSocialFacebookLoading: false,
  loginChannel: '',
  isLoginSuccess: false,
  isShowModal: false,
  socialEmail: '',
  email: '',
  password: '',
  isLoginError: false,
  errorMessage: '',
  userSocialInfo: {},
  userInfo: JSON.parse(localStorage.getItem(USER_INFO))
}

export default function loginReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}

