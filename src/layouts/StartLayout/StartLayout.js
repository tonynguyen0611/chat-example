import React from 'react'
import { Route, Redirect } from 'react-router-dom'
import './StartLayout.scss'
import '../../styles/core.scss'

export const StartLayout = ({ component: Component, store: store, route: route, ...rest }) => {
  return (
    <Route exact={route.exact} {...rest} render={matchProps => (
      <div className='start-layout text-center'>
        <div className='form-container'>
          <b><p>o c o t u r</p></b>
          <b><h3>The network for fashion professionals</h3></b>
          <Component {...matchProps} store={store} />
        </div>
      </div>
    )} />
  )
}

export default StartLayout
