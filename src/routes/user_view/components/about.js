import React from 'react'
import PropTypes from 'prop-types'
import './about.scss'

const loadingImage = <img src='https://cdn.zenquiz.net/static/Assets/loading-animation.gif' alt='Loading...' />

class UserViewAbout extends React.Component {

  componentWillMount () {
    this.props.initialize(this.props.match.params.userId)
  }

  customizeDate (timeObj) {
    if (timeObj) {
      return timeObj.day + '/' + timeObj.month + '/' + timeObj.year
    } else {
      return 'dd/mm/yyyy'
    }
  }

  customizeExternalPageUrl (url) {
    if (url.indexOf('http://') < 0 && url.indexOf('https://') < 0) {
      return 'http://' + url
    }
    return url
  }

  render () {
    let userInfo = this.props.userInfo || {}
    let _this = this
    return (
      <div>
        {
          _this.props.isLoading && loadingImage
        }
        {
          !_this.props.isLoading &&
          <div className='col-lg-12 col-md-12 col-sm-12 col-xs-12 user-view-about'>
            <div className='col-lg-4 col-md-4 col-sm-12 col-xs-12 left-panel'>
              <div className='info-title'>
                Personal info
              </div>
              <div className='info-details'>
                <div className='info-1'>
                  <div className='col-lg-3 col-md-3 col-sm-12 col-xs-12 info-1-title'>
                    Bio
                  </div>
                  <div className='col-lg-9 col-md-9 col-sm-12 col-xs-12 info-1-details'>
                    {userInfo.bio}
                  </div>
                </div>
                <div className='info-1'>
                  <div className='col-lg-3 col-md-3 col-sm-12 col-xs-12 info-1-title'>
                    Skills
                  </div>
                  <div className='col-lg-9 col-md-9 col-sm-12 col-xs-12 info-1-details'>
                    {userInfo.skills ? userInfo.skills[0] : ''}
                  </div>
                </div>
                <div className='info-1'>
                  <div className='col-lg-3 col-md-3 col-sm-12 col-xs-12 info-1-title'>
                    Categories
                  </div>
                  <div className='col-lg-9 col-md-9 col-sm-12 col-xs-12 info-1-details'>
                    {userInfo.categories && userInfo.categories.length > 0 ? userInfo.categories.join(', ') : ''}
                  </div>
                </div>
                <div className='info-1'>
                  <div className='col-lg-3 col-md-3 col-sm-12 col-xs-12 info-1-title'>
                    Location
                  </div>
                  {
                    userInfo.location &&
                    <div className='col-lg-9 col-md-9 col-sm-12 col-xs-12 info-1-details'>
                      {userInfo.location.region ? userInfo.location.city + ', ' : userInfo.location.city}
                      {userInfo.location.country ? userInfo.location.region + ', ' : userInfo.location.region}
                      {userInfo.location.country}
                    </div>
                  }
                </div>
                <div className='info-1'>
                  <div className='col-lg-3 col-md-3 col-sm-12 col-xs-12 info-1-title'>
                    Tags
                  </div>
                  <div className='col-lg-9 col-md-9 col-sm-12 col-xs-12 info-1-details'>
                    {userInfo.tags && userInfo.tags.length > 0 ? userInfo.tags.join(', ') : ''}
                  </div>
                </div>
                <div className='info-1'>
                  <div className='col-lg-3 col-md-3 col-sm-12 col-xs-12 info-1-title'>
                    Website
                  </div>
                  {
                    userInfo.personal_website &&
                    <div className='col-lg-9 col-md-9 col-sm-12 col-xs-12 info-1-details'>
                      <a href={_this.customizeExternalPageUrl(userInfo.personal_website)} target='_blank'> {userInfo.personal_website} </a>
                    </div>
                  }
                </div>
                {
                  userInfo.external_profiles &&
                  <div className='info-1'>
                    <div className='col-lg-3 col-md-3 col-sm-12 col-xs-12 info-1-title' />
                    <div className='col-lg-9 col-md-9 col-sm-12 col-xs-12 info-1-details'>
                      {
                        !userInfo.external_profiles.instagram ? ''
                          : <a href={_this.customizeExternalPageUrl(userInfo.external_profiles.instagram)}
                            target='_blank'>
                            <div className='web-icon'><i className='fa fa-instagram' aria-hidden='true' /></div>
                          </a>
                      }
                      {
                        !userInfo.external_profiles.linkedin ? ''
                          : <a href={_this.customizeExternalPageUrl(userInfo.external_profiles.linkedin)}
                            target='_blank'>
                            <div className='web-icon'><i className='fa fa-linkedin' aria-hidden='true' /></div>
                          </a>
                      }
                      {
                        !userInfo.external_profiles.twitter ? ''
                          : <a href={_this.customizeExternalPageUrl(userInfo.external_profiles.twitter)}
                            target='_blank'>
                            <div className='web-icon'><i className='fa fa-twitter' aria-hidden='true' /></div>
                          </a>
                      }
                    </div>
                  </div>
                }
              </div>
            </div>
            <div className='col-lg-8 col-md-8 col-sm-12 col-xs-12 right-panel'>
              <div className='exp-body'>
                <div className='exp-title'>
                  Education and Experience
                </div>
                <div className='exp-details'>
                  {
                    !userInfo.career_histories ? '' : userInfo.career_histories.map((careerItem, index) => {
                      return (
                        <div className='exp-item' key={index}>
                          <div className='exp-item-default exp-item-title'>{careerItem.title}</div>
                          <div className='exp-item-default exp-item-brand row'>
                            <div className='col-lg-3 col-md-3 col-sm-12 col-xs-12 brand-title'>{careerItem.place}</div>
                            <div className='col-lg-9 col-md-9 col-sm-12 col-xs-12 brand-img'>
                              {/* <img src='' /> */}
                            </div>
                          </div>
                          <div className='exp-item-default exp-item-duration'>
                            <span className='duration-time'>
                              {careerItem.startTimeString} - {careerItem.endTimeString} ({careerItem.duration})
                            </span>
                            {/* <span className='d-location'> London </span> */}
                          </div>
                          <div className='exp-item-default exp-item-details'>
                            {careerItem.description}
                          </div>
                        </div>
                      )
                    })
                  }
                </div>
              </div>
            </div>
          </div>
        }
      </div>
    )
  }
}

UserViewAbout.propTypes = {
  userInfo: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  initialize: PropTypes.func
}
export default UserViewAbout
